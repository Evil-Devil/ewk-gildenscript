<?php
namespace Ewigkeit\Bank;

class Item
{
    public $id = 0;
    public $name = null;
    public $description = null;
    public $price = 0;
    public $stockAmount = 0;
    public $catId = 0;
    public $timePeriod = 0;
    public $members = 0;
    public $maxPurchase = 0;
    public $status = 0;
}