<?php
namespace Ewigkeit\Bank;

use Ewigkeit\Sql\SqlAdapter;
use \Ewigkeit\Bank\Category;
use \Ewigkeit\Bank\Item;

class BankService
{
    private $sql = null;

    public function __construct()
    {
        $this->sql = SqlAdapter::getInstance();
    }

    public function fetchBaseCategories()
    {
        $queryString = "SELECT  `id`, `title`
                        FROM    `bank_kat`
                        WHERE   `cat` = 0
                        ORDER BY  `title`";
        $result = $this->sql->query($queryString);
        return $this->aggregateCategoryResult($result);
    }

    function fetchAllCategories()
    {
        $queryString = "SELECT  `id`, `title`
                        FROM `bank_kat`
                        ORDER BY `title`";
        $result = $this->sql->query($queryString);
        return $this->aggregateCategoryResult($result);
    }

    public function getCategoryTree() {
        $queryString = "SELECT  `id`, `cat`, `title`
                        FROM    `bank_kat`
                        ORDER BY  `cat`, `id`";
        $result = $this->sql->query($queryString);

        $categoryTree = array();
        foreach ($result as $row) {
            $category = new Category();
            $category->id = $row['id'];
            $category->catId = $row['cat'];
            $category->title = $row['title'];

            if ($category->catId == 0 && !isset($categoryTree[$category->id])) {
                $categoryTree[$category->id] = $category;
                continue;
            }
            $categoryTree[$category->catId]->childs[] = $category;
        }
        unset($result);
        return $categoryTree;
    }

    private function aggregateCategoryResult($result)
    {
        $categories = array();
        if ($result !== false) {
            foreach ($result as $row) {
                $category = new Category();
                $category->id = $row['id'];
                $category->title = $row['title'];
                $categories[] = $category;
            }
        }
        return $categories;
    }

    public function fetchItems($catId, $searchKey, $sortBy, $orderBy, $page, $perPage) {
        $where = array();
        $whereStr = '';
        if (!empty($catId)) {
            $where[] = "kat_id = {$catId}";
        }
        if (!empty($searchKey)) {
            $where[] = "name like '%{$searchKey}%'";
        }
        if (sizeof($where) > 0) {
            $whereStr = "WHERE " . implode(' AND ', $where);
        }
        unset($where);
        $startRow = ($page - 1) * $perPage;

        $queryString = "SELECT  `bi`.`id`, bi.`name`, `bi`.`status`, `bi`.`beschreibung`,
                                (`bi`.`menge` - SUM(IFNULL(`ib`.`menge`, 0))) as `verfuegbar`,
                                `bi`.`preis`, `title` as `typ`
                        FROM    `bank_item` `bi`
                          LEFT JOIN `items_buy` `ib` ON (`bi`.`id` = `ib`.`itemId` AND `ib`.`status` = 'vorgemerkt')
                          LEFT JOIN`bank_kat` `bk` ON `bk`.`id` = `bi`.`kat_id`
                        {$whereStr}
                        GROUP BY `bi`.`id`
                        ORDER BY {$sortBy} {$orderBy}
                        LIMIT {$startRow}, {$perPage}";
        return $this->sql->query($queryString);

    }

    public function fetchItem($itemId)
    {
        $itemId = (int) $itemId;



        $queryString = "SELECT  `id`, `name`, `beschreibung`, `preis`, `menge`,
                            `kat_id`, `members`, `periode`, `max`, `status`
                    FROM    `bank_item`
                    WHERE   `id` = {$this->sql->quote($itemId)}";
        $result = $this->sql->query($queryString);
        $item = new \Ewigkeit\Bank\Item();
        if ($result !== false) {
            $item = $this->hydrateBankItem($result[0]);
        }
        return $item;
    }

    private function hydrateBankItem($result) {
        $item = new Item();
        $item->id = $result['id'];
        $item->name = $result['name'];
        $item->description = $result['beschreibung'];
        $item->price = $result['preis'];
        $item->stockAmount = $result['menge'];
        $item->catId = $result['kat_id'];
        $item->members = $result['members'];
        $item->timePeriod = $result['periode'];
        $item->maxPurchase = $result['max'];
        $item->status = $result['status'];
        return $item;
    }
}