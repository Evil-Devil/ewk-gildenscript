<?php
namespace Ewigkeit\Editor;

class FilterOptions {
    public $charClass = 0;
    public $set = 0;
    public $quality = 0;
    public $levelRange = null;

    public function applyRequestData() {
        if (isset($_REQUEST['charclass']) && (int)$_REQUEST['charclass'] > 0) {
            $this->charClass = (int)$_REQUEST['charclass'];
        }
        if (isset($_REQUEST['set']) && (int)$_REQUEST['set'] > 0) {
            $this->set = (int)$_REQUEST['set'];
        }
        if (isset($_REQUEST['quality']) && (int)$_REQUEST['quality'] > 0) {
            $this->quality = (int)$_REQUEST['quality'];
        }
        if (isset($_REQUEST['levelrange']) && trim($_REQUEST['levelrange']) != '') {
            $this->levelRange = trim($_REQUEST['levelrange']);
        }
    }
}