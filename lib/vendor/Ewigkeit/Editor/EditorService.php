<?php
namespace Ewigkeit\Editor;

use Ewigkeit\Sql\SqlAdapter;

class EditorService {

    private $sql = null;
    
    public function __construct() {
        $this->sql = SqlAdapter::getInstance();
    }

    public function fetchCharClasses()
    {
        $queryString = "SELECT  `id`, `name`, `description`
                        FROM    `fw_charclass`
                        ORDER BY    `id`";
        $result = $this->sql->query($queryString);
        if ($result !== false) {
            return $result;
        }
        return array();
    }
        
    public function fetchQualities()
    {
        $queryString = "SELECT  `id`, `name`, `description`
                        FROM    `fw_quality`
                        ORDER BY    `id`";
        $result = $this->sql->query($queryString);
        if ($result !== false) {
            return $result;
        }
        return array();
    }
    
    public function fetchParts()
    {
        $queryString = "SELECT  `id`, `name`, `description`
                        FROM    `fw_part`
                        ORDER BY    `id`";
        $result = $this->sql->query($queryString);
        if ($result !== false) {
            return $result;
        }
        return array();
    }
    
    public function fetchSets(FilterOptions $filter)
    {
        $filterString = " WHERE 1";
        if ($filter->quality > 0) {
            $filterString .= " AND  quality_id = {$filter->quality}";
        }
        if ($filter->charClass > 0) {
            $filterString .= " AND  charclass_id = {$filter->charClass}";
        }
    
        $queryString = "SELECT  `id`, `name`, description
                        FROM    `fw_set`
                        {$filterString}
                        ORDER BY    `name`";
        $result = $this->sql->query($queryString);
        if ($result !== false) {
            return $result;
        }
        return array();
    }
    
    public function fetchItems(FilterOptions $filter)
    {
        $filterString = " WHERE 1";
        if ($filter->quality > 0) {
            $filterString .= " AND  i.quality_id = {$filter->quality}";
        }
        if ($filter->charClass > 0) {
            $filterString .= " AND  i.charclass_id = {$filter->charClass}";
        }
        if ($filter->set > 0) {
            $filterString .= " AND  i.set_id = {$filter->set}";
        }
        if ($filter->levelRange != null) {
            $levels = explode('_', $filter->levelRange);
            
            if (sizeof($levels) == 2) {
                $filterString .= " AND i.level BETWEEN {$levels[1]} AND {$levels[0]}";
            }
        }
    
        $queryString = "SELECT  i.`id`, i.`name`, i.`level`, i.set_id as setId, i.icon_index,
                                s.`name` as setname,
                                s.`description` as setdescription,
                                p.`name` as part,
                                q.`name` as quality,
                                c.`name` as charclass
                        FROM    `fw_item` as i
                            LEFT JOIN `fw_set` as s ON i.set_id = s.id
                            LEFT JOIN `fw_part`as p ON i.part_id = p.id
                            LEFT JOIN `fw_quality` as q ON i.quality_id = q.id
                            LEFT JOIN `fw_charclass` as c ON i.charclass_id = c.id
                        {$filterString}
                        ORDER BY    i.set_id, i.`id`";
        $result = $this->sql->query($queryString);
        if ($result !== false) {
            return $result;
        }
        return array();
    }
    
    public function fetchItem($itemId)
    {   
        $queryString = "SELECT  `id`, `name`, `set_id` as `set`, `quality_id` as quality, `charclass_id` as charclass,
                                `part_id` as part, `type_id` as `type`, `level`, `icon_index`
                        FROM    `fw_item`
                        WHERE   `id` = {$itemId}";
        $result = $this->sql->query($queryString);
        if ($result !== false) {
            return $result[0];
        }
        return null;
    }
    
    public function getItemTypeCss($item)
    {
        if ($item['part'] !== null && $item['charclass'] !== null && (int)$item['icon_index'] != -1) {
            $css .= "eq-{$item['part']}-{$item['charclass']} eq-index-{$item['icon_index']}";
        } else if ($item['part'] !== null && (int)$item['icon_index'] != -1){
            $css .= "eq-{$item['part']} eq-index-{$item['icon_index']}";
        } else {
            $css = 'eq-empty';
        }
        return $css;
    }
    
    public function getItemQualityCss($item)
    {
        if ($item['quality'] !== null) {
            return "eq-{$item['quality']}";
        }
        return '';
    }
}