<?php
namespace Ewigkeit\Common;
use Ewigkeit\Common\AbstractContribution;

class EventContribution extends AbstractContribution
{
    private $events = array();
    private $totalEventCount = 0;
    private $participationCount = 0;
    private $participations = array();

    public function getEvents()
    {
        return $this->events;
    }

    public function getTotalEventCount()
    {
        return $this->totalEventCount;
    }

    public function getParticipationCount()
    {
        return $this->participationCount;
    }

    public function getParticipations()
    {
        return $this->participations;
    }

    public function determineContributionForMember($memberId)
    {
        $memberId = (int) $memberId;        
        $this->determineTotalEventCount();        
        $this->determineEventsForMember($memberId);
    }

    public function getParticipationRatio()    
    {
        if ($this->totalEventCount == 0) {            
            return 0;
        }        
        return round(($this->participationCount / $this->totalEventCount) * 100);
    }

    private function determineEventsForMember($memberId)
    {
        $queryString = "SELECT  COUNT(e.`id`) as amount, e.name						FROM    `events` as e							INNER JOIN `dkp` d on e.`id` = d.`eventId`						WHERE   e.`name` like '[Event] %'						AND     e.`datum` between DATE_SUB(CURDATE(), INTERVAL {$this->intervalDays} DAY) AND CURRENT_TIMESTAMP						AND     d.`memberId` = {$memberId}						GROUP BY    e.name";
        $result = $this->sqlAdapter->query($queryString);
        if ($result !== false) {            
            foreach ($result as $key => $val) {
                $this->participations[$val['name']] = $val['amount'];                
                $this->participationCount = $this->participationCount +                         
                $val['amount'];
            }
            unset($result);
        }
    }
    
    public function determineEventsForAllMembers() {
        $queryString = "SELECT	m.id, count(*) as attends, m.name, m.`eintritt`, unix_timestamp(m.`eintritt`) as ts_eintritt,
						      (UNIX_TIMESTAMP() - unix_timestamp(m.`eintritt`)) as ts_lifetime,
						      0 as contribution
				        FROM  `dkp` d
					       INNER JOIN  `events` e ON d.eventid = e.id
					       INNER JOIN  `members` m on d.memberId = m.id
				        WHERE	m.status = 1
				        AND		m.twink = 0
				        AND		e.`name` LIKE  '[Event] %'
				        AND		e.`datum` >= DATE_SUB( CURDATE( ) , INTERVAL {$this->intervalDays} DAY )
				        GROUP BY	d.memberId";
        $result = $this->sqlAdapter->query($queryString);
        if ($result !== false) {
            return $result;
        }
        return array();
    }

    public function determineTotalEventCount()
    {
        if ($this->totalEventCount > 0) {
            return;
        }
        
        $queryString = "SELECT	count(id) as amount, name						FROM	(							SELECT	id, name							FROM	`events`							WHERE	`name` LIKE '[Event] %'							AND		`datum` between DATE_SUB(CURDATE(),INTERVAL {$this->intervalDays} DAY) AND CURRENT_TIMESTAMP							GROUP BY	DATE(`datum`)) a						GROUP BY	`name`";
        $result = $this->sqlAdapter->query($queryString);
        
        if ($result !== false) {            
            foreach ($result as $key => $val) {                
                $this->events[$val['name']] = $val['amount'];                
                $this->totalEventCount = $this->totalEventCount + $val['amount'];
            }
        }
    }
}