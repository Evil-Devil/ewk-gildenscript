<?php
namespace Ewigkeit\Common;

class Activity
{

    public function getAttentRatioColor($ratio)
    {
        if ($ratio >= 60) {
            return 'green';
        }
        if ($ratio >= 40) {
            return 'yellow';
        }
        if ($ratio >= 25) {
            return 'orange';
        }
        return 'red';
    }
    
    public function getContributionColor($contribution)
    {
        if ($contribution < 450) {
            return 'orange';
        }
        return 'green';
    }
    
    
}