<?php
namespace Ewigkeit\Common;

class ElementarEquipEntity {
    public $head = 0;
    public $body = 0;
    public $shoulders = 0;
    public $hands = 0;
    public $belt = 0;
    public $leg = 0;
    public $boots = 0;
}
