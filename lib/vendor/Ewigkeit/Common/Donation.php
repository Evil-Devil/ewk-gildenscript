<?php
namespace Ewigkeit\Common;

use Ewigkeit\Sql\SqlAdapter;

class Donation {
    
    private $sql = null;
    
    public function __construct() {
        $this->sql = SqlAdapter::getInstance();
    }

    public function retrieveComments($maxComments = 5) {
        $queryString = "SELECT	m.name as spender, ms.komentar
    					FROM	`messages_spende` ms
    					   INNER JOIN `members` m
    					       ON ms.userId = m.id
    					WHERE	LENGTH(ms.`komentar`) > 4
    					ORDER BY	ms.`datum` DESC
                        LIMIT   {$maxComments}";
        $result = $this->sql->query($queryString);
        if ($result !== false) {
            return $result;            
        }
        return array();        
    }
    
}