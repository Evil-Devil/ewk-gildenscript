<?php
namespace Ewigkeit\Sql;

use Ewigkeit\Sql\MySqlAdapter;

class SqlAdapter {

	private static $instance = null;
	private $adapter = null;

	private function __construct() {
	}
	
	public static function getInstance() {
		if (static::$instance === null) {
			static::$instance = new SqlAdapter();
			static::$instance->createConnection();
		}
		return static::$instance->getAdapter();
	}
	
	private function getAdapter() {
		return $this->adapter;
	}
	
	private function createConnection() {
		$this->adapter = new MySqlAdapter(DB_SERVER, DB_USER, DB_PASS, DB_SCHEME);
		$this->adapter->connect();
	}
}
