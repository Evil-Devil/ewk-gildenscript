<?php
namespace Ewigkeit\Sql;

class MySqlAdapter {
	private $connection = null;
	private $server = null;
	private $user = null;
	private $pass = null;
	private $database = null;
	public function __construct($server, $user, $pass, $database = null) {
		$this->server = $server;
		$this->user = $user;
		$this->pass = $pass;
		$this->database = $database;
	}	
	public function setDatabase($database) {
		$this->database = $database;
	}
	public function connect() {
		try {
			$this->connection = new \PDO("mysql:host={$this->server};dbname={$this->database}",
									$this->user, $this->pass,
                                    array(
										\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
										\PDO::MYSQL_ATTR_INIT_COMMAND => "SET SESSION SQL_BIG_SELECTS=1 ")
								);
			$this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		} catch (\PDOException $e) {						
			// TODO: Add Error Handling
			die($e->getMessage());
		}
	}	
	public function quote($value) {
		return $this->connection->quote($value);
	}
	
	// used for delete, update, insert
	public function exec($query) {
		return $this->connection->exec($query);
	}
	
	public function getLastInsertId() {
	    return $this->connection->lastInsertId();
	}
	
	// only for select
	public function query($query, $fetchMode = \PDO::FETCH_ASSOC) {
		$result = null;
		try {
			$result = $this->connection->query($query);
		} catch (\PDOException $e) {
			die($e->getMessage());
		}
		if ($result !== false) {
			return $result->fetchAll($fetchMode);
		}
		return $result;
	}	
	
	
	

}