<?php
include 'bootstrap.php';

use Ewigkeit\Sql\SqlAdapter;

function saveAdminNotice() {
    $sql = SqlAdapter::getInstance();

    $queryString = "INSERT  INTO    admin_notices (site, message)
                    VALUES  ({$sql->quote($_POST['site'])},
                            {$sql->quote($_POST['message'])})
                    ON DUPLICATE KEY
                        UPDATE  message = VALUES(message)";    
    $sql->exec($queryString);
}

if (!empty($_POST)) {
    saveAdminNotice();
    die('ok');
}