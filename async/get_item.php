<?php
include 'bootstrap.php';

use Ewigkeit\Editor\EditorService;

$itemId = 0;

if (isset($_REQUEST['id']) && (int)$_REQUEST['id'] > 0) {
    $itemId = (int)$_REQUEST['id'];
}
$editorService = new EditorService();
$item = $editorService->fetchItem($itemId);
// add content header type
echo json_encode($item);