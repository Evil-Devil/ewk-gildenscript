<?php
include 'bootstrap.php';

use Ewigkeit\Sql\SqlAdapter;

// validate data
$userId = 0;
$dkp = 0;
$comment = '';
if (isset($_POST['user'])) {
    $userId = $_POST['user'];
}
if (isset($_POST['dkp'])) {
    $dkp = $_POST['dkp'];
}
if (isset($_POST['comment'])) {
    $comment = $_POST['comment'];
}

if ($userId > 0 && $dkp > 0) {
    // calc honor
    $honor = $dkp;
    
    $sql = SqlAdapter::getInstance();
    
    $queryString = "INSERT INTO `events` (`name`, `punkte`, `datum`)
                    VALUES      ('Gildenspende', {$sql->quote(-$dkp)}, NOW())";
    $sql->exec($queryString);
    $eventId = $sql->getLastInsertId();    

    $queryString = "INSERT INTO `messages_spende` (`menge`, `userId`, `komentar`, `eventId`)
                    VALUES      ({$sql->quote($dkp)}, {$sql->quote($userId)}, {$sql->quote($comment)}, {$sql->quote($eventId)})";
    $sql->exec($queryString);
    
    $queryString = "INSERT INTO `dkp` (`eventId`, `memberId`)
                    VALUES      ({$sql->quote($eventId)}, {$sql->quote($userId)})";
    $sql->exec($queryString);
    
    // dem spender ruhm geben
    $queryString = "UPDATE  `members`
                    SET     `ruhm` = `ruhm` + {$sql->quote($dkp)}
                    WHERE   `id`= {$userId}";
    $sql->exec($queryString);

    // in ruhm tabelle eintragen
    $queryString = "INSERT INTO `honor` (`memberId`, `eventId`, `value`, `created`)
                    VALUES      ({$sql->quote($userId)}, {$sql->quote($eventId)}, {$sql->quote($honor)}, NOW())";
    $sql->exec($queryString);
    
    die('ok');
}