<?php
include 'bootstrap.php';

use Ewigkeit\Sql\SqlAdapter;

// validate data
$userId = 0;    // who donates?
$donateId = 0;  // who is donated?
$dkpOut = 0;

if (isset($_POST['user'])) {
    $userId = $_POST['user'];
}
if (isset($_POST['donate'])) {
    $donateId = $_POST['donate'];
}
if (isset($_POST['dkpOut'])) {
    $dkpOut = $_POST['dkpOut'];
}

if ($userId > 0 && $donateId > 0 && $dkpOut > 0) {
    $honor = floor($dkpOut * 0.5);
    $donation = floor($dkpOut * 0.8);

    $sql = SqlAdapter::getInstance();

    // name des ziels herausfinden
    $queryString = "SELECT  name
                    FROM    members
                    WHERE   id = " . $donateId;
    $result = $sql->query($queryString);

    // spender sachen machen
    $message = "Spende an Mitglied: {$result[0]['name']}";
    $queryString = "INSERT INTO `events` (`name`, `punkte`, `datum`)
                    VALUES      ({$sql->quote($message)}, {$sql->quote(-$dkpOut)}, NOW())";
    $sql->exec($queryString);
    $eventId = $sql->getLastInsertId();

    $queryString = "INSERT INTO `dkp` (`eventId`, `memberId`)
                    VALUES      ({$sql->quote($eventId)}, {$sql->quote($userId)})";
    $sql->exec($queryString);

    // dem spender ruhm geben
    $queryString = "UPDATE  `members`
                    SET     `ruhm` = `ruhm` + {$sql->quote($honor)}
                    WHERE   `id`= {$userId}";
    $sql->exec($queryString);

    // in ruhm tabelle eintragen
    $queryString = "INSERT INTO `honor` (`memberId`, `eventId`, `value`, `created`)
                    VALUES      ({$sql->quote($userId)}, {$sql->quote($eventId)}, {$sql->quote($honor)}, NOW())";
    $sql->exec($queryString);


    // gespendeten Sachen erledigen
    $message = "Spende von Mitglied: {$_SESSION['name']}";
    $queryString = "INSERT INTO `events` (`name`, `punkte`, `datum`)
                    VALUES      ({$sql->quote($message)}, {$sql->quote($donation)}, NOW())";
    $sql->exec($queryString);
    $eventId = $sql->getLastInsertId();

    $queryString = "INSERT INTO `dkp` (`eventId`, `memberId`)
                    VALUES      ({$sql->quote($eventId)}, {$sql->quote($donateId)})";
    $sql->exec($queryString);

    die('ok');
}