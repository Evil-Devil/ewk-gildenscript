<?php
include 'bootstrap.php';

use Ewigkeit\Editor\EditorService;
use Ewigkeit\Sql\SqlAdapter;

$itemId = 0;
$charClass = 0;
$level = 0;
$name = null;
$part = 0;
$quality = 0;
$set = 0;
$icon = -1;


if (isset($_POST['id']) && (int)$_POST['id'] > 0) {
    $itemId = (int)$_POST['id'];
}
if (isset($_POST['charclass']) && (int)$_POST['charclass'] > 0) {
    $charClass = (int)$_POST['charclass'];
}
if (isset($_POST['level']) && (int)$_POST['level'] > 0) {
    $level = (int)$_POST['level'];
}
if (isset($_POST['name']) && trim($_POST['name']) != '') {
    $name = trim($_POST['name']);
}
if (isset($_POST['part']) && (int)$_POST['part'] > 0) {
    $part = (int)$_POST['part'];
}
if (isset($_POST['quality']) && (int)$_POST['quality'] > 0) {
    $quality = (int)$_POST['quality'];
}
if (isset($_POST['set']) && (int)$_POST['set'] > 0) {
    $set = (int)$_POST['set'];
}
if (isset($_POST['icon']) && (int)$_POST['icon'] >= 0) {
    $icon = (int)$_POST['icon'];
}

if ($itemId === 0 && $name === null) {
    die();
}

$setName = '';
$setDescription = '';

$sql = SqlAdapter::getInstance();
// shall we update or create a new set?
if (isset($_POST['set_edit']) && (int)$_POST['set_edit'] == 1
    && isset($_POST['set_name']) && trim($_POST['set_name']) != '') {
    $setName = trim($_POST['set_name']);
    if (isset($_POST['set_description'])) {
        $setDescription = trim($_POST['set_description']);
    }

    if ($set <= 0) {
        $queryString = "INSERT  INTO    fw_set (`id`, `name`, `description`, `quality_id`, `charclass_id`)
                        VALUES  ({$set}, {$sql->quote($setName)}, {$sql->quote($setDescription)}, {$quality}, {$charClass})";
        $result = $sql->exec($queryString);        
        $set = $sql->getLastInsertId();
    } else {
        $queryString = "UPDATE  fw_set
                        SET     `name` = {$sql->quote($setName)},
                                `description` = {$sql->quote($setDescription)},
                                `quality_id` = {$quality},
                                `charclass_id` = {$charClass}
                        WHERE   `id` = {$set}";
        $result = $sql->exec($queryString);
    }
}

// save the stuff :D
if ($itemId <= 0) {
    $queryString = "INSERT  INTO    fw_item
                            (`id`, `name`, `set_id`, `quality_id`, `charclass_id`, `part_id`, `level`, `icon_index`)
                    VALUES  ({$itemId}, {$sql->quote($name)}, {$set}, {$quality}, {$charClass}, {$part}, {$level}, {$icon})";
    $result = $sql->exec($queryString);
    $itemId = $sql->getLastInsertId();
} else {
    $queryString = "UPDATE  fw_item
                    SET     `name` = {$sql->quote($name)},
                            `set_id` = {$set},
                            `quality_id` = {$quality},
                            `charclass_id` = {$charClass},
                            `part_id` = {$part},
                            `level` = {$level},
                            `icon_index` = {$icon}
                    WHERE   `id` = {$itemId}";
    $result = $sql->exec($queryString);
}
echo "Gespeichert";