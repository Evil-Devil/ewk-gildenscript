<?php
use Ewigkeit\Sql\SqlAdapter;

require_once 'Member.php';

class MemberAdapter
{

    const ACTIVE = 1;

    const INACTIVE = 0;
    private $sql = null;
    private $members = null;

    public function __construct()    {
        $this->sql = SqlAdapter::getInstance();
    }

    public function setSqlAdapter($sql)
    {
        $this->sql = $sql;
    }

    public function getAll($forceReload = false, $status = null)
    {
        if ($this->members === null || $forceReload) {            
            $this->loadMembers($status);
        }        
        return $this->members;
    }

    public function existsName($name)
    {
        $queryString = "SELECT	`id`
						FROM	`members`
						WHERE	`name` = {$this->sql->quote($name)}
						AND		`austritt` IS NULL";        
        $result = $this->sql->query($queryString);
        
        if ($result !== null) {            
            if (sizeof($result) > 0) {                
                return true;
            }            
            return false;
        }        
        return false;
    }

    public function addMember(Member $member)
    {
        $queryString = "INSERT INTO `members` (`name`, `eintritt`, `twink`, `twink_von`, `rolle`)
						VALUES ({$this->sql->quote($member->name)}, 
								{$this->sql->quote($member->joinDatetime)},
								{$this->sql->quote($member->isTwink)},
								{$this->sql->quote($member->twinkOf)},
								{$this->sql->quote($member->role)}
						)
						ON DUPLICATE KEY UPDATE
							`status` = 1,
							`austritt` = NULL,
							`wiedereintritt` = CURRENT_TIMESTAMP()";        
        $result = $this->sql->exec($queryString);

        // write also to characters
        $queryString = "INSERT INTO characters (member_id, name, level, is_twink, contribution, weekly_contribution)
                        VALUES (
                          {$this->sql->quote($member->name)},
                          
                        )";
    }

    public function removeMember(Member $member)
    {
        $queryString = "UPDATE	`members`
						SET		`austritt` = {$this->sql->quote($member->leaveDatetime)},
								`status` = {$this->sql->quote($member->status)},
								`ruhm` = {$this->sql->quote($member->fame)}
						WHERE	`id` = {$this->sql->quote($member->getId())}";        
        $result = $this->sql->exec($queryString);
    }

    public function saveMember(Member $member)
    {}

    public function getCurrentHoliday($memberId)
    {
        $queryString = "SELECT	tsStart, tsEnd, days
						FROM	holidays
						WHERE	memberId = {$memberId}
						AND		tsEnd > " . time();        
        $result = $this->sql->query($queryString);
                
        if ((bool)$result === false) {            
            return false;
        }        
        return (object) $result[0];
    }

    public function takeHolidays($memberId, $days)
    {
        $queryString = "UPDATE	members
						SET		urlaub = 1,
								urlaubs_tage = urlaubs_tage - {$this->sql->quote($days)}
						WHERE	id = {$memberId}";
        
        $result = $this->sql->exec($queryString);
        
        // muss hier weg!        
        $tsStart = time();        
        $tsEnd = $tsStart + ($days * 86400);
        
        $queryString = "INSERT INTO holidays (memberId, tsStart, tsEnd, days)
						VALUES	({$memberId}, {$tsStart}, {$tsEnd}, {$days})";        
        $result = $this->sql->exec($queryString);
    }

    public function loadMember($memberId)
    {
        if ($memberId <= 0) {            
            return false;
        }
        
        $queryString = "SELECT	id, name, level, status, eintritt, austritt, wiedereintritt,
								twink, twink_von, rolle, urlaub, urlaubs_tage, ruhm
						FROM	members
						WHERE	id = {$memberId}";        
        $result = $this->sql->query($queryString);
                        
        if ($result === false) {            
            return false;
        }
        
        $res = $result[0];
        
        $member = new Member();
        $member->setId($res['id']);        
        $member->name = $res['name'];        
        $member->level = $res['level'];        
        $member->status = $res['status'];        
        $member->joinDatetime = $res['eintritt'];        
        $member->leaveDatetime = $res['austritt'];        
        $member->rejoinDatetime = $res['wiedereintritt'];        
        $member->isTwink = $res['twink'];        
        $member->twinkOf = $res['twink_von'];        
        $member->role = $res['rolle'];        
        $member->isHoliday = (bool) $res['urlaub'];        
        $member->holidays = $res['urlaubs_tage'];        
        $member->fame = $res['ruhm'];
        
        unset($res);        
        unset($result);
        
        return $member;
    }

    private function loadMembers($status = null)
    {
        $queryString = "SELECT	id, name, level, status, eintritt, austritt, wiedereintritt,
								twink, twink_von, rolle, urlaub, urlaubs_tage, ruhm
						FROM	members";
        
        if ($status !== null) {
            
            $queryString .= " WHERE `status` = {$status}";
        }
        
        $queryString .= " ORDER BY `name` ASC";
        
        $result = $this->sql->query($queryString);
        
        if ($result === false) {
            
            return false;
        }
        
        foreach ($result as $res) {
            
            $member = new Member();
            
            $member->setId($res['id']);
            
            $member->name = $res['name'];
            
            $member->level = $res['level'];
            
            $member->status = $res['status'];
            
            $member->joinDatetime = $res['eintritt'];
            
            $member->leaveDatetime = $res['austritt'];
            
            $member->rejoinDatetime = $res['wiedereintritt'];
            
            $member->isTwink = $res['twink'];
            
            $member->twinkOf = $res['twink_von'];
            
            $member->role = $res['rolle'];
            
            $member->isHoliday = $res['urlaub'];
            
            $member->holidays = $res['urlaubs_tage'];
            
            $member->fame = $res['ruhm'];
            
            $this->members[] = $member;
        }
        
        unset($result);
    }
    
    
}