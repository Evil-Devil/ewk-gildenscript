<?php
include('config/mysql_connect.php');

$query = '';

$isAjax = false;

// =================================
// GILDENBANK
// ---------------------------------
if (isset($_POST['delete'])) {

    mysql_query("DELETE FROM `bank_item` WHERE id='" . $_POST['id'] . "'") or
    die(mysql_error());

    $message = "Item mit der ID " . $_POST['id'] . " erfolgreich gelöscht";
}
// ---------------------------------

// =================================
// IRGENDWAS ASYNCHRONES
// ---------------------------------
if (isset($_POST['action'])) {

    if ($_POST['action'] == 'ajax') {

        $isAjax = true;
    }

    unset($_POST['action']);
}

if (isset($_POST['tabelle'])) {

    $tabelle = $_POST['tabelle'];

    $write2DB = true;

    // zusätzliche Aktionen für bestimmte Tabellen

    switch ($tabelle) {

        case 'events':

            // datum für die events tabelle formatieren

            $_POST['datum'] = date('Y-m-d H:i:s', strtotime($_POST['datum']));
            break;

        case 'items_buy':
            if (isset($_POST['menge'])) {
                $itemId = $_POST['itemId'];
                // menge und preis des bestellten items ermitteln
                $query = "SELECT  `bi`.`menge` - IFNULL(SUM(`ib`.`menge`), 0) `menge`, `bi`.`preis`, `periode`, `max`
					      FROM `bank_item` `bi`
					        LEFT JOIN `items_buy` `ib` ON `bi`.`id` = `ib`.`itemId`
					      WHERE `bi`.`id` = {$itemId}
					      AND `ib`.`status` = 'vorgemerkt'";
                $item = mysql_fetch_object(mysql_query($query));

                if ($_POST['menge'] > $item->menge) {
                    $write2DB = false;
                    $message = 'Das Item ist nicht in der von dir gewünschten Menge verfügbar!';
                    break;
                }

                // saldo des bestellers ermitteln

                $query = "SELECT `memberId`, SUM(`Punkte`) `budget` FROM (
                        SELECT `memberId`, SUM(`ib`.`menge` * `ib`.`preis`) * -1 `Punkte` FROM `items_buy` `ib`
                        INNER JOIN `bank_item` `bi` ON `bi`.`id` = `ib`.`itemId`
                        WHERE `ib`.`status` <> 'storno'
                        GROUP BY `memberId`
                      UNION
                        SELECT `memberId`, SUM(`punkte`) `Punkte` FROM `dkp`
                        LEFT JOIN `events` `e` ON `e`.`id` = `dkp`.`eventId`
                        GROUP BY `memberId`
					UNION
                        SELECT `memberId`, SUM(`punkte`) `Punkte` FROM `bonus`
						GROUP BY `memberId`
                    ) `b`
                    WHERE `memberId` = " . $member['id'] . "
                    GROUP BY `memberId`";

                $user = mysql_fetch_object(mysql_query($query));

                $n1 = $_POST['menge'];
                $n2 = $item->preis;
                $test = $n1 * $n2;
                if ($test > $user->budget) {
                    $write2DB = false;
                    $message = 'Der Preis des Items überschreitet dein Budget!';
                    break;
                }

                $query = "SELECT SUM(`menge`) `menge` FROM `items_buy`

                    WHERE `memberId` = " . $member['id'] . "
                        AND `itemId` = $itemId
						AND `status` <> 'storno'
                        AND `datum` >= DATE_SUB(CURDATE() , INTERVAL ' . $item->periode . ' DAY)
                    ORDER BY `datum` DESC";

                $buy = mysql_fetch_object(mysql_query($query));

                if ($item->max > 0 && ($buy->menge + $_POST['menge']) > $item->max) {

                    $write2DB = false;
                    $message = 'Das Limit wurde überschritten!!<br> ' .
                        $item->max . ' mal / ' . $item->periode .
                        ' Tage (0 = kein Limit!)';
                    break;
                }

                $message = 'Deine Bestellung wurde vorgemerkt! Du hast ' . $test .
                    ' DKPs ausgegeben';
            }

            // bei bestätigung der Betsellung aus der Bank nehmen

            if (isset($_POST['status']) && $_POST['status'] == 'gebucht') {
                $buyId = $_POST['id'];
                $query = "SELECT `itemId`, `menge` FROM `items_buy` WHERE `id` = $buyId";
                $buy = mysql_fetch_object(mysql_query($query));
                mysql_query("UPDATE `bank_item` SET `menge` = `menge` - $buy->menge WHERE `Id` = $buy->itemId");
            }

            break;

        default:

            // nothing

            break;
    }

    // nur in Datenbank schreiben, wenn vorangegangene Bedingungen erfüllt
    if ($write2DB) {
        $query = dbInsert($_POST, $isAjax);
    }
}

function makeSqlString($wert)
{
    $wert = mysql_real_escape_string($wert);
    return (is_numeric($wert))
        ? $wert
        : "'$wert'";
}

function dbInsert($data, $isAjax)
{
    $cols = array();
    $rows = array();
    $upd = array();
    $tabelle = $data['tabelle'];
    unset($data['tabelle']);

    foreach ($data as $col => $row) {

        if (sizeof($cols) > 0) {
            $upd[] = '`' . $col . '` = VALUES(' . $col . ')';
        }

        $cols[] = "`$col`";

        if (!is_array($row)) {
            $row = array(
                $row
            );
        }

        foreach ($row as $i => $r) {
            $rows[$i][$col] = makeSqlString($r);
        }
    }

    $werte = array();

    foreach ($rows as $row) {
        if (count($cols) == count($row)) {
            $werte[] = '(' . join(',', $row) . ')';
        }
    }

    $spalten = join(',', $cols);
    $werte = join(',', $werte);
    $upd = join(',', $upd);
    $query = "INSERT `$tabelle` ($spalten) VALUES $werte ON DUPLICATE KEY UPDATE $upd";
    mysql_query($query);

    // bei Ajax Anfragen die InsertId als Antwort

    if ($isAjax) {

        echo mysql_insert_id();
    }

    return $query;
}

?>