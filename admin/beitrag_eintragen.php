<?php
if ($member['rolle'] <= 2) {
    die('Du hast für diesen Bereich keinen Zugriff :p');    
}


// werte speichern
if (isset($_POST['act']) && $_POST['act'] == 'save') {
    //echo '<pre>'.print_r($_POST, 1).'</pre>';
    
    // did we load old data?
    if (isset($_POST['reloaddate']) && !empty($_POST['reloaddate'])) {
        // delete everything old
        $queryString = "DELETE  FROM    beitrag
                        WHERE   DATE(woche) = '{$_POST['reloaddate']}'";
        $sql->exec($queryString);
    }
    
    $contributions = array();
    foreach ($_POST['memberId'] as $key => $memberId) {
        $contribution = (int)$_POST['beitrag'][$key];
        $increase = (int)$_POST['zuwachs'][$key];
        $contributions[] = "({$memberId}, '{$_POST['date']} 00:00:00', {$contribution}, {$increase})";
    }
    
    if (sizeof($contributions) > 0) {
        $queryString = "INSERT INTO beitrag (memberId, woche, beitrag, zuwachs)
                        VALUES ".implode(",\n", $contributions);
        //echo '<pre>'.print_r($queryString, 1).'</pre>';
        $sql->exec($queryString);
    }
    // do a redirect :O
    header("Location: index.php?dir=admin&site=beitrag_eintragen");
    die();
}

// --------------------------------------------------------------------------------------------------------------------
$contributionInfo = array();
$lastUpdatedWeek = null;


$queryString = "SELECT  DATE_FORMAT(MAX(DATE(woche)), '%d.%m.%Y') as lastWeek
                FROM    beitrag";
$result = $sql->query($queryString);
$lastUpdatedWeek = $result[0]['lastWeek'];

$queryString = "SELECT  m.id, m.name, IFNULL(b1.beitrag, 0) as contribution,
                        0 as increase, 0 as weekContribution, 0 as `index`
                FROM    members m
                    LEFT JOIN (
                        SELECT  memberId, beitrag
                        FROM    beitrag
                        WHERE   DATE_FORMAT(DATE(woche), '%d.%m.%Y') = '{$lastUpdatedWeek}'
                    ) b1 ON m.id = b1.memberId
                WHERE   m.status = 1
                ORDER BY    m.name";
$result = $sql->query($queryString, \PDO::FETCH_OBJ);
$index = 0;
foreach ($result as $res) {
    $res->index = $index++;
    $contributionInfo[$res->id] = $res;
}
unset($index);
unset($result);
unset($res);
 
//echo '<pre>'.print_r($result, 1).'</pre>';


/*$query = "SELECT `id`, `name` FROM `members` WHERE `status` = 1 ORDER BY `name` ASC"; // LIMIT ".(($seite-1)*$eps).", ".$eps;
$users = mysql_query($query);*/
$maxWeeks = 10;
$startDateTime = new \DateTime();
$dayOfWeek = date('w', $startDateTime->getTimestamp());

if ($dayOfWeek > 0) {
    $startDateTime->sub(new \DateInterval("P{$dayOfWeek}D"));
}

$endDateTime = new \DateTime($startDateTime->format('Y-m-d H:i:s'), $startDateTime->getTimezone());
$endDateTime->add(new \DateInterval('P6D'));
$sevenDays = new \DateInterval('P7D');

// alte beiträge laden, wenn welche angefordert wurden
$reloadDate = null;
$reloadInfo = array();
if (isset($_GET['act']) && $_GET['act'] == 'load' && isset($_GET['date']) && $_GET['date'] !== '') {
    $reloadDate = $_GET['date'];
    
    $queryString = "SELECT  memberId, beitrag as contribution, zuwachs as increase
                    FROM    beitrag
                    WHERE   DATE(woche) = '{$reloadDate}'";
    $result = $sql->query($queryString, \PDO::FETCH_OBJ);    
    foreach ($result as $res) {
        if (isset($contributionInfo[$res->memberId])) {
            $contributionInfo[$res->memberId]->contribution = ($res->contribution - $res->increase);
            $contributionInfo[$res->memberId]->weekContribution = $res->contribution;
            $contributionInfo[$res->memberId]->increase = $res->increase;
        }
    }
    
}

?>

<script>

$('document').ready(function() {
	// tastatureingabe für alle eingabefelder mit attribut 'tabindex' abfangen
	$('form input[tabindex]').keydown(function(e) {
		// auf <enter> prüfen
		if (e.keyCode == 13) {
			// eingabefeld mit tabindex des aktuellen + 1 und inhalt auswählen
			$('form input[tabindex='+($(this).attr('tabindex')+1)+']').select();
			// standardaktion (vom <enter>-knopf) verhindern
			e.preventDefault();
			return false;
		}
	});
});

function weekContribution(frm, index) {
	var totalContribution = parseInt(frm['contribution[]'][index].value);
	var newTotalContribution = parseInt(frm['beitrag[]'][index].value);

	frm['zuwachs[]'][index].value = 0;
	if (newTotalContribution > 0) {	
		   frm['zuwachs[]'][index].value = (newTotalContribution - totalContribution);
	}		
}
</script>
<h1>Mitglieder Beitrag eintragen</h1>
<div>
    <h2>Frühester Beitrags Eintrag: <?= $lastUpdatedWeek ?></h2>
    <form action="index.php" method="post" onsubmit="return confirm('Daten abschicken?');">
    <input type="hidden" name="dir" value="admin" />
    <input type="hidden" name="site" value="beitrag_eintragen" />
    <input type="hidden" name="act" value="save" />
    <input type="hidden" name="reloaddate" value="<?= $reloadDate ?>" />
    <fieldset>
    <strong>Wochen-Auswahl</strong>
    <ul>
        <?php for ($i=0; $i<$maxWeeks; $i++): ?>        
        <li style="padding: 5px;">
            <input type="radio" name="date" value="<?= $startDateTime->format('Y-m-d') ?>" <?= checked($startDateTime->format('Y-m-d'), $reloadDate) ?> />
            <span style=""><?= $startDateTime->format('\K\W\ W\: d.m.Y'); ?> - <?= $endDateTime->format('d.m.Y'); ?></span>
            <a href="index.php?dir=admin&site=beitrag_eintragen&act=load&date=<?= $startDateTime->format('Y-m-d') ?>">&raquo; Beiträge anzeigen</a>
            <?php if ($i > 0 && $i < 2): ?><hr color="#FF0000" /><?php endif; ?>
        </li>
        <?php
        $startDateTime->sub($sevenDays);
        $endDateTime->sub($sevenDays);
        ?>        
        <?php endfor; ?>        
    </ul>
    </fieldset>
    <fieldset>
    <?php if ($reloadDate !== null): ?>
    <p>
        <h3>Daten für die Woche vom <?= $lastUpdatedWeek ?> geladen</h3>
        <span style="color: #FF0000;">Achtung, derzeit werden die Beiträge aus der bearbeiteten Wochen nicht bis zur aktuellen Woche entsprechend korrigiert - Einzig die jeweilige Woche!
            <br /><strong>Es wird empfohlen nur die letzte bzw. aktuelle Woche zu bearbeiten.</strong></span>
    </p>
    <?php endif; ?>
    <table>
        <tr>
            <th>Mitglied</th>
            <th>Aktuelle Vorwoche</th>
            <th>Ausgewählte Woche</th>
            <th>Wochen-Beitrag</th>
        </tr>
        <?php foreach ($contributionInfo as $key => $val): ?>
        <tr>
            <td><?= $val->name ?></td>
            <td>
                <input type="hidden" name="contribution[]" value="<?= $val->contribution ?>" disabled="disabled" />
                <?= $val->contribution ?>
            </td>
            <td>
                <input type="hidden" name="memberId[]" value="<?= $val->id ?>" />
                <input type="text" name="beitrag[]" tabindex=<?= $val->index ?>" onclick="this.select();" onkeyup="weekContribution(this.form, <?= $val->index ?>);" value="<?= $val->weekContribution ?>" size="6" maxlength="6" style="text-align: right;" />                
            </td>
            <td><input type="text" name="zuwachs[]" value="<?= $val->increase ?>" size="2" readonly="readonly" style="background: #ECECEC;" /></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <button type="submit" tabindex="999">eintragen</button>
    </fieldset>
    </form>
    
</div>