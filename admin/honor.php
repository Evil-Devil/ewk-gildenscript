<?php
use Ewigkeit\Sql\SqlAdapter;

function addHonor($eventName, $memberId, $honor) {

    $sql = SqlAdapter::getInstance();
    // create the event
    $queryString = "INSERT INTO events (name, punkte, datum)
                    VALUES      ({})";


//    $queryString = "INSERT INTO honor (memberId, )"
}

function guildDonateHonor($memberId)
{
    // Gildenspende: 1 DKP = 1 RUHM

    $queryString = "select e.id, e.punkte, dkp.memberId, e.name, e.datum
                    from events e
                    inner join dkp on e.id = dkp.eventId
                    where dkp.memberId = {$memberId}
                    AND e.name like 'Gildenspende%'";
    return SqlAdapter::getInstance()->query($queryString);
}

function memberDonateHonor($memberId)
{
    // Memberspende:
    // 1 DKP = 0.5 RUHM (seit: 01.03.2015)
    // 1 DKP = ??? RUHM (alt)

    $queryString = "select e.id, e.punkte, dkp.memberId, e.name, e.datum
                    from events e
                    inner join dkp on e.id = dkp.eventId
                    where dkp.memberId = {$memberId}
                    and e.name like 'Spende an%'";
    return SqlAdapter::getInstance()->query($queryString);
}

function membersContribution($memberId) {
    $queryString = "select memberId, beitrag from beitrag where woche = (select max(woche) from beitrag) and memberId = {$memberId}";
    $result = SqlAdapter::getInstance()->query($queryString);
    if (count($result) > 0) {
        return $result[0]['beitrag'];
    }
    return 0;
}

// calculate honor based on guild lifetime
function memberLifetimeMonth($entryDateTs)
{
    // jeder monat mitglied = 3 ruhm
    $time = time();
    $currentYear = date('Y', $time);
    $currentMonth = date('m', $time);
    $currentDay = date('d', $time);

    $entryYear = date('Y', $entryDateTs);
    $entryMonth = date('m', $entryDateTs);
    $entryDay = date('d', $entryDateTs);
    $entryMonthLength = 30;
    // not sure about this
    if ($entryMonth == 2) {
        $entryMonthLength = 28;
    }
    $monthDiff = 0;

    if ($entryYear == $currentYear) {
        $monthDiff = $currentMonth - $entryMonth;
    } else {
        $yearDiff = ($currentYear - $entryYear);
        if ($yearDiff > 0) {
            $yearDiff--;
        }
        $monthDiff = $currentMonth + ( (12-$entryMonth) + (12 * $yearDiff)) - 1;
    }
    if ($monthDiff > 0) {
        if ($entryDay == $currentDay || $entryDay < $currentDay) {
            $monthDiff++;
        }
    }

    return $monthDiff;
}

// retrieve all members
$queryString = "SELECT	m.id, 0 as avgContribution, m.name, m.`eintritt`,
                        if (m.`wiedereintritt` > m.`eintritt`, unix_timestamp(m.`wiedereintritt`), unix_timestamp(m.`eintritt`))  as ts_eintritt,
                        (UNIX_TIMESTAMP() - if (m.`wiedereintritt` > m.`eintritt`, unix_timestamp(m.`wiedereintritt`), unix_timestamp(m.`eintritt`))) as ts_lifetime,
                        0 as contribution, 0 as attends, ruhm
                FROM    members m
                WHERE   m.status = 1
                AND     m.twink = 0";
$allMembersTmp = SqlAdapter::getInstance()->query($queryString);
$allMembers = array();

foreach ($allMembersTmp as $row) {
    // for members x, retrieve honor points
    $row['guildDonateHonor'] = guildDonateHonor($row['id']);
    $row['memberDonateHonor'] = memberDonateHonor($row['id']);
    $row['lifetimeMonth'] = memberLifetimeMonth($row['ts_eintritt']);
    $row['lifetimeHonor'] = $row['lifetimeMonth'] * 3;
    $row['contribution'] = membersContribution($row['id']);

    // count honor
    $honor = 0;
    $memberDonateHonor = 0;
    $guildDonateHonor = 0;
    $lifetimeHonor = $row['lifetimeHonor'];
    // Beitrag Ruhm, 2 Ruhm alle 5.000 Beitrag
    $contributionHonor = floor($row['contribution'] / 5000) * 2;


    // Gildenspende: 1 DKP = 1 RUHM
    $row['honors']['guildDonate'] = 0;
    foreach ($row['guildDonateHonor'] as $entry) {
        $honor += abs($entry['punkte']);
        $row['honors']['guildDonate'] += abs($entry['punkte']);
        $guildDonateHonor += abs($entry['punkte']);
    }

    // Memberspende:
    // 1 DKP = 0.5 RUHM (seit: )
    // 1 DKP = ??? RUHM (alt)
    $row['honors']['memberDonate'] = 0;
    foreach ($row['memberDonateHonor'] as $entry) {
        $memberDonateHonor += abs($entry['punkte'] * 0.5);
        if ($entry['datum'] >= '2015-03-01 00:00:00') {
            $honor += abs($entry['punkte'] * 0.5);
            $row['honors']['memberDonate'] = abs($entry['punkte'] * 0.5);
        } else {
            $honor += abs($entry['punkte'] * 0.8);
            $row['honors']['memberDonate'] = abs($entry['punkte'] * 0.8);
        }
    }

//    $honor += ($row['lifetimeMonth'] * 3);
    $row['honor'] = ($honor + $memberDonateHonor + $guildDonateHonor + $lifetimeHonor + $contributionHonor);
    $row['memberDonateHonor'] = $memberDonateHonor;
    $row['guildDonateHonor'] = $guildDonateHonor;
    $row['contributionHonor'] = $contributionHonor;

    $honor += ($row['lifetimeMonth'] * 3);
    $row['honors']['lifetimeMonth'] = $row['lifetimeMonth'] * 3;
    $row['honor'] = $honor;
    $allMembers[$row['id']] = $row;
}
unset($allMembersTmp);

#echo '<pre>'.print_r($allMembers,1).'</pre>';


?>
<h1>Ruhm-System</h1>
<form>
    <fieldset>
        <legend>Ruhm-Ereignis eintragen</legend>

        <label>Mitglied:</label>
        <select name="memberId">
            <options value="0">-Bitte auswählen-</options>
            <?php foreach ($allMembers as $key => $val): ?>
                <option value="<?= $val['id'] ?>"><?= $val['name'] ?></option>
            <?php endforeach; ?>
        </select>
        <br/>
        <label>Ereignis</label>
        <select name="event_name">
            <option value="">-Bitte auswählen-</option>
            <option value="support">Support (Geldspende)</option>
        </select>
        <br/>
        <label>Ruhm</label>
        <input type="text" name="honor" value="" maxlength="3" size="2">
    </fieldset>
</form>
<hr/>
<fieldset>
    <a href="">Ruhm aller komplett neu berechnen - basierend auf aktueller Events - bitte nur einmal tun!</a>

    <p>Nachfolgend Ruhmvorschau dieser Aktion</p>
    <table border="1">
        <thead>
        <tr>
            <th>Member</th>
            <th>Eintritt</th>
            <th>Monate</th>
            <th>Beitrag</th>
            <th>Aktueller Ruhm</th>
            <th>Ruhm Mitgliedschaft</th>
            <th>Ruhm Beitrag</th>
            <th>Ruhm Gildenspende</th>
            <th>Ruhm Mitgliedspende</th>
            <th>Neuer Ruhm</th>
            <th>Ruhm Details:</th>
            <th>Guild Donate</th>
            <th>Member Donate</th>
            <th>Mitgliedsdauer</th>
            <th>Bonus Monate</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($allMembers as $oneMember): ?>
            <tr>
                <td><?= $oneMember['name'] ?></td>
                <td><?= $oneMember['eintritt'] ?></td>
                <td><?= $oneMember['lifetimeMonth'] ?></td>
                <td><?= $oneMember['contribution'] ?></td>
                <td><?= $oneMember['ruhm'] ?></td>
                <td><?= $oneMember['lifetimeHonor'] ?></td>
                <td><?= $oneMember['contributionHonor'] ?></td>
                <td><?= $oneMember['guildDonateHonor'] ?></td>
                <td><?= $oneMember['memberDonateHonor'] ?></td>
                <td><?= $oneMember['honor'] ?></td>
                <td></td>
                <td><?= $oneMember['honors']['guildDonate'] ?></td>
                <td><?= $oneMember['honors']['memberDonate'] ?></td>
                <td><?= $oneMember['honors']['lifetimeMonth'] ?></td>
                <td></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</fieldset>