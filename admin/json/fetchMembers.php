<?php
/*
 * diese datei wird aktuell nicht mehr genutzt, da es kein Core Connect mehr gibt
 */

die('STOP!');

/* Verbindung zur Datenbank herstellen */
include('config/mysql_connect.php');

/* alle Member bei Perfect World auslesen */
$array = json_decode(file_get_contents("http://core.de.perfectworld.eu/guild/getmembers?guild_id=48-233497-fw"), true);

/* Status aller Member auf 0 setzen */
mysql_query("UPDATE `members` SET `status` = 0");

/* alle Member einzeln in Schleife durchalufen */
foreach ($array['data'] as $key => $value) {
	/* Neue Memberhinzufügen und Status vorhandener wieder auf 1 setzen */
	mysql_query("INSERT `members` (`name`, `level`) VALUES ('".$value['username']."', ".$value['level'].") ON DUPLICATE KEY UPDATE `status` = 1, `wiedereintritt` = IF(`austritt` IS NOT NULL, CURRENT_TIMESTAMP, NULL), `austritt` = NULL");
}

/* Austrittsdatum ausgetretener Member eintragen */
mysql_query("UPDATE `members` SET `austritt` = CURRENT_TIMESTAMP WHERE `status` = 0 AND `austritt` IS NULL");

/* Verbindung zur Datenbank schliessen */
mysql_close($link);
?>