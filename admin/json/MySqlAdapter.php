<?php

class MySqlAdapter
{
    private $connection = null;
    private $server = null;
    private $user = null;
    private $pass = null;
    private $database = null;

    public function __construct($server, $user, $pass, $database = null)
    {
        $this->server = $server;
        $this->user = $user;
        $this->pass = $pass;
        $this->database = $database;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function connect()
    {
        try {
            $this->connection = new PDO(
                "mysql:host={$this->server};dbname={$this->database}",
                $this->user, $this->pass
            );
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            // TODO: Add Error Handling
            die($e->getMessage());
        }
    }

    public function quote($value)
    {
        return $this->connection->quote($value);
    }

    // used for delete, update, insert
    public function exec($query)
    {
        return $this->connection->exec($query);
    }

    // only for select
    public function query($query)
    {
        $result = null;
        try {
            $result = $this->connection->query($query);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
        if ($result !== false) {
            return $result->fetchAll(PDO::FETCH_ASSOC);
        }
        return $result;
    }
}