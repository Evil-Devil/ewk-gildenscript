<?php
require_once 'functions/MySqlAdapter.php';

class SqlAdapter {

	private static $instance = null;
	private $adapter = null;

	private function __construct() {
	}
	
	public static function getInstance() {
		if (static::$instance === null) {
			static::$instance = new SqlAdapter();
			static::$instance->createConnection();
		}
		return static::$instance->getAdapter();
	}
	
	private function getAdapter() {
		return $this->adapter;
	}
	
	private function createConnection() {
		// don't like this :(
		require_once 'config/config.php';		
		$this->adapter = new MySqlAdapter(DB_SERVER, DB_USER, DB_PASS, DB_SCHEME);
		$this->adapter->connect();
	}
}
