<?php
require_once 'functions/AbstractContribution.php';

class GuildContribution extends AbstractContribution {
	
	private $average = null;
	
	public function getAverageForMember($memberId) {
		if ($this->average === null) {
			$this->determineAverageForMember($memberId);			
		}
		return $this->average;
	}
	
	private function determineAverageForMember($memberId) {
		$memberId = (int)$memberId;
		$this->average = 0;
		
		$queryString = "SELECT	AVG(`beitrag`) as avgContribution
						FROM	`bonus`
						WHERE	`memberId` = {$this->sqlAdapter->quote($memberId)}
						AND		`datum` >= DATE_SUB( CURDATE( ) , INTERVAL {$this->intervalDays} DAY )";
		$result = $this->sqlAdapter->query($queryString);
		
		if ($result !== false) {
			$this->average = round($result[0]['avgContribution']);
		}
	}
	
	public function getForMember($memberId) {
		$memberId = (int)$memberId;
		
		$queryString = "SELECT	`beitrag` as contribution, `datum` as date, UNIX_TIMESTAMP(`datum`) tsDatetime
						FROM	`bonus`
						WHERE	`memberId` = {$this->sqlAdapter->quote($memberId)}
						AND		`datum` >= DATE_SUB( CURDATE( ) , INTERVAL {$this->intervalDays} DAY )
						ORDER BY `datum` DESC";
		$result = $this->sqlAdapter->query($queryString);
		if ($result !== false) {
			return $result;
		}
		return array();
	}
}