<?php
class Member {
	private $id = 0;
	public $name = null;
	public $user = 0;
	public $pass = null;
	public $level = 0;
	public $status = 0;
	public $joinDatetime = null;
	public $leaveDatetime = null;
	public $rejoinDatetime = null;
	public $isTwink = false;
	public $twinkOf = null;
	public $role = 0;
	public $holidays = 0;
	public $fame = 0;
	
	public function __construct($id = 0) {
		$this->id = $id;
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function setId($id) {
		$this->id = (int)$id;
	}
}
