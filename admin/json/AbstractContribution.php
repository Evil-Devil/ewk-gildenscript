<?php
class AbstractContribution {
	private $defaultIntervalDays = 60;
	
	protected $intervalDays = 0;
	protected $sqlAdapter = null;
	
	public function __construct() {
		$this->intervalDays = $this->defaultIntervalDays;
		$this->sqlAdapter = SqlAdapter::getInstance();
	}
	
	public function setIntervalDays($days) {
		$this->intervalDays = (int)$days;
	}
	
	public function getIntervalDays() {
		return $this->intervalDays;
	}
	
	protected function getSqlAdapter() {
		return $this->sqlAdapter;
	}
}