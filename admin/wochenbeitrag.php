<?php
use Ewigkeit\Sql\SqlAdapter;
// jder tag urlaub zählt für 13 beitrag

function updateWeekContribution() {
    $sql = SqlAdapter::getInstance();
    $weekContribution = array();
    
    // retrieve data
    $contribution = calculateWeekContribution();
    
    $holidays = determineHolidays();
    
    // apply holidays to contribution
    foreach ($contribution as $key => $val) {
        if (isset($holidays[$val['id']])) {
            $contribution[$key]['vacation'] = $holidays[$val['id']];
        }
    }
    
    
    foreach ($contribution as $contrib) {
        $weekContribution[$contrib['id']] = array(
        	'dkp' => calculateBonusDkp($contrib['increase'], $contrib['vacation']),
            'old5kBonus' => 0,
            'new5kBonus' => ($contrib['5k_bonus'] * 2),
            'contribution' => $contrib['increase'],
        );
    }
    
    // retrieve old bonus values
    $queryString = "SELECT  member_id as memberId, wert as ruhm
                    FROM    event_zuordnung
                    WHERE   wert > 0";
    $currentBonus = $sql->query($queryString);    
    foreach ($currentBonus as $val) {
        if (isset($weekContribution[$val['memberId']])) {
            $weekContribution[$val['memberId']]['old5kBonus'] = $val['ruhm'];
        }
    }
    

    $time = date('Y-m-d h:i:s');
    // insert values    
    foreach ($weekContribution as $memberId => $contrib) {
        // contribution 5000
        if ($contrib['new5kBonus'] > 0) {
            $queryString = "INSERT  INTO event_zuordnung (member_id, wert)
                            VALUES  ({$memberId}, {$contrib['new5kBonus']})
                            ON DUPLICATE KEY
                            UPDATE  wert = {$contrib['new5kBonus']}";
            $sql->exec($queryString);
        }
        
        // dkp for week bonus
        $queryString = "INSERT  INTO `bonus` (`punkte`, `memberId`, `datum`, `name`, `beitrag`)
                        VALUES  ({$contrib['dkp']}, {$memberId}, {$sql->quote($time)}, 'Wochenbonus {$contrib['dkp']}', {$contrib['contribution']})";
        $sql->exec($queryString);
        
        
        // honor
        $honorDiff = $contrib['new5kBonus'] - $contrib['old5kBonus'];
        if ($honorDiff > 0) {        
            $queryString = "UPDATE  members
                            SET     ruhm = ruhm + {$honorDiff}
                            WHERE   id = {$memberId}";
            $sql->exec($queryString);
        }
        
    }
    
    
    
    //$queryString = "INSERT `event_zuordnung` (`member_id`, `wert`) VALUES (".$row['id'].", ".$row['Ruhm (5000)'].") ON DUPLICATE KEY UPDATE `wert` =".$row['Ruhm (5000)']."";
    
    
}

function calculateBonusDkp($contribution, $vacationDays) {
    $reduction = $vacationDays * 13;   

    if ($contribution < (88 - $reduction)) {
        return -5;
    }
    if ($contribution >= 88 && $contribution < 130) {
        return 0;
    }    
    if ($contribution >= 130 && $contribution < 300) {
        return 1;
    }    
    if ($contribution >= 300) {
        return 2;
    }
    return 0;
}

function calculateWeekContribution() {
    $queryString = "SELECT  m.id, m.name, FLOOR(b2.beitrag / 5000) as 5k_bonus, b2.zuwachs as increase,
                            DATE_FORMAT(lastWeek, '%d.%m.%Y') as lastWeek, 0 as holidays
                    FROM    members m, (
                                SELECT MAX(DATE(woche)) as lastWeek
                                FROM    beitrag
                            ) b1,
                            beitrag b2
                    WHERE   m.status = 1
                    AND     DATE(b2.woche) = b1.lastWeek
                    AND     m.id = b2.memberId
                    ORDER BY    m.name";
    return SqlAdapter::getInstance()->query($queryString);
}

function determineHolidays() {
    $queryString = "SELECT  memberId, days,
                            datediff(FROM_UNIXTIME(tsStart), DATE_SUB(b1.lastWeek, interval 6 day)) as weekStartDiff,
                            datediff(FROM_UNIXTIME(tsEnd), b1.lastWeek) as weekEndDiff
                    FROM    holidays h, (
                                SELECT MAX(DATE(woche)) as lastWeek
                                FROM    beitrag
                            ) b1
                    WHERE   (DATE(FROM_UNIXTIME(tsStart)) < DATE_SUB(b1.lastWeek, interval 6 day)
                            AND DATE(FROM_UNIXTIME(tsEnd)) >= DATE_SUB(b1.lastWeek, interval 6 day))
                    OR      (DATE(FROM_UNIXTIME(tsStart)) >= DATE_SUB(b1.lastWeek, interval 6 day)
                            AND DATE(FROM_UNIXTIME(tsEnd)) > b1.lastWeek)
                    OR      (DATE(FROM_UNIXTIME(tsStart)) >= DATE_SUB(b1.lastWeek, interval 6 day)
                            AND DATE(FROM_UNIXTIME(tsEnd)) <= b1.lastWeek)";
    $result = SqlAdapter::getInstance()->query($queryString);    
    $tmp = array();    
    //echo '<pre>'.print_r($result, 1).'</pre>';
    foreach ($result as $val) {
        $days = 7;
        
        if ($val['weekStartDiff'] < 0) {
            $restDays = $val['days'] + $val['weekStartDiff'];
            if ($val['weekEndDiff'] > 0) {
                $restDays = $days;                
            } else {
                $restDays = $restDays - $val['weekEndDiff'];                
            }
            $days = $restDays;
        } else {            
            if ($val['weekStartDiff'] > 0) {
                $restDays = $val['weekStartDiff'];
            }
            if ($val['weekEndDiff'] < 0) {
                $restDays - $val['weekEndDiff'];
            }
            $days = $restDays;
        }
        $tmp[$val['memberId']] = $days;
        //echo "Holidays for {$val['memberId']} this week: ".$days."<br />";
    }    
    return $tmp;
}

// do the magic ;)
if (isset($_POST['act']) && $_POST['act'] == 'save') {
    updateWeekContribution();
}


$contribution = calculateWeekContribution();
$holidays = determineHolidays();

// apply holidays to contribution
foreach ($contribution as $key => $val) {
    if (isset($holidays[$val['id']])) {        
        $contribution[$key]['holidays'] = $holidays[$val['id']];
    }
}
    
 

$lasttime = "SELECT MAX(`woche`) as woche FROM `beitrag` ORDER BY `beitrag`.`woche`  DESC LIMIT 1";

$beitrag = mysql_query($lasttime);
$btr = mysql_fetch_object($beitrag);


$lasttime1 = "SELECT `datum` FROM `bonus` ORDER BY `datum`  DESC LIMIT 1";

$bonus = mysql_query($lasttime1);
$bns = mysql_fetch_object($bonus);

$fontcolor = '';
if($bns->datum > $btr->woche) {
	$fontcolor = '#008000';
} elseif ($bns->datum < $btr->woche) {
	$fontcolor = '#FF0000';
}	


?>
<style>
.red { color: #FF0000; font-weight: bold; }
.orange { color: #EE7600; font-weight: bold; }
.yellow { color: #FFB90F; font-weight: bold; }
.green { color: #228B22; font-weight: bold; }
</style>

<h1>Mitglieder Aktivität</h1>

<p>Anhand der Zeit erkennt Ihr wann der letzte Beitrag eingetragen wurde, um diesen dann berechnen/eintragen zu lassen.<br> Oder Ihr schaut wann das letzte mal Bonus gegeben wurde. Jetzt auch farblich markiert ob die Zeit aktueller als die vom Beitrag ist.</p>
<ul>
    <li>
        <label>letzte Beitragsaktualisierung:</label>
        <strong><?= $btr->woche; ?></strong>
    </li>
	<li>
        <label>letzte Wochenbonusaktualisierung:</label>
        <strong style="color: <?= $fontcolor; ?>"><?php echo $bns->datum; ?></strong></b>
    </li>
</ul>

<br />
<form id="form_bonus" action="index.php" method="post" onsubmit="return confirm('Daten abschicken?');">
    <input type="hidden" name="dir" value="admin" />
    <input type="hidden" name="site" value="wochenbeitrag" />
    <input type="hidden" name="act" value="save" />
    <p>Bitte prüfe nochmal die Einträge bevor du absendest! <input type="submit">geprüft eintragen</button></p>
    <table id="overview" cellspacing="0" cellpadding="2">
        <thead>
    		<tr>
    			<th valign="top">Mitglied</th>
                <th valign="top">Wochenbeitrag</th>
                <th valign="top">Bonus</th>
                <th valign="top">Ruhm 5000</th>
                <th valign="top">Urlaubstage</th>
            </tr>
        </thead>
        <?php foreach ($contribution as $contrib): ?>
        <tr>
            <td><?= $contrib['name'] ?></td>
            <td align="right"><?= $contrib['increase'] ?></td>
            <td align="right"><?= calculateBonusDkp($contrib['increase'], $contrib['holidays']); ?></td>
            <td align="right"><?= $contrib['5k_bonus'] ?></td>
            <td align="right"><?= $contrib['holidays'] ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
    <p>Bitte prüfe nochmal die Einträge bevor du absendest! <input type="submit">geprüft eintragen</button></p>
</form>
