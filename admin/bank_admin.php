<?php
use Ewigkeit\Bank\BankService;
use Ewigkeit\Bank\Item;
use Ewigkeit\Bank\Category;

$bankService = new \Ewigkeit\Bank\BankService();
$baseCategories = $bankService->fetchBaseCategories();
$allCategories = $bankService->fetchAllCategories();

$periods = array(
    '0'    => 'Immer',
    '7'    => 'Woche',
    '25'   => 'Monat',
    '3650' => 'Immer'
);

$item = new \Ewigkeit\Bank\Item();
// any item to load?
if (isset($_GET['itemId'])) {
    $item = $bankService->fetchItem($_GET['itemId']);
}
?>

<?php if ($item->id === 0): ?>
<form method="post">
    <fieldset id="infobox">
        <legend><b>Bank Kategorien anlegen</b></legend>
        <label for="title">Titel</label>
        <input type="text" name="title">
        <select name="cat">
            <option value="0">Hauptkategorie</option>
            <option value="">Unterkategorie in:</option>
            <?php foreach ($baseCategories as $category): ?>
                <option value="<?php echo $category->id ?>"> - <?php echo $category->title ?></option>
            <?php endforeach; ?>
        </select>
        <button type="submit" name="tabelle" value="bank_kat">eintragen</button>
    </fieldset>
</form>
<?php endif; ?>


<form method="post" action="">
    <input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
    <fieldset id="infobox">
        <legend><b>Item <?php echo ($item->id != '0')
                    ? 'bearbeiten'
                    : 'anlegen'; ?></b></legend>
        <!--items eingeben-->
        <ul>
            <li>
                <label for="name">Item Name</label>
                <input type="text" size="30" name="name" value="<?php echo $item->name; ?>"/>
            </li>
            <li>
                <label for="kat_id">Kategorie</label>
                <select name="kat_id" onChange="">
                    <option value="-1">Bitte wählen</option>
                    <?php
                    foreach ($allCategories as $category) {
                        $selected = '';
                        if ($category->id == $item->catId) {
                            $selected = 'selected="selected"';
                        }
                        echo '<option value="' . $category->id . '"' . $selected . '>' . $category->title . '</option>';
                    }
                    ?>
                </select>
            </li>
            <li>
                <label for="Anzahl">Anzahl</label>
                <input type="text" size="5" name="menge" value="<?php echo $item->stockAmount; ?>"/>
            </li>
            <li>
                <label for="max">Maximal</label>
                <input type="text" size="5" name="max" value="<?php echo $item->maxPurchase; ?>"/>
            </li>
            <li>
                <label for="periode">Periode</label>
                <select name="periode" onChange="">
                    <?php
                    foreach ($periods as $days => $name) {
                        $selected = '';
                        if ($days == $item->timePeriod) {
                            $selected = 'selected="selected"';
                        }
                        echo '<option value="' . $days . '"' . $selected . '>' . $name . '</option>';
                    }
                    ?>
                </select>
            </li>
            <li>
                <label for="preis">Preis</label>
                <input type="text" size="5" name="preis" value="<?php echo $item->price; ?>"/>
            </li>
            <li>
                <label for="Beschreibung" style="display: block;">Beschreibung</label>
                <textarea rows="10" name="beschreibung" cols="50" class="two_lines"><?php echo $item->description; ?></textarea>
            </li>
        </ul>
        <button value="bank_item" name="tabelle" type="submit">speichern</button>
    </fieldset>
</form>