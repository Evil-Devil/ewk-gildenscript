<?php
use Ewigkeit\Sql\SqlAdapter;

// speichern
if (isset($_POST['act']) && $_POST['act'] == 'save') {
    $userId = (int)fetchGP('userId', 0);
    $role = fetchGP('role', 1);
    
    if ($userId > 0) {
        $sql = SqlAdapter::getInstance();
        
        $password = trim(fetchGP('pass'));
        $queryPass = '';
        if ($password !== '') {
            $queryPass = ", `pass` = {$sql->quote(MD5($password))}";
        }
        
        $queryString = "UPDATE  `members`
                        SET     `rolle` = {$sql->quote($role)}
                                {$queryPass}
                       WHERE    `id` = {$sql->quote($userId)}";
        $sql->exec($queryString);

        // redirect
        header('Location: index.php?dir=admin&site=edit_user&status=success&userId='.$userId);
        die();
    }
}

$user = (object) array(
    'id'           => '0',
    'name'         => '',
    'kat_id'       => '0',
    'menge'        => '',
    'max'          => '0',
    'preis'        => '',
    'periode'      => '0',
    'beschreibung' => '',
);

if (isset($_GET['userId'])) {
    $query  = "SELECT * FROM `members` WHERE `id` = " . $_GET['userId'];
    $user   = mysql_fetch_object(mysql_query($query));
}

$roles = array(
    '1' => 'Member',
    '2' => 'Twinks',
    '3' => 'Manager',
    '4' => 'Admin',
);

?>
<br /><br />
<?php if (fetchGP('status') === 'success'): ?>
<h2>Speichern erfolgreich</h2>
<?php endif;?>
<form method="post" action="index.php">
    <input type="hidden" name="dir" value="admin" />
    <input type="hidden" name="site" value="edit_user" />
    <input type="hidden" name="userId" value="<?= $user->id; ?>" />
    <input type="hidden" name="act" value="save" />
    <fieldset id="infobox">
        <legend><b>User '<?= $user->name; ?>' bearbeiten</b></legend>        
        <ul>
            <li>
                <label for="role">Rolle</label>
                <select id="role" name="role">                    
                <?php foreach ($roles as $id => $name): ?>
                    <option value="<?= $id ?>" <?= selected($id, $user->rolle) ?>><?= $name ?></option>
                <?php endforeach; ?>
                </select>
            </li>
            <li>
                <label for="pass">Passwort</label>
                <input type="text" name="pass" />
            </li>
        </ul>
        <input type="submit" value="speichern" />
    </fieldset>
</form>