<?php
include '../config/config.php';
include '../functions/MySqlAdapter.php';

$sql = new MySqlAdapter(DB_SERVER, DB_USER, DB_PASS, DB_SCHEME);
$sql->connect();

// ermittel und speichere den aktuellen gesamtbeitrag
$queryString = "update	members a, (
					select	memberId, MAX(beitrag) as totalDedication
					from	beitrag
					group by	memberid) b
				set	a.gesamtbeitrag = b.totalDedication
				where	a.id = b.memberId";
$result = $sql->exec($queryString);
echo "<p>Total Dedication: Updated {$result} rows</p>";


// dkp lohn und strafen eintragen
$queryString = "update	members a, (
					select	d.memberId, sum(e.punkte) as dkps
					from	dkp d inner join events e on d.eventId = e.id
					group by	d.memberid) b
				set a.m_dkp = b.dkps
				where a.id = b.memberId";
$result = $sql->exec($queryString);
echo "<p>DKP Salary/Penalty: Updated {$result} rows</p>";

// dkp eink�ufe
$queryString = "update	members a, (
					select	memberId, sum(preis) as dkps
					from	items_buy
					where	status = 'gebucht'
					group by	memberId) b
				set	a.m_dkp = a.m_dkp - b.dkps
				where	a.id = b.memberId";
$result = $sql->exec($queryString);
echo "<p>DKP Investments: Updated {$result} rows</p>";