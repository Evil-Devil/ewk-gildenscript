<?php
if ($member === false) {
    echo '<div class="message">Du bist nicht angemeldet!</div>';
    die();
}
if ($member['rolle'] < 4) {
    echo '<div class="message">Du hast keinen Zugriff zu diesem Bereich!</div>';
    die();
}

$sort_order = (isset($_GET['sort_order']))
    ? $_GET['sort_order']
    : 'ASC';
$sort = (isset($_GET['sort']))
    ? $_GET['sort']
    : 'Datum';
$seite = (isset($_GET['seite']))
    ? $_GET['seite']
    : '1';
$eps = 30; // Einträge pro Seite

// bestellungen
$queryString = "SELECT `ib`.`id`, `datum` `Datum`, `m`.`name` `Member`, `bi`.`name` `Item`, `ib`.`menge` `Menge`
                FROM `items_buy` `ib`
                    LEFT JOIN `bank_item` `bi` ON `ib`.`itemId` = `bi`.`id`
                    LEFT JOIN `members` `m` ON `ib`.`memberId` = `m`.`id`
                WHERE `ib`.`status` = 'vorgemerkt'
                ORDER BY `$sort` $sort_order
                LIMIT " . (($seite - 1) * $eps) . ", " . $eps;

$ergebnis = mysql_query($queryString);
$cssClasses = array('gerade', 'ungerade');
?>
<style>
    .status img {
        margin: 0 4px;
        cursor: pointer;
    }
</style>
<script>
    $('document').ready(function () {
        $('.btn').click(function () {
            var form = $(this).parents('form:first');
            form.find('input[name="status"]').val($(this).attr('alt'));
            form.submit();
        });
    })
</script>

<?php
$orders = mysql_fetch_assoc($ergebnis);
if ($orders === false) {
    echo '<div class="message">Es liegen keine Bestellungen vor</div>';
    exit;
}
?>

<table id="overview" cellspacing="0" cellpadding="2">
    <thead>
    <tr>
        <?php
        $spalten = array_keys($orders);
        unset($spalten[0]);

        $sort_order1 = ($sort_order == 'ASC')
            ? 'DESC'
            : 'ASC';

        foreach ($spalten as $spalte) {
            $link = '<a href="index.php?dir=admin&site=bestellungen&seite=' . $seite . '&sort=' . $spalte . '&sort_order=' . $sort_order1 . '">' . $spalte . '</a>';
            $symbol = '';
            if ($spalte == $sort) {
                $symbol = ($sort_order1 == 'DESC')
                    ? '<img width="11" height="9" title="aufsteigend" alt="aufsteigend" src="img/s_asc.png">'
                    : '<img width="11" height="9" title="absteigend" alt="absteigend" src="img/s_desc.png">';
            }

            //$symbol = '<div class="symbol">'.$symbol.'</div>';

            echo '<th valign="top">' . $link . $symbol . '</th>';
        }
        ?>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    /* ausgelesene Daten ausgeben */
    $i = ($seite - 1) * $eps;
    mysql_data_seek($ergebnis, 0);

    while ($row = mysql_fetch_assoc($ergebnis)) {
        $class = ($i % 2 > 0)
            ? 'gerade'
            : 'ungerade';
        echo '<tr class="' . $class . '">

			<td width="100" align="center"  value="<?php ' . (++$i) . ' ?>">' . $row['Datum'] . '</td>
			<td width="100" align="left">' . $row['Member'] . '</td>
            <td width="240" align="left">' . $row['Item'] . '</td>
            <td width="100" align="center">' . $row['Menge'] . '</td>
            <td class="status">
            <form action="index.php?dir=admin&site=bestellungen" method="post">
                <input type="hidden" name="id" value="' . $row['id'] . '" />
 	                    <input type="hidden" name="status" />
                <input type="hidden" name="tabelle" value="items_buy" />
                <img class="btn" alt="gebucht" src="img/accept.png" />
                <img class="btn" alt="storno" src="img/deny.png" />
	</form>
   

            </td>
        </tr>';
    }
    ?>

    </tbody>
</table>

<ul>
    <?php
    if ($i % $eps == 0) {
        echo '<li><a href="index.php?dir=admin&site=bestellungen&seite=' . ($seite + 1) . '&sort=' . $sort . '&sort_order=' . $sort_order . '">vor</a></li>';
    } else {
        echo '<li>vor</li>';
    }

    if ($seite > 1) {
        echo '<li><a href="index.php?dir=admin&site=bestellungen&seite=' . ($seite - 1) . '&sort=' . $sort . '&sort_order=' . $sort_order . '">zurück</a></li>';
    } else {
        echo '<li>zurück</li>';
    }
    ?>
</ul>

<div>
    <?php
    $queryString = "SELECT  `ib`.`id`, `datum`, `m`.`name` as member, `bi`.`name` as item, `ib`.`menge`
                FROM    `items_buy` `ib`
                  LEFT JOIN   `bank_item` `bi` ON `ib`.`itemId` = `bi`.`id`
                  LEFT JOIN   `members` `m` ON `ib`.`memberId` = `m`.`id`
              WHERE   `ib`.`status` = 'gebucht'
              ORDER BY  `datum` DESC
              LIMIT 30";

    $lastTransfers = $sql->query($queryString);
    $queryString = "SELECT  `ib`.`id`, `datum`, `m`.`name` as member, `bi`.`name` as item, `ib`.`menge`
                    FROM    `items_buy` `ib`
                      LEFT JOIN   `bank_item` `bi` ON `ib`.`itemId` = `bi`.`id`
                      LEFT JOIN   `members` `m` ON `ib`.`memberId` = `m`.`id`
                  WHERE   `ib`.`status` = 'storno'
                  ORDER BY  `datum` DESC
                  LIMIT 30";

    $lastStornos = $sql->query($queryString);
    ?>
    <div>
        <div style="display: inline-block; padding: 5px;">
            <h3>letzten Transfers</h3>
            <table id="overview" cellspacing="0" cellpadding="2">
                <thead>
                <tr>
                    <th>Datum</th>
                    <th>Member</th>
                    <th>Item</th>
                    <th>Anzahl</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($lastTransfers as $key => $transfer): ?>
                    <tr class="<?= $cssClasses[$key % 2] ?>">
                        <td><?= $transfer['datum'] ?></td>
                        <td width="100" align="left"><?= $transfer['member'] ?></td>
                        <td width="240" align="left"><?= $transfer['item'] ?></td>
                        <td width="100" align="center"><?= $transfer['menge'] ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div style="display: inline-block; padding: 5px;">
            <h3>letzten Stornos</h3>
            <table id="overview" cellspacing="0" cellpadding="2">
                <thead>
                <tr>
                    <th>Datum</th>
                    <th>Member</th>
                    <th>Item</th>
                    <th>Anzahl</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($lastStornos as $key => $storno): ?>
                    <tr class="<?= $cssClasses[$key % 2] ?>">
                        <td><?= $storno['datum'] ?></td>
                        <td width="100" align="left"><?= $storno['member'] ?></td>
                        <td width="240" align="left"><?= $storno['item'] ?></td>
                        <td width="100" align="center"><?= $storno['menge'] ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
