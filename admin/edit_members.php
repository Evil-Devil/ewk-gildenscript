<?php
use Ewigkeit\Sql\SqlAdapter;

require_once 'functions/MemberAdapter.php';
require_once 'functions/Member.php';

// init everything we need :)
$memberAdapter = new MemberAdapter(SqlAdapter::getInstance());


// react on submit
$errorMsg = null;
// mitglied eintragen
if (isset($_POST['add'])) {	
	
	$member = new Member();	
	$member->twinkOf = '';
	$member->isTwink = 0;
	$member->role = 1;
	$member->status = MemberAdapter::INACTIVE;
	
	if (isset($_POST['name']) && !empty($_POST['name'])) {
		$member->name = $_POST['name'];
	} 
	if (isset($_POST['datum']) && !empty($_POST['datum'])) {
		$member->joinDatetime = date('Y-m-d H:i:s', strtotime($_POST['datum']));
	}	
	if (isset($_POST['twink']) && is_numeric($_POST['twink'])) {
		$member->isTwink = (int)$_POST['twink'];
	}
	if (isset($_POST['twinkof']) && !empty($_POST['twinkof'])) {
		$member->twinkOf = $_POST['twinkof'];
	}
	
	// error check
	if (empty($member->name)) {
		$errorMsg = 'Bitte einen Namen angeben!';		
	} else {		
		if ($memberAdapter->existsName($member->name) !== false) {
			$errorMsg = "Das Mitglied \"{$member->name}\" existiert bereits";
		}
	}
	
	if ($errorMsg === null) {
		$retVal = $memberAdapter->addMember($member);
		header('Location: index.php?dir=admin&site=edit_members&added');
		//header('Location: edit_member.php?added');
		die();
	}
}

// mitglied entfernen
if (isset($_POST['remove'])) {
	$member = new Member();
	
	if (isset($_POST['member']) && !empty($_POST['member'])) {
		$member->setId($_POST['member']);
		$member->leaveDatetime = date('Y-m-d H:i:s');
		$member->status = MemberAdapter::INACTIVE;
		$retVal = $memberAdapter->removeMember($member);
		header('Location: index.php?dir=admin&site=edit_members&removed');
		//header('Location: edit_member.php?removed');
		die();
	}	
}

$activeMembers = $memberAdapter->getAll(false, MemberAdapter::ACTIVE);

?>
<script type="text/javascript">
function displayTwinkList(select) {
	var display = 'none';
	if (select.options[select.selectedIndex].value == 1) {
		display = 'block';
	}
	document.getElementById('twinklist').style.display = display;
}

function confirmRemove(form) {
	var name = form.member[form.member.selectedIndex].text;
	return confirm('Willst du wirklich \"' + name + '\" austragen?');
}

function confirmAdd(form) {
	var name = form.name.value;	
	return confirm('Willst du wirklich \"' + name + '\" eintragen?');
};

</script>

<?php if ($errorMsg !== null) { echo "<strong>{$errorMsg}</strong><br />"; } ?>
<?php if (isset($_GET['added'])): ?><div class="message">Das Mitglied wurde eingetragen</div><br /><?php endif; ?>
<?php if (isset($_GET['removed'])): ?><div class="message">Das Mitglied wurde ausgetragen</div><?php endif; ?>
	<fieldset id="settings">
		<legend>Memberverwaltung</legend>			
		[NEU!]Hier werden Member eingetragen ausgetragen oder gleich auf Twink gesetzt
		<br><br>
		<?php if ($errorMsg !== null) { echo "<strong>{$errorMsg}</strong><br />"; } ?>
		
		<!--<form action="edit_member.php" method="post" onsubmit="return confirmAdd(this);">-->
		<form action="index.php?dir=admin&site=edit_members" method="post" onsubmit="return confirmAdd(this);">
			<legend><b>Member eintragen:</b></legend><br>
			<ul>
				<li>
					<label for="datum">Eintrittsdatum</label>
					<input id="datum" type="text" name="datum" value="<?php echo date('d.m.Y H:i').':00'; ?>" />
				</li>
				<li>
					<label for="name">Membername</label>            
					<input id="name" type="text" name="name" />
				</li>
				<li>
					<label for="twink">Twink?</label>
					<select id="twink" name="twink" onChange="displayTwinkList(this);">
						<option value="0">Nein</option>
						<option value="1">Ja</option>
					</select>
				</li>
				<li id="twinklist" style="display: none;">
					<label for="twinkof">Twink von:</label>
					<select id="twinkof" name="twinkof">
						<option value="" selected="selected">Bitte wählen</option>
						<?php foreach ($activeMembers as $member): ?>
							<?php if ($member->isTwink == 0): ?>
							<option value="<?php echo $member->name ?>"><?php echo $member->name ?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</li>
				<input name="add" type="submit" value="eintragen" />
			</ul>
		</form>		
		<br><br>
		<legend><b>Member austragen:</b></legend><br>
		
		<!--<form action="edit_member.php" method="post" onsubmit="return confirmRemove(this);">-->
		<form action="index.php?dir=admin&site=edit_members" method="post" onsubmit="return confirmRemove(this);">
			<select name="member">
				<option value="0">Bitte wählen</option>
			<?php foreach ($activeMembers as $member): ?>
				<option value="<?php echo $member->getId() ?>"><?php echo $member->name ?></option>
			<?php endforeach; ?>
			</select>
			<input name="remove" type="submit" value="austragen" />
		</form>
		
	</fieldset>