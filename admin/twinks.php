<?php
if (fetchGP('act') == 'save') {

    // bruteforce save ...
    $memberIds = fetchGP('id');
    $twinks = fetchGP('twink');
    $twinkOf = fetchGP('twink_von');

    $queryString = "UPDATE  members
                    SET     twink = %s,
                            twink_von = %s
                    WHERE   id = %s";

    $sql = \Ewigkeit\Sql\SqlAdapter::getInstance();
    foreach ($memberIds as $index => $memberId) {
        $sql->exec(
            sprintf(
                $queryString,
                $twinks[$index],
                $sql->quote($twinkOf[$index]),
                $sql->quote($memberId)
            )
        );
    }
    // redirect
    header('Location: index.php?dir=admin&site=twinks&success=1');
    die();
}

// alle aktiven chars holen
function getAllActiveCharacters()
{
    $queryString = "SELECT  `id`, `name`, `twink`, `twink_von`
                    FROM    `members`
                    WHERE   `status` = 1
                    ORDER BY  `name`";
    $sql = \Ewigkeit\Sql\SqlAdapter::getInstance();
    return $sql->query($queryString, PDO::FETCH_CLASS);
}

$characters = getAllActiveCharacters();

// filter members
$members = array();
foreach ($characters as $character) {
    if ($character->twink == 0) {
        $members[] = $character;
    }
}

$users = $characters;

function isChecked($value)
{
    if ($value == 1) {
        return 'checked="checked"';
    }
    return '';
}

function isSelected($src, $dest)
{
    if ($src == $dest) {
        return 'selected="selected"';
    }
    return '';
}

if (fetchGP('success') == 1) {
    echo '<h2>Änderungen gespeichert</h2>';
}
echo '<h3>' . date('d.m.Y') . '</h3>';
$cssClasses = array('gerade', 'ungerade');
?>
<form action="index.php" method="post" onsubmit="return confirm('Daten abschicken?');">
    <input type="hidden" name="dir" value="admin"/>
    <input type="hidden" name="site" value="twinks"/>
    <input type="hidden" name="act" value="save"/>
    <fieldset>
        <legend>Twinkliste</legend>
        <table id="overview">
            <tr>
                <th>Mitglied</th>
                <th>ist Twink?</th>
                <th>Twink von..</th>
            </tr>
            <?php foreach ($characters as $index => $character): ?>
                <tr class="<?= $cssClasses[$index % 2] ?>">
                    <td><?= $character->name ?></td>
                    <td>
                        <input type="hidden" name="id[<?= $index ?>]" value="<?= $character->id ?>"/>
                        <input type="hidden" name="twink[<?= $index ?>]" value="0"/>
                        <input type="checkbox" tabindex="<?= $index ?>" name="twink[<?= $index ?>]"
                               style="text-align: right;" value="1" <?= isChecked($character->twink) ?> />
                    <td>
                        <select name="twink_von[<?= $index ?>]">
                            <option value="">Bitte wählen</option>
                            <?php foreach ($members as $owner): ?>
                                <option value="<?= $owner->name ?>" <?= isSelected(
                                    $character->twink_von,
                                    $owner->name
                                ) ?>><?= $owner->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
        <button type="submit">eintragen</button>
    </fieldset>
</form>
