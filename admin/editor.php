<?php
use Ewigkeit\Editor\FilterOptions;
use Ewigkeit\Editor\EditorService;

function arrayToNamedJson($keyField, $valueField, $inArray)
{   
    $isMultiField = (bool)(sizeof($valueField) > 1);
    $properties = array();
    
    foreach ($inArray as $inVal) {
        if (isset($inVal[$keyField])) {
            if ($isMultiField) {
                $multiProperty = array();
                foreach ($valueField as $field) {
                    if (isset($inVal[$field])) {
                        $multiProperty[] = $field.': "'.$inVal[$field].'"';
                    }
                }
                $properties[] = $inVal[$keyField].': {'.implode(',', $multiProperty).'}';
            } else {
                if (isset($inVal[$valueField])) {
                    $properties[] = $inVal[$keyField].': "'.$inVal[$valueField].'"';
                }
            }
        }        
    }
    return '{'.implode(',', $properties).'}';
} 

function isSelected($source, $target)
{
    if ($source == $target) {
        return 'selected="selected"';
    }
    return '';
}


$levelRanges = array(
        '90_80' => '90 - 80',
        '79_70' => '79 - 70',
        '69_60' => '69 - 60',
        '59_50' => '59 - 50',
        '49_40' => '49 - 40',
        '39_30' => '39 - 30',
        '29_20' => '29 - 20',
        '19_10' => '19 - 10',
        '09_0' => '09 - 0'
);
/*
$equipTypes = array(
        'weapon'	=> 'Waffe',
        'head'		=> 'Kopf',
        'chest'		=> 'Brust',
        'shoulders'	=> 'Schultern',
        'waist'		=> 'Gürtel',
        'hands'		=> 'Hände',
        'legs'		=> 'Beine',
        'feet'		=> 'Füße',
);*/

//$levels = array(1,2,4,5,10,12,14,15,20,22,24,25,30,32,34,35,40,42,44,45,50,52,54,55,60,61,62,64,65,66,67,70,71,72,75,76,77,78);
//arsort($levels);

//$charclassIndex = 1;
/*
$charclasses = array(
        'warrior'	=> 'Krieger',
        'protector' => 'Patriot',
        'assassin'	=> 'Assassine',
        'marksman'	=> 'Schütze',
        'mage'		=> 'Magier',
        'priest'	=> 'Priester',
        'vampire'	=> 'Vampir',
        'bard' 		=> 'Barde',
);*/

$filterOptions = new FilterOptions();
$filterOptions->applyRequestData();

$editorService = new EditorService();

$charClasses = $editorService->fetchCharClasses();
$qualities = $editorService->fetchQualities();
$parts = $editorService->fetchParts();
$sets = $editorService->fetchSets($filterOptions);
$items = $editorService->fetchItems($filterOptions);


$lastSetId = 0;
$slotcount = 30;
$item = $editorService->fetchItem(1);
//echo '<pre>'.print_r($item, 1).'</pre>';
?>

<style>
dt { font-weight: bold; clear: left; }
dd { clear: left; } 



.item , .eq-quali {
	position: absolute;
	display: block;
	height: 40px;
	width: 40px;
	float: left;
	/*background: #FF0000;	*/
}

.item {
    position: relative;
}

.item-description, .item-edit {
    float: left;
    padding: 15px;
}

.editor-item {
    cursor: pointer;
}


.editor {
	position: fixed;
	z-index: 100000;
	top: 250px;
	left: 460px;
}
</style>

<script>
// <![CDATA[
var iconSlotMatrix = {
	'chest': [1,10,20,30,40,50,,50,60,65,60,70,75,70,77,,75,,75],
	'head': [,,,35,45,55,,50,62,67,60,72,77,70,77,,75,,75],
	'shoulders': [,,22,32,42,52,,50,61,66,60,71,76,70,77,,75,,75],
	'hands': [,14,24,34,44,54,,50,62,67,60,72,77,70,77,,75,,75],
	'waist': [4,14,24,34,44,54,,50,62,67,60,72,77,70,77,,75,,75],
	'legs': [2,12,22,32,42,52,,50,61,66,60,71,76,70,77,,75,,75],
	'feet': [5,15,25,35,45,55,,50,62,67,60,72,77,70,77,,75,,75,,,,,,,,,75],
	'weapon': [1,10,20,30,40,,50,50,60,60,65,,70,70,75,,77,75,75,1,1,1,1,1,60,1,1,1,1,1,20],
	'offhand': [30,40,50,60,70,80,90,],
	'ring': [35,45,55,65,78,75,77],
	'necklace': [,45,55,65,78,75,77]
};

var slotData = null;
var selected = {
	'charclass': '',
	'type': '',
	'level': '',
	'quality': '',	
};

function initSlotsAndItems() {
	if (slotData != null) {
		return;
	}
	slotData = [];
	for (var i=0; i<31; i++) {
		slotData[i] = {
			'slot': document.getElementById('slot_' + i),
			'item': document.getElementById('item_' + i),
			'quali': document.getElementById('quali_' + i)			
		};
	}	
}

// ]]>
</script>

<?php include 'partial/admin_editor_dialog.php' ?>

<script>
// <!CDATA[

// ]]>
</script>

<div>
    <form action="index.php" method="GET">
        <input type="hidden" name="dir" value="admin" />
        <input type="hidden" name="site" value="editor" />
        <fieldset>    
        	<legend>Auswahl-Filter</legend>
        	<label>Charakter-Klasse:</label>
        	<select name="charclass" onchange="this.form.submit();">
        		<option value="">-Bitte auswählen-</option>
                <?php foreach ($charClasses as $class): ?>
                <option value="<?php echo $class['id'] ?>"
        			<?php echo isSelected($filterOptions->charClass, $class['id'])?>><?php echo $class['description'] ?></option>
                <?php endforeach; ?>
            </select>
            <br />
            <label>Set:</label>
            <select name="set" onchange="this.form.submit();">
        		<option value="">-Bitte auswählen-</option>
        		<?php foreach ($sets as $set): ?>
                <option value="<?php echo $set['id']?>"
        			<?php echo isSelected($filterOptions->set, $set['id'])?>><?php echo $set['name'] . '(' . $set['description'] . ')' ?></option>
                <?php endforeach; ?>
        	</select>
        	<br />
        	<label>Qualität:</label>
        	<select name="quality" onchange="this.form.submit();">
        		<option value="">-Bitte auswählen-</option>
        		<?php foreach ($qualities as $quality): ?>
                <option value="<?php echo $quality['id']?>"
        			<?php echo isSelected($filterOptions->quality, $quality['id'])?>><?php echo $quality['description'] ?></option>
                <?php endforeach; ?>
        	</select>
        	<br />
        	<label>Level-Bereich:</label>
        	<select name="levelrange" onchange="this.form.submit();">
        		<option value="">-Bitte auswählen-</option>
        		<?php foreach ($levelRanges as $key => $val): ?>
                <option value="<?php echo $key?>"
        			<?php echo isSelected($filterOptions->levelRange, $key)?>><?php echo $val ?></option>
                <?php endforeach; ?>
        	</select>
        	<br />
        	<input type="submit" value="Filtern" />
        </fieldset>
    </form>
    <hr />
    <dl>
    <?php foreach ($items as $item): ?>
        <?php if ((int)$item['setId'] != $lastSetId) {
            $lastSetId = (int)$item['setId'];
            echo "<dt>{$item['setname']} ({$item['setdescription']})</dt>";
        } ?>    
        <dd class="editor-item" id="<?php echo $item['id'] ?>">
            <span class="item <?php echo $editorService->getItemTypeCss($item) ?>">
                <span class="eq-quali <?php echo $editorService->getItemQualityCss($item) ?>"></span>
            </span>
            <span class="item-description"><?php echo $item['name'] ?> (Lvl <?php echo $item['level'] ?>)</span>            
        </dd>
    <?php endforeach; ?>
    </dl>
</div>

<script>
// <![CDATA[
var itemEditorForm = jQuery("form#editor-item-form");

var charClasses = <?php echo arrayToNamedJson('id', 'name', $charClasses); ?>;
var parts = <?php echo arrayToNamedJson('id', 'name', $parts); ?>;
var qualities = <?php echo arrayToNamedJson('id', 'name', $qualities); ?>;
var sets = <?php echo arrayToNamedJson('id', array('name', 'description'), $sets); ?>;

initSlotsAndItems();

function resetForm() {	
	itemEditorForm[0].id.value = 0;
	itemEditorForm[0].name.value = '';
	itemEditorForm[0].level.value = 0;
    applySelection(itemEditorForm[0].set, 0);
    applySelection(itemEditorForm[0].charclass, 0);
    applySelection(itemEditorForm[0].quality, 0);
    applySelection(itemEditorForm[0].part, 0);

    for (var i=0, il=30; i<il; i++) {
    	slotData[i].slot.style.display = 'none';
    }
}

function toggleSetName() {	
	itemEditorForm[0].set_name.disabled = !itemEditorForm[0].set_name.disabled;
	itemEditorForm[0].set_description.disabled = !itemEditorForm[0].set_description.disabled;
}

function applySetName(select) {
	var setId = parseInt(select.options[select.selectedIndex].value);
	if (setId <= 0) {
		itemEditorForm[0].set_name.value = '';
		return;
	}
	itemEditorForm[0].set_name.value = sets[setId].name;
	itemEditorForm[0].set_description.value = sets[setId].description; 
}

function applySelection(select, value) {
	jQuery(select).find('option[value="' + value + '"]').attr("selected", "selected");
}

function displayItems(selectedIcon) {
		
	var form = itemEditorForm.serializeJSON();
	if (typeof parts[form.part] == 'undefined') {
		return;
	}
	
	var iconIndex = selectedIcon || -1;	
	var charClassName = '';
	if (typeof charClasses[form.charclass] != 'undefined') {
		charClassName = '-' + charClasses[form.charclass]; 
	} 	
	var cssClassName = 'item eq-' + parts[form.part] + charClassName + ' eq-index-';
	var cssQualityClass = '';
	
	if (form.quality > 0) {
		cssQualityClass = 'eq-quali eq-' + qualities[form.quality];
	}	
	
	for (var i=0, il=30; i<il; i++) {
		// reset slot		
		slotData[i].slot.style.display = 'none';
		// display slots
		if (typeof iconSlotMatrix[parts[form.part]][i] != 'undefined') {
			slotData[i].item.className = (cssClassName + i);
			slotData[i].quali.className = cssQualityClass;
			slotData[i].slot.style.display = 'block';
			if (iconIndex == i) {
				itemEditorForm[0].icon[i].checked = true;
			}
		}
	}
}

function asyncSaveItem(frm) {
	jQuery.post("async/save_item.php", jQuery(frm).serialize())
	   .done(function(data) {
		   alert(data);
	   });
	return false;
}
            
function asyncGetItem(obj, itemId) {
	//console.log(obj);
	//console.log(itemId);	
	jQuery.getJSON("async/get_item.php", {id: itemId})
	   .done(function(json) {
		   itemEditorForm[0].reset();
		   itemEditorForm[0].id.value = json.id;
		   itemEditorForm[0].name.value = json.name;
		   itemEditorForm[0].level.value = json.level;		   
		   if (json.set > 0) {
			   itemEditorForm[0].set_name.value = sets[json.set].name;
			   itemEditorForm[0].set_description.value = sets[json.set].description;
			   applySelection(itemEditorForm[0].set, json.set);
		   }		   
		   applySelection(itemEditorForm[0].charclass, json.charclass);
		   applySelection(itemEditorForm[0].quality, json.quality);
		   applySelection(itemEditorForm[0].part, json.part);
		   displayItems(json.icon_index);
	   })
	   .fail(function() {
		   resetForm();
	   });
}
            
jQuery('dd.editor-item').click(function() {
	var obj = jQuery(this);
	return asyncGetItem(obj, obj.prop('id'));
});


// ]]>
</script>