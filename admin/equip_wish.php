<?php
use Ewigkeit\Sql\SqlAdapter;
use Ewigkeit\Common\EquipWishEntity;
use Ewigkeit\Common\VexxerEquipEntity;
use Ewigkeit\Common\ElementarEquipEntity;

if (empty($_SESSION) || $member['rolle'] < 3) {
    echo '<div class="message">Du bist nicht angemeldet oder hast nicht den nötigen Rang!</div>';
    die();
}

function fieldValue(array $data, $field, $default)
{
    if (isset($data[$field])) {
        return $data[$field];
    }
    return $default;
}

function prepareEntityData($data)
{

    $vexxer77 = new VexxerEquipEntity();
    $vexxer85 = new VexxerEquipEntity();
    $vexxer90 = new VexxerEquipEntity();
    $vexxer95 = new VexxerEquipEntity();
    $elementar77 = new ElementarEquipEntity();
    $elementar85 = new ElementarEquipEntity();
    $elementar95 = new ElementarEquipEntity();

    // assign everyting...
    $vexxer77->weapon = fieldValue($data, 'vexxer77_weapon', 0);
    $vexxer77->hands = fieldValue($data, 'vexxer77_hands', 0);
    $vexxer77->boots = fieldValue($data, 'vexxer77_boots', 0);
    $vexxer85->weapon = fieldValue($data, 'vexxer85_weapon', 0);
    $vexxer85->hands = fieldValue($data, 'vexxer85_hands', 0);
    $vexxer90->weapon = fieldValue($data, 'vexxer90_weapon', 0);
    $vexxer90->hands = fieldValue($data, 'vexxer90_hands', 0);
    $vexxer95->weapon = fieldValue($data, 'vexxer95_weapon', 0);

    $elementar77->head = fieldValue($data, 'ele77_head', 0);
    $elementar77->body = fieldValue($data, 'ele77_body', 0);
    $elementar77->shoulders = fieldValue($data, 'ele77_shoulders', 0);
    $elementar77->hands = fieldValue($data, 'ele77_hands', 0);
    $elementar77->belt = fieldValue($data, 'ele77_belt', 0);
    $elementar77->leg = fieldValue($data, 'ele77_leg', 0);
    $elementar77->boots = fieldValue($data, 'ele77_boots', 0);
    $elementar85->head = fieldValue($data, 'ele85_head', 0);
    $elementar85->body = fieldValue($data, 'ele85_body', 0);
    $elementar85->shoulders = fieldValue($data, 'ele85_shoulders', 0);
    $elementar85->hands = fieldValue($data, 'ele85_hands', 0);
    $elementar85->belt = fieldValue($data, 'ele85_belt', 0);
    $elementar85->leg = fieldValue($data, 'ele85_leg', 0);
    $elementar85->boots = fieldValue($data, 'ele85_boots', 0);
    $elementar95->head = fieldValue($data, 'ele95_head', 0);
    $elementar95->body = fieldValue($data, 'ele95_body', 0);
    $elementar95->shoulders = fieldValue($data, 'ele95_shoulders', 0);
    $elementar95->hands = fieldValue($data, 'ele95_hands', 0);
    $elementar95->belt = fieldValue($data, 'ele95_belt', 0);
    $elementar95->leg = fieldValue($data, 'ele95_leg', 0);
    $elementar95->boots = fieldValue($data, 'ele95_boots', 0);

    $equip = new EquipWishEntity();
    $equip->vexxer77 = $vexxer77;
    $equip->vexxer85 = $vexxer85;
    $equip->vexxer90 = $vexxer90;
    $equip->vexxer95 = $vexxer95;
    $equip->elementar77 = $elementar77;
    $equip->elementar85 = $elementar85;
    $equip->elementar95 = $elementar95;
    $equip->name = $data['name'];
    $equip->charClass = $data['charClassName'];
    $equip->charClassId = $data['charClassId'];

    return $equip;
}

function readEquipWishes()
{
    $sql = SqlAdapter::getInstance();

    $queryString = "SELECT  ew.memberId, ew.charClassId,
                            ew.vexxer77_weapon, ew.vexxer77_hands, ew.vexxer77_boots,
                            ew.vexxer85_weapon, ew.vexxer85_hands,
                            ew.vexxer90_weapon, ew.vexxer90_hands,
                            ew.vexxer95_weapon,
                            ew.ele77_head, ew.ele77_body, ew.ele77_shoulders, ew.ele77_hands, ew.ele77_belt, ew.ele77_leg, ew.ele77_boots,
                            ew.ele85_head, ew.ele85_body, ew.ele85_shoulders, ew.ele85_hands, ew.ele85_belt, ew.ele85_leg, ew.ele85_boots,
                            ew.ele95_head, ew.ele95_body, ew.ele95_shoulders, ew.ele95_hands, ew.ele95_belt, ew.ele95_leg, ew.ele95_boots,
                            m.name, c.description as charClassName
                    FROM    equip_wish ew                        
                        RIGHT OUTER JOIN fw_charclass c ON ew.charClassId = c.id
                        RIGHT OUTER JOIN members m ON ew.memberId = m.id
                    WHERE	m.status = 1
			        AND		m.twink = 0";
    $result = $sql->query($queryString);

    $entities = array();

    foreach ($result as $val) {
        $entities[] = prepareEntityData($val);
    }
    return $entities;
}

function htmlButtonStatusIcon($value)
{
    switch ($value) {
        case 1:
            $icon = 'plus';
            break;
        case 2:
            $icon = 'minus';
            break;
        case 3:
            $icon = 'help';
            break;
        case 4:
            $icon = 'check';
            break;
        default:
            $icon = 'cancel';
    }

    return '<span class="ui-state-default ui-corner-all" style="display:inline-block;"><span class="eq-' . $icon . ' ui-icon ui-icon-' . $icon . '"></span></span>';
}

function htmlTableHead()
{
    $tpl = '<tr>
                <th></th>
                <th>Rasse</th>
                <th>Waffe</th>
                <th>Kopf</th>
                <th>Brust</th>
                <th>Schultern</th>
                <th>Taille</th>
                <th>Hände</th>
                <th>Beine</th>
                <th>Füsse</th>
            </tr>';
    return $tpl;
}

function htmlWishlistVexxer77($wishlist)
{
    $html = '';
    foreach ($wishlist as $wish) {
        $entity = $wish->vexxer77;
        $html .= '<tr>
                    <td><strong>' . $wish->name . '</strong></td>
                    <td>' . $wish->charClass . '</td>
                    <td title="Waffe">' . htmlButtonStatusIcon($entity->weapon) . '</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td title="Hände">' . htmlButtonStatusIcon($entity->hands) . '</td>
                    <td></td>
                    <td title="Füsse">' . htmlButtonStatusIcon($entity->boots) . '</td>
                </tr>';
    }
    return $html;
}

function htmlWishlistVexxer77Summary($wishlist)
{
    //print_r($wishlist);
    $html = '';
    foreach ($wishlist as $charClass => $wish) {
        $html .= '<tr>
                    <td><strong>' . $charClass . '</strong></td>
                    <td></td>
                    <td title="Waffe">' . $wish->vexxer77->weapon . '</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td title="Hände">' . $wish->vexxer77->hands . '</td>
                    <td></td>
                    <td title="Füsse">' . $wish->vexxer77->boots . '</td>
                </tr>';
    }
    return $html;
}

function htmlWishlistVexxer85Summary($wishlist)
{
    //print_r($wishlist);
    $html = '';
    foreach ($wishlist as $charClass => $wish) {
        $html .= '<tr>
                    <td><strong>' . $charClass . '</strong></td>
                    <td></td>
                    <td title="Waffe">' . $wish->vexxer85->weapon . '</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td title="Hände">' . $wish->vexxer85->hands . '</td>
                    <td></td>
                    <td></td>
                </tr>';
    }
    return $html;
}

function htmlWishlistVexxer90Summary($wishlist)
{
    //print_r($wishlist);
    $html = '';
    foreach ($wishlist as $charClass => $wish) {
        $html .= '<tr>
                    <td><strong>' . $charClass . '</strong></td>
                    <td></td>
                    <td title="Waffe">' . $wish->vexxer90->weapon . '</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td title="Hände">' . $wish->vexxer90->hands . '</td>
                    <td></td>
                    <td></td>
                </tr>';
    }
    return $html;
}

function htmlWishlistElementar77Summary($wishlist)
{
    //print_r($wishlist);
    $html = '';
    foreach ($wishlist as $charClass => $wish) {
        $html .= '<tr>
                    <td><strong>' . $charClass . '</strong></td>
                    <td></td>
                    <td</td>
                    <td title="Kopf">' . $wish->elementar77->head . '</td>
                    <td title="Brust">' . $wish->elementar77->body . '</td>
                    <td title="Schultern">' . $wish->elementar77->shoulders . '</td>
                    <td title="Taille">' . $wish->elementar77->belt . '</td>
                    <td title="Hände">' . $wish->elementar77->hands . '</td>
                    <td title="Beine">' . $wish->elementar77->leg . '</td>
                    <td title="Füsse">' . $wish->elementar77->boots . '</td>
                </tr>';
    }
    return $html;
}

function htmlWishlistElementar95Summary($wishlist)
{
    //print_r($wishlist);
    $html = '';
    foreach ($wishlist as $charClass => $wish) {
        $html .= '<tr>
                    <td><strong>' . $charClass . '</strong></td>
                    <td></td>
                    <td></td>
                    <td title="Kopf">' . $wish->elementar95->head . '</td>
                    <td title="Brust">' . $wish->elementar95->body . '</td>
                    <td title="Schultern">' . $wish->elementar95->shoulders . '</td>
                    <td title="Taille">' . $wish->elementar95->belt . '</td>
                    <td title="Hände">' . $wish->elementar95->hands . '</td>
                    <td title="Beine">' . $wish->elementar95->leg . '</td>
                    <td title="Füsse">' . $wish->elementar95->boots . '</td>
                </tr>';
    }
    return $html;
}


function htmlWishlistElementar85Summary($wishlist)
{
    //print_r($wishlist);
    $html = '';
    foreach ($wishlist as $charClass => $wish) {
        $html .= '<tr>
                    <td><strong>' . $charClass . '</strong></td>
                    <td></td>
                    <td></td>
                    <td title="Kopf">' . $wish->elementar85->head . '</td>
                    <td title="Brust">' . $wish->elementar85->body . '</td>
                    <td title="Schultern">' . $wish->elementar85->shoulders . '</td>
                    <td title="Taille">' . $wish->elementar85->belt . '</td>
                    <td title="Hände">' . $wish->elementar85->hands . '</td>
                    <td title="Beine">' . $wish->elementar85->leg . '</td>
                    <td title="Füsse">' . $wish->elementar85->boots . '</td>
                </tr>';
    }
    return $html;
}

function wishlistSummary($wishlist)
{
    $wishes = array();
    foreach ($wishlist as $wish) {
        if (!isset($wishes[$wish->charClass])) {
            $equip = new EquipWishEntity();
            $equip->vexxer77 = new VexxerEquipEntity();
            $equip->vexxer85 = new VexxerEquipEntity();
            $equip->vexxer90 = new VexxerEquipEntity();
            $equip->vexxer95 = new VexxerEquipEntity();
            $equip->elementar77 = new ElementarEquipEntity();
            $equip->elementar85 = new ElementarEquipEntity();
            $equip->elementar95 = new ElementarEquipEntity();
            $wishes[$wish->charClass] = $equip;
        }
        // apply vexxer 77
        if ($wish->vexxer77->weapon == 1) {
            $wishes[$wish->charClass]->vexxer77->weapon += $wish->vexxer77->weapon;
        }
        if ($wish->vexxer77->hands == 1) {
            $wishes[$wish->charClass]->vexxer77->hands += $wish->vexxer77->hands;
        }
        if ($wish->vexxer77->boots == 1) {
            $wishes[$wish->charClass]->vexxer77->boots += $wish->vexxer77->boots;
        }
        // apply vexxer 85
        if ($wish->vexxer85->weapon == 1) {
            $wishes[$wish->charClass]->vexxer85->weapon += $wish->vexxer85->weapon;
        }
        if ($wish->vexxer85->hands == 1) {
            $wishes[$wish->charClass]->vexxer85->hands += $wish->vexxer85->hands;
        }
        // apply vexxer 90
        if ($wish->vexxer90->weapon == 1) {
            $wishes[$wish->charClass]->vexxer90->weapon += $wish->vexxer90->weapon;
        }
        if ($wish->vexxer90->hands == 1) {
            $wishes[$wish->charClass]->vexxer90->hands += $wish->vexxer90->hands;
        }
        // apply vexxer 95
        if ($wish->vexxer95->weapon == 1) {
            $wishes[$wish->charClass]->vexxer95->weapon += $wish->vexxer95->weapon;
        }
        // apply elemental 77
        if ($wish->elementar77->head == 1) {
            $wishes[$wish->charClass]->elementar77->head += $wish->elementar77->head;
        }
        if ($wish->elementar77->body == 1) {
            $wishes[$wish->charClass]->elementar77->body += $wish->elementar77->body;
        }
        if ($wish->elementar77->shoulders == 1) {
            $wishes[$wish->charClass]->elementar77->shoulders += $wish->elementar77->shoulders;
        }
        if ($wish->elementar77->belt == 1) {
            $wishes[$wish->charClass]->elementar77->belt += $wish->elementar77->belt;
        }
        if ($wish->elementar77->hands == 1) {
            $wishes[$wish->charClass]->elementar77->hands += $wish->elementar77->hands;
        }
        if ($wish->elementar77->leg == 1) {
            $wishes[$wish->charClass]->elementar77->leg += $wish->elementar77->leg;
        }
        if ($wish->elementar77->boots == 1) {
            $wishes[$wish->charClass]->elementar77->boots += $wish->elementar77->boots;
        }
        // apply elemental 85
        if ($wish->elementar85->head == 1) {
            $wishes[$wish->charClass]->elementar85->head += $wish->elementar85->head;
        }
        if ($wish->elementar85->body == 1) {
            $wishes[$wish->charClass]->elementar85->body += $wish->elementar85->body;
        }
        if ($wish->elementar85->shoulders == 1) {
            $wishes[$wish->charClass]->elementar85->shoulders += $wish->elementar85->shoulders;
        }
        if ($wish->elementar85->belt == 1) {
            $wishes[$wish->charClass]->elementar85->belt += $wish->elementar85->belt;
        }
        if ($wish->elementar85->hands == 1) {
            $wishes[$wish->charClass]->elementar85->hands += $wish->elementar85->hands;
        }
        if ($wish->elementar85->leg == 1) {
            $wishes[$wish->charClass]->elementar85->leg += $wish->elementar85->leg;
        }
        if ($wish->elementar85->boots == 1) {
            $wishes[$wish->charClass]->elementar85->boots += $wish->elementar85->boots;
        }
        // apply elemental 85
        if ($wish->elementar95->head == 1) {
            $wishes[$wish->charClass]->elementar95->head += $wish->elementar95->head;
        }
        if ($wish->elementar95->body == 1) {
            $wishes[$wish->charClass]->elementar95->body += $wish->elementar95->body;
        }
        if ($wish->elementar95->shoulders == 1) {
            $wishes[$wish->charClass]->elementar95->shoulders += $wish->elementar95->shoulders;
        }
        if ($wish->elementar95->belt == 1) {
            $wishes[$wish->charClass]->elementar95->belt += $wish->elementar95->belt;
        }
        if ($wish->elementar95->hands == 1) {
            $wishes[$wish->charClass]->elementar95->hands += $wish->elementar95->hands;
        }
        if ($wish->elementar95->leg == 1) {
            $wishes[$wish->charClass]->elementar95->leg += $wish->elementar95->leg;
        }
        if ($wish->elementar95->boots == 1) {
            $wishes[$wish->charClass]->elementar95->boots += $wish->elementar95->boots;
        }
    }
    return $wishes;
}

function htmlWishlistVexxer85($wishlist)
{
    $html = '';
    foreach ($wishlist as $wish) {
        $entity = $wish->vexxer85;
        $html .= '<tr>
                    <td><strong>' . $wish->name . '</strong></td>
                    <td>' . $wish->charClass . '</td>
                    <td title="Waffe">' . htmlButtonStatusIcon($entity->weapon) . '</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td title="Hände">' . htmlButtonStatusIcon($entity->hands) . '</td>
                    <td></td>
                    <td></td>                    
                </tr>';
    }
    return $html;
}

function htmlWishlistVexxer90($wishlist)
{
    $html = '';
    foreach ($wishlist as $wish) {
        $entity = $wish->vexxer90;
        $html .= '<tr>
                    <td><strong>' . $wish->name . '</strong></td>
                    <td>' . $wish->charClass . '</td>
                    <td title="Waffe">' . htmlButtonStatusIcon($entity->weapon) . '</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td title="Hände">' . htmlButtonStatusIcon($entity->hands) . '</td>
                    <td></td>
                    <td></td>
                </tr>';
    }
    return $html;
}


function htmlWishlistVexxer95($wishlist)
{
    $html = '';
    foreach ($wishlist as $wish) {
        $entity = $wish->vexxer95;
        $html .= '<tr>
                    <td><strong>' . $wish->name . '</strong></td>
                    <td>' . $wish->charClass . '</td>
                    <td title="Waffe">' . htmlButtonStatusIcon($entity->weapon) . '</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>';
    }
    return $html;
}


function htmlWishlistElementar($wishlist, $lvl)
{
    $html = '';
    $eleSet = 'elementar' . $lvl;
    foreach ($wishlist as $wish) {
        $entity = $wish->$eleSet;
        $html .= '<tr>
                    <td><strong>' . $wish->name . '</strong></td>
                    <td>' . $wish->charClass . '</td>
                    <td></td>
                    <td title="Kopf">' . htmlButtonStatusIcon($entity->head) . '</td>
                    <td title="Brust">' . htmlButtonStatusIcon($entity->body) . '</td>
                    <td title="Schultern">' . htmlButtonStatusIcon($entity->shoulders) . '</td>
                    <td title="Taille">' . htmlButtonStatusIcon($entity->belt) . '</td>
                    <td title="Hände">' . htmlButtonStatusIcon($entity->hands) . '</td>
                    <td title="Beine">' . htmlButtonStatusIcon($entity->leg) . '</td>
                    <td title="Füsse">' . htmlButtonStatusIcon($entity->boots) . '</td>
                </tr>';
    }
    return $html;
}

$equipWishes = readEquipWishes();
$wishSummary = wishlistSummary($equipWishes);

?>
<style>
    .wishlist {
        border-spacing: 10px;
        border: 0px;
    }

    .eq-plus {
        background-color: #00FF00;
        color: #212121;
    }

    .eq-minus {
        background-color: #FF0000;
        color: #212121;
    }

    .eq-help {
        background-color: #FFFF00;
        color: #212121;
    }

    .eq-check {
        background-color: #FFFFFF;
        color: #212121;
    }

    .ui-widget-icon-font-size {
        font-size: 0.5em;
    }
</style>

<div style="padding: 25px;">
    <h1>Ausrüstungs-Wunsch Übersicht</h1>

    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">Vexxer-/GK-Set Lv. 77</a></li>
            <li><a href="#tabs-2">Vexxer-/GK-Set Lv. 85</a></li>
            <li><a href="#tabs-3">Vexxer-/GK-Set Lv. 90</a></li>
            <li><a href="#tabs-4">Vexxer-/GK-Set Lv. 95</a></li>
            <li><a href="#tabs-5">Elementar-Set Lv. 77</a></li>
            <li><a href="#tabs-6">Elementar-Set Lv. 85</a></li>
            <li><a href="#tabs-7">Elementar-Set Lv. 95</a></li>
            <li><a href="#tabs-8">Zusammenfassung</a></li>
        </ul>
        <div id="tabs-1">
            <table id="wishlist1" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistVexxer77($equipWishes) ?>
                </tbody>
            </table>
        </div>
        <div id="tabs-2">
            <table id="wishlist2" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistVexxer85($equipWishes) ?>
                </tbody>
            </table>
        </div>
        <div id="tabs-3">
            <table id="wishlist3" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistVexxer90($equipWishes) ?>
                </tbody>
            </table>
        </div>
        <div id="tabs-4">
            <table id="wishlist4" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistVexxer95($equipWishes) ?>
                </tbody>
            </table>
        </div>
        <div id="tabs-5">
            <table id="wishlist5" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistElementar($equipWishes, 77) ?>
                </tbody>
            </table>
        </div>
        <div id="tabs-6">
            <table id="wishlist6" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistElementar($equipWishes, 85) ?>
                </tbody>
            </table>
        </div>
        <div id="tabs-7">
            <table id="wishlist7" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistElementar($equipWishes, 95) ?>
                </tbody>
            </table>
        </div>
        <div id="tabs-8">
            <h3>Vexxer-/GK-Set Lv. 77</h3>
            <table id="wishlist8_1" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistVexxer77Summary($wishSummary) ?>
                </tbody>
            </table>
            <h3>Vexxer-/GK-Set Lv. 85</h3>
            <table id="wishlist8_2" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistVexxer85Summary($wishSummary) ?>
                </tbody>
            </table>
            <h3>Vexxer-/GK-Set Lv. 90</h3>
            <table id="wishlist8_3" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistVexxer85Summary($wishSummary) ?>
                </tbody>
            </table>
            <h3>Vexxer-/GK-Set Lv. 95</h3>
            <table id="wishlist8_3" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistVexxer90Summary($wishSummary) ?>
                </tbody>
            </table>
            <h3>Elementar-Set Lv. 75</h3>
            <table id="wishlist8_4" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistElementar77Summary($wishSummary) ?>
                </tbody>
            </table>
            <h3>Elementar-Set Lv. 85</h3>
            <table id="wishlist8_5" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistElementar85Summary($wishSummary) ?>
                </tbody>
            </table>
            <h3>Elementar-Set Lv. 95</h3>
            <table id="wishlist8_6" class="wishlist">
                <thead>
                <?= htmlTableHead() ?>
                </thead>
                <tbody>
                <?= htmlWishlistElementar95Summary($wishSummary) ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("#tabs").tabs();
    });
    //tablesort =)
    $(function () {
        $("#wishlist1").tablesorter();
        $("#wishlist2").tablesorter();
        $("#wishlist3").tablesorter();
        $("#wishlist4").tablesorter();
        $("#wishlist5").tablesorter();
        $("#wishlist6").tablesorter();
        $("#wishlist7").tablesorter();
        $("#wishlist8").tablesorter();
    });
</script>