<?php
use Ewigkeit\Common\GuildContribution;
use Ewigkeit\Common\EventContribution;
use Ewigkeit\Common\Activity;
use Ewigkeit\Sql\SqlAdapter;

if ($member['rolle'] < 3) {
	echo '<div class="message">Du bist nicht angemeldet oder hast den nötigen Rang!</div>';
	die();
}

$activity = new Activity();

function determineDaysInGuildIndex($daysInGuild) {
	if ($daysInGuild <= 14) {
		return '0-14days';
	} else if ($daysInGuild <= 59) {
		return '15-59days';
	} else {
		return '60+days';
	}
}

function determineRatioColor($attendRatio) {
	$color = 'red';
	if ($attendRatio >= 25) {
		$color = 'orange';				
		if ($attendRatio >= 60) {					
			$color = 'green';
		} else if ($attendRatio >= 40 and $attendRatio < 60) {
			$color = 'yellow';
		}
	}
	return $color;
}

$memberActivities = array(
	'0-14days' => array(),
	'15-59days' => array(),
	'60+days' => array(),
);

$cssClasses = array('gerade', 'ungerade');

$daysInGuildDescriptions = array(
	'0-14days' => 'Noch keine 15 Tage Mitglied',
	'15-59days' => '15 bis 59 Tage Mitglied',
	'60+days' => '60 Tage und länger Mitglied',
);

$eventContribution = new EventContribution();
$guildContribution = new GuildContribution();
$eventContribution->determineTotalEventCount();
$totalEvents = $eventContribution->getTotalEventCount();

// retrieve all members
$queryString = "SELECT	m.id, 0 as avgContribution, m.name, m.`eintritt`, unix_timestamp(m.`eintritt`) as ts_eintritt,
                        (UNIX_TIMESTAMP() - unix_timestamp(m.`eintritt`)) as ts_lifetime,
                        0 as contribution, 0 as attends
                FROM    members m
                WHERE   m.status = 1
                AND     m.twink = 0";
$allMembersTmp = SqlAdapter::getInstance()->query($queryString);
$allMembers = array();
foreach ($allMembersTmp as $row) {
    $allMembers[$row['id']] = $row;
}
unset($allMembersTmp);
//echo '<pre style="border: 1px solid #000000; width: 320px; height: 160px; overflow: scroll; float: left;">Members: '.count($allMembers).PHP_EOL.print_r($allMembers, 1).'</pre>';



$averageGuildContribution = $guildContribution->getAverageForAllMembers();
//echo '<pre style="border: 1px solid #000000; width: 320px; height: 160px; overflow: scroll; float: left;">Guild Contrib:<br />'.print_r($averageGuildContribution, 1).'</pre>';
foreach ($averageGuildContribution as $row) {
    $allMembers[$row['id']]['avgContribution'] = $row['avgContribution'];
    $allMembers[$row['id']]['contribution'] = (int)$row['avgContribution'];

}
unset($averageGuildContribution);

$eventsForAllMembers = $eventContribution->determineEventsForAllMembers();
//echo '<pre style="border: 1px solid #000000; width: 320px; height: 160px; overflow: scroll; float: left;">Events Member: '.print_r($eventsForAllMembers, 1).'</pre>';
foreach ($eventsForAllMembers as $row) {
    $allMembers[$row['id']]['attends'] = $row['attends'];
}

//echo '<pre style="border: 1px solid #000000; width: 320px; height: 160px; overflow: scroll; float: left;">Final Members: '.count($allMembers).PHP_EOL.print_r($allMembers, 1).'</pre>';

// apply overall memberActivities
foreach ($allMembers as $row) {
    $row['daysInGuild'] = (int)($row['ts_lifetime'] / 86400); // 1 day: 60 * 60 * 24
    $index = determineDaysInGuildIndex($row['daysInGuild']);
    $memberActivities[$index][$row['name']] = $row;
}
//echo '<pre style="border: 1px solid #000000; width: 320px; height: 160px; overflow: scroll; float: left;">Activities<br />'.print_r($memberActivities, 1).'</pre>';



?>
<br clear="left" />
<style>
.red { color: #FF0000; font-weight: bold; }
.orange { color: #EE7600; font-weight: bold; }
.yellow { color: #FFB90F; font-weight: bold; }
.green { color: #228B22; font-weight: bold; }
</style>

<h1>Mitglieder Aktivität</h1>

<table id="overview" cellspacing="0" cellpadding="2">
	<thead>
		<tr>
			<th valign="top">Name</th>
			<th valign="top">Beitritt</th>
			<th valign="top">Tage in Gilde</th>
			<th valign="top" colspan="2">Event-Beteiligung</th>
			<th valign="top">Beitrag</th>
			<th valign="top">Bank-Nutzung</th>
		</tr>		
	</thead>
	<tbody>	
		
	<?php	
	foreach ($memberActivities as $dayGroupIndex => $dayGroupVal):				
		ksort($dayGroupVal);
		?>		
		<tr>
			<th colspan="7" valign="top"><?php echo $daysInGuildDescriptions[$dayGroupIndex] ?></th>
		</tr>
		<?php foreach ($dayGroupVal as $row => $val):
			$attendRatio = round($val['attends'] / $totalEvents * 100);
			$ratioColor = $activity->getAttentRatioColor($attendRatio);
			$contribColor = $activity->getContributionColor($val['contribution']);
			$bankAllowed = 'Nein';
			$bankColor = 'red';
			if ($val['daysInGuild'] > 60 && ($attendRatio >= 25 || $val['contribution'] >= 450)) {
				$bankAllowed = 'Ja';
				$bankColor = 'green';
			}
			?>
		<tr class="<?php echo $cssClasses[$row % 2] ?>">
			<td width="120" align="left"><?php echo $val['name'] ?></td>
			<td><?php echo date('d.m.Y H:i', $val['ts_eintritt']) ?></td>
			<td align="right"><?php echo (int)$val['daysInGuild'] ?></td>
			<td class="<?php echo $ratioColor ?>" width="70" align="right"><?php echo $attendRatio ?>%</td>
			<td class="<?php echo $ratioColor ?>"align="right"><?php echo $val['attends'].'/'.$totalEvents ?></td>
			<td class="<?php echo $contribColor ?>" align="right"><?php echo $val['contribution'] ?></td>
			<td class="<?php echo $bankColor ?>"align="center"><?php echo $bankAllowed ?></td>
		</tr>
		<?php
		endforeach;
	endforeach;
	?>
	</tbody>
</table>