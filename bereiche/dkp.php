<?php
// ob member im Normalfall wirklich false ist ...?
if ($member === false) {
	echo '<div class="message">Du bist nicht angemeldet!</div>';
	die();
}

$sortOrder = fetchGP('sort_order', 'DESC');
$sort       = fetchGP('sort', 'datum');
$seite      = fetchGP('seite', 1);
$eps        = 30; // Eintr�ge pro Seite
$memberId = $member['id'];
if (isset($_REQUEST['userId'])) {
    $memberId = $_REQUEST['userId'];
}

$newSortOrder = 'ASC';
$sortSymbol = '<img width="11" height="9" title="aufsteigend" alt="aufsteigend" src="img/s_asc.png">';
if ($sortOrder == $newSortOrder) {
	$newSortOrder = 'DESC';
	$sortSymbol = '<img width="11" height="9" title="absteigend" alt="absteigend" src="img/s_desc.png">';
}

$dkpList = array();
$dkpListSize = 0;

// we need a better DB structure for this whole stuff o_O - union performance is worse - if it is possible to improve this ^^"
$queryString = "SELECT	SQL_CALC_FOUND_ROWS
						`e`.`name`, `e`.`datum`, `e`.`punkte`, 1 AS `menge`, 'gebucht' AS `status`
				FROM	`dkp` d
					INNER JOIN `events` `e`
						ON `e`.`id` = d.`eventId`
				WHERE	memberId = {$memberId}				
				
				UNION ALL			
				SELECT	bi.`name`, ib.`datum`, (-1 * ib.`preis`) punkte, ib.`menge`, ib.`status`
				FROM `items_buy` ib
					INNER JOIN `bank_item` bi
					ON bi.`id` = ib.`itemId`
				WHERE	memberId = {$memberId}
				
				UNION ALL
				SELECT	`name`, `datum`, `punkte`, 1 AS `menge`, 'gebucht' AS `status`
				FROM	bonus
				WHERE	memberId = {$memberId}
				ORDER BY	{$sort} {$sortOrder}
				LIMIT	".(($seite - 1) * $eps).", ".$eps;
$result = $sql->query($queryString);
if ($result !== false) {
	$dkpList = $result;
}

// how many rows do we have without the limit?
$queryString = "SELECT FOUND_ROWS() as rows";
$result = $sql->query($queryString);
if ($result !== false) {
	$dkpListSize = $result[0]['rows'];
}

                           
$baseLinkUrl = 'index.php?dir=bereiche&site=dkp&seite='.$seite.'&sort_order='.$newSortOrder.'&sort=';
$cssClasses = array('even', 'odd');

?>                                           
<div><?php echo $member['name'] ?> hier deine Auflistung von DKPs</div>

<table id="overview" cellspacing="0" cellpadding="2">
	<colgroup>
		<col width="150" />
		<col width="240" />
		<col width="100" />
		<col width="100" />
	</colgroup>
	<tr>
		<th valign="top"><a href="<?php echo $baseLinkUrl?>datum">Datum <?php echo $sortSymbol ?></a></th>
		<th valign="top"><a href="<?php echo $baseLinkUrl?>name">Verwendung <?php echo $sortSymbol ?></a></th>
		<th valign="top"><a href="<?php echo $baseLinkUrl?>punkte">Punkte <?php echo $sortSymbol ?></a></th>
		<th valign="top"><a href="<?php echo $baseLinkUrl?>status">Status <?php echo $sortSymbol ?></a></th>    
	</tr>
	<?php foreach ($dkpList as $key => $val): ?>
	<?php
	$fontColor = '#000000';
	$costs = $val['menge'] * $val['punkte'];
	if ($costs > 0) {
		$fontColor = '#009933';
	} else if ($costs < 0) {
		$fontColor = '#FF0000';
	}
	$amountInfo = '';
	if ($val['menge'] > 1) {
		$amountInfo = '<strong>'.$val['menge'].'x</strong>';
	}
	?>
	<tr class="<?php echo $cssClasses[$key % 2] ?>">
		<td align="center"><?php echo $val['datum'] ?></td>
		<td align="left"><?php echo $val['name'] ?></td>
		<td align="right" style="color: <?php echo $fontColor ?>"><?php echo $costs ?></td>
		<td align="center"><?php echo $val['status'] ?></td>
	</tr>
	<?php endforeach; ?>
</table>

<ul>
<?php
// wie viele Seiten? - wir können dies noch schön erweitern :D
$maxPages = ceil($dkpListSize / $eps);
if ($maxPages > 1) {	
	$siteLinkUrl = 'index.php?dir=bereiche&site=dkp&sort_order='.$sortOrder.'&sort='.$sort.'&seite=';
	if ($seite >= 1 && $seite < $maxPages) {		
		echo '<li><a href="'.$siteLinkUrl.($seite+1).'">vor</li>';
	}
	if ($seite > 1 && $seite <= $maxPages) {		
		echo '<li><a href="'.$siteLinkUrl.($seite-1).'">zurück</li>';
	}
} ?>
</ul>