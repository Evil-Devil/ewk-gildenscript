<?php 
use Ewigkeit\Sql\SqlAdapter;

$act = fetchGP('act');
$username = trim(fetchGP('username'));
$password = trim(fetchGP('pass'));

// TODO: move logic to dedicated class
if ($act === 'doLogin') {

    // default redirect to the login with error :O
    $redirect = 'index.php?dir=bereiche&site=login&status=error';
    if (empty($username) || empty($password)) {
        header('Location: ' . $redirect);
        die();
    }

    // check if the user does have a password...
    $queryString = "SELECT  id
                    FROM    members
                    WHERE   name = {$sql->quote($username)}
                    AND     (pass is null
                            OR      pass = '')";
    $result = $sql->query($queryString);
    
    if ((bool)$result !== false) {        
        // this user does not have password ...so we set it
        $queryString = "UPDATE  members
                        SET     pass = {$sql->quote(MD5($password))}
                        WHERE   id = {$sql->quote($result[0]['id'])}";
        $sql->exec($queryString);
    }
    
    // try to login - maybe a pass was set previously :D
    $sql = SqlAdapter::getInstance();
    $queryString = "SELECT  id, name, klasse, status, twink, rolle
                    FROM    members
                    WHERE   name = {$sql->quote($username)}
                    AND     pass = {$sql->quote(MD5($password))}";    
    $result = $sql->query($queryString);
        
    if ((bool)$result !== false) {
        $redirect = 'index.php?dir=bereiche&site=overview';
        // save the result to the session, yea, obviously ;)

        $sessionUser = $result[0];
        // add another field for future changes to get rid of the direct assignment
        $sessionUser['user'] = $result[0];
        
        $_SESSION = $sessionUser;
    }
    header('Location: ' . $redirect);
    die();
}
?>

<form action="index.php" method="post">
    <input type="hidden" name="act" value="doLogin" />
    <input type="hidden" name="dir" value="bereiche" />
    <input type="hidden" name="site" value="login" />	
	<table width="400" bgcolor="#000000" border="0" cellpadding="5" cellspacing="1" align="center">
        <?php if (fetchGP('status') === 'error'): ?>
        <tr><td colspan="2" align="center"><h2 style="color: #FD0000;">Einloggen nicht möglich,<br />überprüfe deine Eingabe</h2></td></tr>
        <?php endif;?>
    	<tr bgcolor="#1b1b1b">
    		<th colspan="2"><font color="#ccc" size="3">Login nur für Gildenmember </font></th>
    	</tr>
    	<tr>
    		<td width="170" bgcolor="#F4F2EE">Dein Ingame Name</td>
    		<td width="230" bgcolor="#ffffff">
    			<input type="text" name="username" size="20" class="input" value="<?= $username ?>" />
    		</td>
    	</tr>
    	<tr>
    		<td width="170" bgcolor="#F4F2EE">Passwort</td>
    		<td width="230" bgcolor="#ffffff">
    			<input type="password" name="pass" size="20" class="input">
    		</td>
    	</tr>
    	<tr>
    		<td bgcolor="#F4F2EE" align="center" colspan="2">
                <input type="submit" value="Anmelden" />
    		</td>
    	</tr>
	</table>
</form>	