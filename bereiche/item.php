<?php
function saveOrder() {
    $memberId = fetchGP('memberId');
    $itemId = fetchGP('itemId');
    $preis = fetchGP('preis');
    $menge = fetchGP('menge');

    $queryString = "";
    \Ewigkeit\Sql\SqlAdapter::getInstance()->exec($queryString);
}


if ($member === false) {
    echo '<div class="message">Du bist nicht angemeldet!</div>';
    die();
}



$itemId = fetchGP('id', 0);
$query = "SELECT `bi`.`id` `itemId`, `beschreibung`, `name`, `bi`.`menge` - IFNULL(SUM(`ib`.`menge`), 0) `mengex`, `bi`.`preis`, `periode`, `max`
                FROM    `bank_item` `bi`
                  LEFT JOIN `items_buy` `ib` ON `bi`.`id` = `ib`.`itemId`
    WHERE `bi`.`id` = $itemId AND `ib`.`status` = 'vorgemerkt'";

$ergebnis = mysql_query($query);
$row = mysql_fetch_object($ergebnis);

$gesamt = "SELECT SUM(`menge`) `mengen`
                FROM    `items_buy`
WHERE `itemId` = $itemId AND `status` = 'gebucht'";
$gesamtsumme = mysql_query($gesamt);
$gesamtsumme1 = mysql_fetch_object($gesamtsumme);

?>
<script>
function checkAmount(input, minAmount) {
    return (input >= minAmount);
}
</script>
<form action="index.php?dir=bereiche&site=bank" method="post" onsubmit="return checkAmount(this.menge.value, 1);">
<fieldset>
    <legend><b><?php echo $row->name; ?></b></legend>
    <p><Font style=" font-size: 12px;"><?php echo $row->beschreibung; ?></Font></p>
    <ul>
        <li>
            <label>Verfügbar:</label>
            <b><?php echo $row->mengex; ?></b>
        </li>
        <li>
            <label>bereits verkauft:</label>

            <?php if ($gesamtsumme1->mengen > 0) {
                echo $gesamtsumme1->mengen . ' mal';
            }?>

            <script>
                $(function () {
                    $("#spinner").spinner({
                        spin: function (event, ui) {
                            if (ui.value > 100) {
                                $(this).spinner("value", -100);
                                return false;
                            } else if (ui.value < -0) {
                                $(this).spinner("value", 0);
                                return false;
                            }
                        }
                    });
                });
            </script>
        </li>

        <li>
            <label>Limit:</label>
            <b><?php echo $row->max . ' mal / ' . $row->periode; ?> Tage</b> (0 = kein Limit!)
        </li>
        <li>
            <label>DKP Preis:</label>
            <b><?php echo $row->preis; ?></b>
        </li>
        <li>
            <label for="menge">Menge:</label>
            <input type="text" id="spinner" name="menge" value="0" size="2" maxlength="3"
                   style="text-align: center;" onfocus="this.select();"/>
            Stück
        </li>
    </ul>
    <input type="hidden" name="memberId" value="<?php echo $member['id']; ?>"/>
    <input type="hidden" name="itemId" value="<?php echo $itemId; ?>"/>
    <input type="hidden" name="preis" value="<?php echo $row->preis; ?>"/>
    <button value="items_buy" name="tabelle" type="submit" style="display: inline-block;">bestellen</button>
</fieldset>
</form>