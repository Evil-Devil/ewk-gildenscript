<?php
$user = $_SESSION['user'];
/* variable aus der URL auslesen*/
$sort_order = (isset($_GET['sort_order'])) ? $_GET['sort_order'] : 'ASC';
$sort = (isset($_GET['sort'])) ? $_GET['sort'] : 'name';
$seite = (isset($_GET['seite'])) ? $_GET['seite'] : '1';
##echo $sort_order;
$eps   = 50; // Einträge pro Seite

/* Daten auslesen */
$query = "SELECT `eintritt`, `name` `Member`, `level` `Level`, `beitrag` `Beitrag (Gesamt)`, ABS(SUM(`beitrag`)-(2*`beitrag`)) `Beitrag (Woche)` FROM (
        SELECT `name`, `level`, `eintritt`, IF(ISNULL(`beitrag`), 0, `beitrag`) AS `beitrag`, `woche`
		FROM `beitrag` `b`
		RIGHT JOIN `members` `m` ON `m`.`id` = `b`.`memberId` OR ISNULL(`b`.`memberId`)
		RIGHT JOIN (
			SELECT `woche` `datum`
			FROM `beitrag`
			GROUP BY `woche`
			ORDER BY `woche` DESC
			LIMIT 2
		) `w` ON `datum` = `woche` OR ISNULL(`woche`)
		WHERE `status` = 1
		ORDER BY `name`, `woche` DESC
	) `a`
GROUP BY `name`
ORDER BY `$sort` $sort_order
LIMIT ".(($seite-1)*$eps).", ".$eps;

$ergebnis = mysql_query($query);

$last5 = "SELECT *
FROM `members`
WHERE `status` = 0
ORDER BY `members`.`austritt` DESC
LIMIT 5 ";

$lastm = mysql_query($last5);
mysql_close($link);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<div>
<table id="overview" cellspacing="0" cellpadding="2">
	<thead>
	<tr>
		<th width="20"></th>
		<?php
			$spalten = array_keys(mysql_fetch_assoc($ergebnis));
			unset($spalten[0]);
			$sort_order1 = ($sort_order == 'ASC') ? 'DESC' : 'ASC';
			
			foreach ($spalten as $spalte) {
				$link = '<a href="overview.php?seite='.$seite.'&sort='.$spalte.'&sort_order='.$sort_order1.'">'.$spalte.'</a>';
				$symbol = '';
				if ($spalte == $sort) {
					$symbol = ($sort_order1 == 'DESC')
						? '<img width="11" height="9" title="aufsteigend" alt="aufsteigend" src="img/s_asc.png">'
						: '<img width="11" height="9" title="absteigend" alt="absteigend" src="img/s_desc.png">';	
				}
				
				//$symbol = '<div class="symbol">'.$symbol.'</div>';
				
				echo '<th valign="top">'.$link.$symbol.'</th>';
			}
		?>
		<th valign="top" width="50">DKP</th>
	</tr>
	</thead>
	<tbody>
	<?php
	/* ausgelesene Daten ausgeben */
	$i = ($seite-1)*$eps;
	mysql_data_seek($ergebnis, 0);
	while ($row = mysql_fetch_assoc($ergebnis)) {
		$datum = date('d.m.Y', strtotime($row['eintritt']));
		$fontcolor = '#000';
		
		if($row['Beitrag (Woche)'] > 120) {
			$fontcolor = '#009933';
		}
		
		if($row['Beitrag (Woche)'] < 65){
			$fontcolor = '#FF0000';
		} 
		
		$class = ($i%2 > 0) ? 'gerade' : 'ungerade';

		if(strtotime($row['eintritt']) > time() - 432000) {
			$row['Member'] = $row['Member'].' <img width="16" height="16" title="NewMember" alt="NewMember" src="img/new-icon.png">';
		}
		
		echo '<tr class="'.$class.'">
			<td width="20">'.(++$i).'</td>
			<td width="110"><a href="#" title="Eintrittsdatum: '.$datum.'">'.$row['Member'].'</a></td>
			<td width="40" align="center">'.$row['Level'].'</td>
			<td width="70" align="right">'.$row['Beitrag (Gesamt)'].'</td>
			<td width="70" align="right"><font title="List:Beitrag letzter Wochen" color="'.$fontcolor.'">'.$row['Beitrag (Woche)'].'</font></td>
			<td width="50" align="right">00</td>
		</tr>';
	}
	?>
		</tbody>
	</table>
</div>
<div class="rechts">
	<fieldset id="austritt">
		<legend>letzten 5 Austritte</legend>
		<table width="100%">
		<?php
		$n = 0; 
		while ($row = mysql_fetch_object($lastm)) {
			$datumlm = date('d.m.Y H:i:s', strtotime($row->austritt));
			echo '<tr><td align="right">'.++$n.'. </td><td><strong>'.$row->name.'</strong></td><td>'.$datumlm.'</td></tr>';
		}
	?>
		</table>
	</fieldset>
	<fieldset id="infobox">
		<legend>Info Box</legend>
		Info: 1,2 Sachen kurz schon mal erläutert..
		Das Script wird jede Stunde aktualisiert - neue Member werden erkannt und eingetragen, und erhalten dabei für 5Tage eine Markierung
		Die letzten 5 Member die unsere Gilde verlassen haben findet Ihr oberhalb.<p>In der Übersicht habt Ihr Einblick auf alle wichtigen Statistiken die man auch fein sortieren kann. Beiträge werden immer Sonntags eingetragen die gehen nun leider nicht automatisch(Dank Klimperline)Farbe ROT sollte euch zu denken geben xD</p>
		<p>Zwecks Login oder anderen Funktionen des Scripts sei gesagt das bisher nur erstmal die Übersicht funktioniert alles andere kommt nach und nach dann.</p>
    </fieldset>
</div>
<ul>
<?php
	if ($i % $eps == 0) {
		echo '<li><a href="overview.php?seite='.($seite+1).'&sort='.$sort.'&sort_order='.$sort_order.'">vor</a></li>';
	} else {
		echo '<li>vor</li>';
	}
	
	if ($seite > 1) {
		echo '<li><a href="overview.php?seite='.($seite-1).'&sort='.$sort.'&sort_order='.$sort_order.'">zurück</a></li>';
	} else {
		echo '<li>zurück</li>';
	}
?>
</ul>
<body>
</body>
</html>
