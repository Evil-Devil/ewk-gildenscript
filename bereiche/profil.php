<style>
    ul {
        list-style-type: none;
    }

    ul li {
        float: left;
        padding: 0;
        margin: 0;
    }

    ul li:nth-child(odd) {
        clear: both;
    }

    ul li input {
        width: 50px;
        margin: 2px;
    }

    ul li label {
        display: inline-block;
        width: 90px;
        margin: 5px;
    }


</style>

<div>
    <ul>
        <li><label>Lebenspunkte</label></li>
        <li><input type="text" name="hp" value=""/></li>
        <li><label>Manapunkte</label></li>
        <li><input type="text" name="mp" value=""/></li>
        <li><label>Angriff</label></li>
        <li>
            <input type="text" name="atkmin" value=""/>
            <input type="text" name="atkmax" value=""/>
        </li>
        <li><label>Verteidigung</label></li>
        <li><input type="text" name="def" value=""/></li>
        <li><label>Genauigkeit</label></li>
        <li><input type="text" name="acc" value=""/></li>
        <li><label>Ausweichen</label></li>
        <li><input type="text" name="eva" value=""/></li>
        <li><label>Krit Chance</label></li>
        <li><input type="text" name="critrate" value=""/></li>
        <li><label>Krit Ausweichen</label></li>
        <li><input type="text" name="criteva" value=""/></li>
        <li><label>Krit Schaden</label></li>
        <li><input type="text" name="critdmg" value=""/></li>
        <li><label>Krit Verteidigung</label></li>
        <li><input type="text" name="critdef" value=""/></li>
        <!--<li><label></label></li>
        <li><input type="text" name="" value=""/></li>
        <li><label></label></li>
        <li><input type="text" name="" value=""/></li>
        <li><label></label></li>
        <li><input type="text" name="" value=""/></li>-->
    </ul>
</div>
<div>
    <ul>
        <li><label>Physisch</label></li>
        <li><input type="text" name="physical" value=""/></li>
        <li><label>Erde</label></li>
        <li><input type="text" name="earth" value=""/></li>
        <li><label>Wasser</label></li>
        <li><input type="text" name="water" value=""/></li>
        <li><label>Feuer</label></li>
        <li><input type="text" name="fire" value=""/></li>
        <li><label>Wind</label></li>
        <li><input type="text" name="wind" value=""/></li>
        <li><label>Licht</label></li>
        <li><input type="text" name="light" value=""/></li>
        <li><label>Dunkel</label></li>
        <li><input type="text" name="dark" value=""/></li>
    </ul>
    <ul>
        <li><label>Physisch</label></li>
        <li><input type="text" name="physical" value=""/></li>
        <li><label>Erde</label></li>
        <li><input type="text" name="earth" value=""/></li>
        <li><label>Wasser</label></li>
        <li><input type="text" name="water" value=""/></li>
        <li><label>Feuer</label></li>
        <li><input type="text" name="fire" value=""/></li>
        <li><label>Wind</label></li>
        <li><input type="text" name="wind" value=""/></li>
        <li><label>Licht</label></li>
        <li><input type="text" name="light" value=""/></li>
        <li><label>Dunkel</label></li>
        <li><input type="text" name="dark" value=""/></li>
    </ul>

</div>


