<?php
use Ewigkeit\Common\EventContribution;
use Ewigkeit\Common\GuildContribution;
use Ewigkeit\Sql\SqlAdapter;

use Ewigkeit\Bank\BankService;
use Ewigkeit\Bank\Category;

if ($member === false) {
    echo '<div class="message">Du bist nicht angemeldet!</div>';
    die();
}

$guildContribution = new GuildContribution();
$guildContribution->setIntervalDays(14);
$weeklyContributions = $guildContribution->getForMember($member['id']);

$eventContribution = new EventContribution();
$eventContribution->determineContributionForMember($member['id']);


// member eintritt ermitteln
$minMemberTimeDone = false;    // bereits 60 tage in der Gilde?

$queryString = "SELECT	unix_timestamp(date_add(eintritt, interval 60 day)) as minMemberTime
				FROM	members
				WHERE	id = " . $member['id'];
$result = $sql->query($queryString);
if ($result !== false) {
    if (time() > $result[0]['minMemberTime']) {
        $minMemberTimeDone = true;
    }
}
unset($result);


$allowedBankAccess = true;
if (!$minMemberTimeDone
    || ($eventContribution->getParticipationRatio() < 25
        && $guildContribution->getAverageForMember($member['id']) < 450)
) {
    $allowedBankAccess = false;
}

if ($member['rolle'] > 3) {
    $allowedBankAccess = true;
}


if (!$allowedBankAccess) {
    echo '<div class="message">Tut uns leid, du hast derzeit keinen Zugriff zur Bank!</div>';
    if (!$minMemberTimeDone) {
        echo '<div class="message">Du bist erst ' . $memberDays . ' von notwendigen 60 Tagen Mitglied!</div>';
    }
    die();
}


/* variable aus der URL auslesen*/
$sort_order = fetchGP('sort_order', 'ASC');
$sort = fetchGP('sort', 'name');
$kat_id = fetchGP('kat_id');
$keyword = fetchGP('keyword');
$seite = fetchGP('seite', 1);
$eps = 30; // Einträge pro Seite

// dkp ermitteln
$queryString = "SELECT  SUM(dkps.dkp) as dkp
                FROM	(
                        SELECT  sum(`e`.`punkte`) as dkp
                        FROM	`dkp` d
                          INNER JOIN  `events` `e` ON `e`.`id` = d.`eventId`
                        WHERE d.memberId = {$member['id']}

                        UNION ALL
                        SELECT	sum((-1 * ib.`preis`) * ib.menge) as dkp
                        FROM	`items_buy` ib
                            INNER JOIN  `bank_item` bi ON bi.`id` = ib.`itemId`
                        WHERE   ib.`status` <> 'storno'
                        AND		ib.memberId = {$member['id']}

                        UNION ALL
                        SELECT	sum(b.`punkte`) as dkp
                        FROM	bonus b
                        WHERE   b.memberId = {$member['id']}
                    ) dkps";


$result = $sql->query($queryString);
$userDkp = $result[0]['dkp'];
unset($result);
?>


<style>
    .status img {
        margin: 0 2px;
        cursor: pointer;
    }

    #overview tr td a,
    #overview tr td form {
        display: inline-block;
    }

    #kategorien {
        width: 170px;
    }
</style>
<script>
    $('document').ready(function () {
        $("a.overlay[rel]").tooltip();
        $('.btn').click(function () {
            var form = $(this).parents('form:first');
            form.find('input[name="status"]').val($(this).attr('alt'));
            form.submit();
        });
    })
</script>

<?php
// letzten bestellungen ermitteln
$queryString = "SELECT  `ib`.`id`, `datum`, `bi`.`name` as item, `ib`.`menge`, ib.status
                FROM    `items_buy` `ib`
                  LEFT JOIN   `bank_item` `bi` ON `ib`.`itemId` = `bi`.`id`
                WHERE ib.memberId = {$member['id']}
                ORDER BY  `datum` DESC
                LIMIT 20";
$lastTransactions = $sql->query($queryString);

function determineSortOrderSymbol($field, $sortBy, $sortOrder)
{
    $sortAsc = '<img width="11" height="9" title="aufsteigend" alt="aufsteigend" src="img/s_asc.png">';
    $sortDesc = '<img width="11" height="9" title="absteigend" alt="absteigend" src="img/s_desc.png">';

    if ($field != $sortBy) {
        return $sortDesc;
    }

    if ($sortOrder == 'DESC') {
        return $sortAsc;
    }
    return $sortDesc;
}

function determineSortOrder($field, $sortBy, $sortOrder)
{
    if ($field != $sortBy) {
        return 'DESC';
    }
    if ($sortOrder == 'DESC') {
        return 'ASC';
    }
    return 'DESC';
}

function createSortLink($site, $field, $sortBy, $orderBy)
{
    $tpl = '<a href="index.php?dir=bereiche&site=bank&seite=%s&sort=%s&sort_order=%s">%s</a> %s';
    $sortOrder = determineSortOrder($field, $sortBy, $orderBy);
    $symbol = determineSortOrderSymbol($field, $sortBy, $sortOrder);
    return sprintf($tpl, $site, $sortBy, $sortOrder, ucfirst($field), $symbol);
}

function statusButton($status)
{
    if ($status < 1) {
        return '<img class="btn" alt="1" title="aktivieren" src="img/accept.png"/>';
    }
    return '<img class="btn" alt="0" title="deaktivieren" src="img/deny.png"/>';
}

function createActionForm($itemId, $status)
{
    $statusButton = statusButton($status);
    return <<<TPL
<form class="status" action="index.php?dir=bereiche&site=bank" method="post">
    <input type="hidden" name="id" value="{$itemId}"/>
    <input type="hidden" name="status"/>
    <input type="hidden" name="tabelle" value="bank_item"/>
    {$statusButton}
</form>
<form action="index.php?dir=bereiche&site=bank" method="post" onsubmit="return confirm('Item löschen?');">
    <input type="hidden" name="id" value="{$itemId}"/>
    <input type="hidden" name="delete" value="item entfernen"/>
    <img class="btn" width="19" height="19" title="Item löschen" src="img/bt-remove-icon.png"/>
</form>
TPL;
}

function createEditLink($itemId)
{
    return <<<TPL
<a href="index.php?dir=admin&site=bank_admin&itemId={$itemId}"><img title="Bearbeiten" src="img/edit.jpg"></a>
TPL;
}

function getWochenBeitrag($beitrag)
{
    if (sizeof($beitrag) > 0) {
        return $beitrag[0]['contribution'];
    }
    return 0;
}

$cssClasses = array(
    'gerade',
    'ungerade'
);

// gather categories
$bankService = new BankService();
$categoryTree = $bankService->getCategoryTree();

// gather items
$items = $bankService->fetchItems($kat_id, $keyword, $sort, $sort_order, $seite, $eps);
?>


<div>
    <div>
        <div style="display: inline-block; padding: 5px; float: left">
            <h3>Gildenbank</h3>
            <table id="overview" cellspacing="0" cellpadding="2">
                <colgroup>
                    <col width="200">
                    <col>
                    <col>
                    <col width="90">
                    <col width="79">
                </colgroup>
                <thead>
                <tr>
                    <th valign="top"><?= createSortLink($seite, 'name', $sort, $sort_order) ?></th>
                    <th valign="top"><?= createSortLink($seite, 'verfuegbar', $sort, $sort_order) ?></th>
                    <th valign="top"><?= createSortLink($seite, 'preis', $sort, $sort_order) ?></th>
                    <th valign="top"><?= createSortLink($seite, 'typ', $sort, $sort_order) ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                /* ausgelesene Daten ausgeben */
                $i = ($seite - 1) * $eps;

                foreach ($items as $item):
                    if ( ($item['status'] == 0 || $item['verfuegbar'] == 0) && $member['rolle'] < 3 ) {
                        continue;
                    }
                    $class = $cssClasses[$i % 2];
                    ?>
                    <tr class="<?php echo $class; ?>">
                        <td value="<?php echo ++$i ?>">
                            <a href="index.php?dir=bereiche&site=item&id=<?php echo $item['id'] ?>" class="_overlay"
                               rel="#item'.$row['itemId'].'">
                                <?php echo $item['name'] ?>
                            </a>

                        </td>
                        <td align="right"><?php echo $item['verfuegbar']; ?></td>
                        <td align="right"><?php echo $item['preis']; ?></td>
                        <td align="right"><?php echo $item['typ']; ?></td>
                        <td>
                            <?php if ($member['rolle'] > 2) : ?>
                                <?= createEditLink($item['id']) ?>
                                <?= createActionForm($item['id'], $item['status']) ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if (sizeof($items) == 0): ?>
                    <tr>
                        <td colspan="5">
                            <div class="message">Keine Items gefunden!</div>
                        </td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
            <ul>
                    <?php

                    $i = ($seite - 1) * $eps;
                    if ($i % $eps == 0) {
                        echo '<li><a href="index.php?dir=bereiche&site=bank&seite=' . ($seite + 1) . '&sort=' . $sort . '&sort_order=' . $sort_order . '">vor</a></li>';
                    } else {
                        echo '<li>vor</li>';
                    }

                    if ($seite > 1) {
                        echo '<li><a href="index.php?dir=bereiche&site=bank&seite=' . ($seite - 1) . '&sort=' . $sort . '&sort_order=' . $sort_order . '">zurück</a></li>';
                    } else {
                        echo '<li>zurück</li>';
                    }
                    ?>
                </ul>
        </div>
        <div style="display: inline-block; padding: 5px; float: left;">
            <h3>Deine letzten Bestellungen</h3>
            <table id="overview" cellspacing="0" cellpadding="2">
                <thead>
                    <tr>
                        <th>Datum</th>
                        <th>Item</th>
                        <th>Anzahl</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($lastTransactions as $transaction): ?>
                    <tr>
                        <td width="100" align="left"><?= $transaction['datum'] ?></td>
                        <td width="240" align="left"><?= $transaction['item'] ?></td>
                        <td width="100" align="center"><?= $transaction['menge'] ?></td>
                        <td width="100" align="center"><?= $transaction['status'] ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div id="kategorien" class="rechts">
        <fieldset id="account">
            <legend><b><?php echo $member['name']; ?></b></legend>

            <div>
                <a href="http://ewigkeit.shivtr.com/forum_threads/1199596" target="_blank" title="Infos">Konto
                    aufladen</a>

                <div>
                    <a href="index.php?dir=bereiche&site=dkp" title="DKP Überblick"><b><?= $userDkp ?></b> DKP</a>
                </div>
            </div>
            <div>
                <a href="index.php?dir=bereiche&site=pw&userId=<?php echo $member['id']; ?>"
                   title="Eventbeteiligung in letzten 60 Tagen"><b><?= $eventContribution->getParticipationRatio() ?>%
                        Eventbeteitigung</a>
            </div>
            <div>
                <a href="index.php?dir=bereiche&site=pw&userId=<?php echo $member['id']; ?>"
                   title="Wochenbeitrag"><b><?= getWochenBeitrag($weeklyContributions) ?></b> Wochenbeitrag</a>
            </div>
        </fieldset>
        <form action="index.php?dir=bereiche&site=bank" method="post">
            <fieldset id="search">
                <legend>Suche</legend>
                <input type="text" name="keyword" style="width: 100px;">
                <button type="submit"><span class="ui-icon ui-icon-search"></span></button>
            </fieldset>
        </form>

        <div id="kategorien">
            <ul>
                <?php foreach ($categoryTree as $mainCategory): ?>
                    <li>
                        <h3><?= $mainCategory->title ?></h3>
                        <ul>
                            <?php foreach ($mainCategory->childs as $subCategory): ?>
                                <li>
                                    <a href="index.php?dir=bereiche&site=bank&kat_id=<?= $subCategory->id ?>"><?= $subCategory->title ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

    </div>

</div>