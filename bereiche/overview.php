<?php
use Ewigkeit\Common\EventContribution;
use Ewigkeit\Common\GuildContribution;
use Ewigkeit\Sql\SqlAdapter;

// das muss weg!!!
$memberSessionData = $_SESSION;


/* variable aus der URL auslesen*/
$sort_order = (isset($_GET['sort_order'])) ? $_GET['sort_order'] : 'DESC';
$sort       = (isset($_GET['sort'])) ? $_GET['sort'] : 'Beitrag';
$seite      = (isset($_GET['seite'])) ? $_GET['seite'] : '1';
$eps        = 50; // Einträge pro Seite


// last member leaves
$queryString = "SELECT  `name`, DATE_FORMAT(`austritt`, '%d.%m.%Y %h:%i') as leaveDate
                FROM    `members`
                WHERE   `status` = 0
                ORDER BY    `austritt` DESC
                LIMIT 8";
$lastMemberLeaves = $sql->query($queryString, \PDO::FETCH_OBJ);


// retrieve the dkp amounts...
$queryString = "SELECT  m.id as memberId, m.name, SUM(dkps.dkp) as dkp
                FROM    members m,
                    (
                        SELECT  sum(`e`.`punkte`) as dkp, d.memberId
                        FROM	members m
                            LEFT JOIN   `dkp` d ON m.id = d.memberId
                            INNER JOIN  `events` `e` ON `e`.`id` = d.`eventId`
                        WHERE   m.status = 1
                        AND     m.twink = 0
                        GROUP BY    d.memberId
                        
                        UNION ALL
                        SELECT	sum((-1 * ib.`preis`) * ib.menge) as dkp, ib.memberId
                        FROM	members m
                            LEFT JOIN   `items_buy` ib ON  m.id = ib.memberId
                            INNER JOIN  `bank_item` bi ON bi.`id` = ib.`itemId`
                        WHERE   m.status = 1                             
                        AND     m.twink = 0
                        AND     ib.`status` <> 'storno'
                        GROUP BY    ib.memberId
                        
                        UNION ALL
                        SELECT	sum(b.`punkte`) as dkp, b.memberId
                        FROM	members m
                            LEFT JOIN   bonus b ON m.id = b.memberId
                        WHERE   m.status = 1
                        AND     m.twink = 0
                        GROUP BY    b.memberId
                    ) dkps
                WHERE   m.id = dkps.memberId
                AND     m.status = 1
                GROUP BY    m.id
                ORDER BY    m.name";
$memberDkps = $sql->query($queryString, \PDO::FETCH_OBJ);
// lazy way...find our user
foreach ($memberDkps as $val) {
    if ($val->memberId == $member['id']) {
        $user = $val;
        break;
    }
}
// Fallback für 0 nummern - muss später anders gelöst werden
if (empty($user)) {
    $user = (object)array(
    	'memberId' => $member['id'],
        'name' => $member['name'],
        'dkp' => 0,
    );
}

$contributions = array();

// get total and last week contribution including honor
$queryString = "SELECT  m.id, m.name, m.ruhm as honor, 0 as dkp,
		                b2.beitrag as contribution, b2.zuwachs as lastWeekContribution, DATE(m.eintritt) as eintritt
                FROM    members m, (
                            SELECT MAX(DATE(woche)) as lastWeek
                            FROM    beitrag
                        ) b1,
                        beitrag b2
                WHERE   m.status = 1
                AND     m.twink = 0
                AND     DATE(b2.woche) = b1.lastWeek
                AND     m.id = b2.memberId
                ORDER BY    ";
if ($sort != 'dkp') {
    $queryString .= "{$sort} {$sort_order}";
} else {
    $queryString .= "beitrag {$sort_order}";
}

$contributionInfo = $sql->query($queryString);
//echo '<pre>'.print_r($contributionInfo, 1).'</pre>';
foreach ($contributionInfo as $info) {
    $contributions[$info['id']] = $info;
}
unset($contributionInfo);

$totalDkps = 0;
// attach dkps to contributions
foreach ($memberDkps as $dkp) {
    if (isset($contributions[$dkp->memberId])) {
        $contributions[$dkp->memberId]['dkp'] = $dkp->dkp;
        if ($dkp->dkp > 0) {
            $totalDkps += $dkp->dkp;
        }
    }
}

// 
$rowCounter = 1;
//$cssClasses = array('even', 'odd');
$cssClasses = array('', '');

function getContributionColor($contribution) {
    if ($contribution > 130) {
        return '#009933';
    }
    if ($contribution < 88) {
        return '#FF0000';
    }
    return '#000000';
}

// member eintritt ermitteln
$minMemberTimeDone = false;	// bereits 60 tage in der Gilde?

$queryString = "SELECT	unix_timestamp(date_add(eintritt, interval 60 day)) as minMemberTime
				FROM	members
				WHERE	id = ".$member['id'];
$result = $sql->query($queryString);
if ($result !== false) {
    if (time() > $result[0]['minMemberTime']) {
        $minMemberTimeDone = true;
    }
}
unset($result);


// TODO: auslagern in eine funktion oder bereich ... ka wie das gehen soll :D
$eventContribution = new EventContribution();
$eventContribution->determineContributionForMember($member['id']);
$eventParticipation = $eventContribution->getParticipations();
$allEvents = $eventContribution->getEvents();

$guildContribution = new GuildContribution();
$weeklyContributions = $guildContribution->getForMember($member['id']);

$allowedBankAccess = true;
if (!$minMemberTimeDone
    || ($eventContribution->getParticipationRatio() < 25
        && $guildContribution->getAverageForMember($member['id']) < 450) ) {
    $allowedBankAccess = false;
}
?>
<?php if ($member['rolle'] > 3): ?>
<br /><p><strong>Gesamt DKP:</strong> <?= $totalDkps; ?></p>
<?php endif; ?>

<div class="rechts">
    <fieldset>
        <legend>letzte Austritte</legend>
        <table width="100%">
            <?php foreach ($lastMemberLeaves as $memberLeave): ?>
            <tr>
                <td><strong><?= $memberLeave->name; ?></strong></td>
                <td><?= $memberLeave->leaveDate; ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </fieldset>
    <fieldset>
		<legend>Ruhmpunkte</legend>
		<p><img title="Ruhm" src="img/Ruhm_Icon.png" style="float: left" vspace="5" hspace="10">
            <strong>Erreiche Ruhm:</strong>
            <br />Sammle Ruhm indem
            <br />Du die Gilde oder deren
            <br />Member unterstützt!</p>
        <p><strong>Information:</strong>
            <br />Dies ist eine weitere kommende Funktion unserer Gilde. Sammelt Ruhm um euch noch stärker an Ewigkeit zu binden, und werdet Ruhmreich! Diese Funktion ist eine nötige Erneuerung um unser DKP-System stabil zu halten, außerdem bringt es nach einigen überlegen auch nutzvolle Zwecke mit sich. Setzt euch an die Spitze!
            <br />Viel Spass.</p>
        <p><strong>Wie komme ich an Ruhm?:</strong></p>
        <ul>
            <li>- <a href="#" onclick="openDialog('#dlg-dkp-donate-guild');">Spende</a> an die Gilde</li><li>
            <li>- <a href="#" onclick="openDialog('#dlg-dkp-donate-user');">Spende</a> an Member</li><li>
            <li>- weitere <a href="#" onclick="openDialog('#dlg-dkp-donate-info');">Möglichkeiten</a></li>
        </ul>
        <strong>Fragen?</strong> Klick <a href="http://ewigkeit.shivtr.com/forum_threads/1602658" target="_blank">hier</a>
    </fieldset>
    <fieldset>
		<legend>Info Box</legend>
	     <p>Das Script wird ständig soweit es die Lust und Zeit zulässt weiter entwickelt daher enstehen kleine Baustellen die meist erkennbar durch "In Arbeit" gemacht werden. Nicht wundern xD</p>
	</fieldset>
	<img src="./img/ggg.png" />
</div>

<table id="overview" cellspacing="0" cellpadding="2">
    <thead>
        <tr>
            <th valign="top">Member</th>
            <th valign="top">Gesamt</th>
            <th valign="top">Woche</th>
            <th valign="top">DKP</th>
            <th valign="top">Ruhm</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($contributions as $contribution): ?>
            <?php
            $dkpMemberInfo = $contribution['name'];
            if ($member['rolle'] > 3) {
                $dkpMemberInfo = '<a href="index.php?dir=admin&site=edit_user&userId='.$contribution['id'].'">'.$contribution['name'].'</a>';
            }
            ?>
        <tr class="<?= $cssClasses[$rowCounter++ % 2]?>" <?= ($contribution['id'] == $user->memberId ? 'style="font-weight: bold; background: #AFAFAF;"' : '')?>>
            <td width="100" title="Eintritt: <?= $contribution['eintritt']?>"><?= $dkpMemberInfo ?></td>
            <td width="52" align="right"><?= $contribution['contribution']; ?></td>
            <td width="65" align="right" style="color: <?= getContributionColor($contribution['lastWeekContribution']); ?>"><?= $contribution['lastWeekContribution']; ?>
            <td width="35" align="right"><?= $contribution['dkp']; ?></td>
            <td width="42" align="right"><?= $contribution['honor']; ?><img title="Ruhmpunkte" src="img/20px-Ruhm_Icon.png" height="11" width="12"  vspace="0" hspace="1"></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>




<script>
function openDialog(id) {
	$(id).dialog( {
	    modal: true,
	    width: 500, minHeight: 150
	    }
    );
};

function asyncDonate(frm) {
	jQuery.post(frm.action, jQuery(frm).serialize())
	   .done(function(data) {
		   if (data == 'ok') {
			   alert('Vielen Dank für deine Spende');
			   document.location.reload();
		   } else {
			   alert('Spendenvorgang nicht erfolgreich. Melde dich im Forum');
		   }
	   });
	return false;	
}
// tablesort =)
$(function(){
    $("#overview").tablesorter({sortList:[[0,0]]});
});

</script>
<?php
if ($allowedBankAccess) {    
    include 'partial/dkp_donate_user_dialog.phtml';    
} else {
	$dialogName = 'dlg-dkp-donate-user';
	include 'partial/dkp_donate_denied.phtml';
}
if ($user->dkp > 3) {
    include 'partial/dkp_donate_guild_dialog.phtml';
} else {
    $dialogName = 'dlg-dkp-donate-guild';
    include 'partial/dkp_donate_denied.phtml';
}
include 'partial/dkp_donate_info_dialog.phtml';
?>
	