<?php
use Ewigkeit\Sql\SqlAdapter;
use Ewigkeit\Common\EquipWishEntity;
use Ewigkeit\Common\VexxerEquipEntity;
use Ewigkeit\Common\ElementarEquipEntity;
use Ewigkeit\Editor\EditorService;

if (empty($_SESSION)) {
    die('Bitte anmelden!');
}

function prepareEntityData($data) {
    
    $vexxer77 = new VexxerEquipEntity();
    $vexxer85 = new VexxerEquipEntity();
    $vexxer90 = new VexxerEquipEntity();
    $vexxer95 = new VexxerEquipEntity();
    $elementar77 = new ElementarEquipEntity();
    $elementar85 = new ElementarEquipEntity();
    $elementar95 = new ElementarEquipEntity();

    // assign everyting...
    $vexxer77->weapon = isset($data['vexxer77_weapon']) ? $data['vexxer77_weapon'] : 0;    
    $vexxer77->hands = isset($data['vexxer77_hands']) ? $data['vexxer77_hands'] : 0;
    $vexxer77->boots = isset($data['vexxer77_boots']) ? $data['vexxer77_boots'] : 0;
    
    $vexxer85->weapon = isset($data['vexxer85_weapon']) ? $data['vexxer85_weapon'] : 0;
    $vexxer85->hands = isset($data['vexxer85_hands']) ? $data['vexxer85_hands'] : 0;

    $vexxer90->weapon = isset($data['vexxer90_weapon']) ? $data['vexxer90_weapon'] : 0;
    $vexxer90->hands = isset($data['vexxer90_hands']) ? $data['vexxer90_hands'] : 0;

    $vexxer95->weapon = isset($data['vexxer95_weapon']) ? $data['vexxer95_weapon'] : 0;
    
    $elementar77->head = isset($data['ele77_head']) ? $data['ele77_head'] : 0;
    $elementar77->body = isset($data['ele77_body']) ? $data['ele77_body'] : 0;
    $elementar77->shoulders = isset($data['ele77_shoulders']) ? $data['ele77_shoulders'] : 0;
    $elementar77->hands = isset($data['ele77_hands']) ? $data['ele77_hands'] : 0;
    $elementar77->belt = isset($data['ele77_belt']) ? $data['ele77_belt'] : 0;
    $elementar77->leg = isset($data['ele77_leg']) ? $data['ele77_leg'] : 0;
    $elementar77->boots = isset($data['ele77_boots']) ? $data['ele77_boots'] : 0;
    
    $elementar85->head = isset($data['ele85_head']) ? $data['ele85_head'] : 0;
    $elementar85->body = isset($data['ele85_body']) ? $data['ele85_body'] : 0;
    $elementar85->shoulders = isset($data['ele85_shoulders']) ? $data['ele85_shoulders'] : 0;
    $elementar85->hands = isset($data['ele85_hands']) ? $data['ele85_hands'] : 0;
    $elementar85->belt = isset($data['ele85_belt']) ? $data['ele85_belt'] : 0;
    $elementar85->leg = isset($data['ele85_leg']) ? $data['ele85_leg'] : 0;
    $elementar85->boots = isset($data['ele85_boots']) ? $data['ele85_boots'] : 0;
    
    $elementar95->head = isset($data['ele95_head']) ? $data['ele95_head'] : 0;
    $elementar95->body = isset($data['ele95_body']) ? $data['ele95_body'] : 0;
    $elementar95->shoulders = isset($data['ele95_shoulders']) ? $data['ele95_shoulders'] : 0;
    $elementar95->hands = isset($data['ele95_hands']) ? $data['ele95_hands'] : 0;
    $elementar95->belt = isset($data['ele95_belt']) ? $data['ele95_belt'] : 0;
    $elementar95->leg = isset($data['ele95_leg']) ? $data['ele95_leg'] : 0;
    $elementar95->boots = isset($data['ele95_boots']) ? $data['ele95_boots'] : 0;
    
    $equip = new EquipWishEntity();
    $equip->vexxer77 = $vexxer77;
    $equip->vexxer85 = $vexxer85;
    $equip->vexxer90 = $vexxer90;
    $equip->vexxer95 = $vexxer95;
    $equip->elementar77 = $elementar77;
    $equip->elementar85 = $elementar85;
    $equip->elementar95 = $elementar95;
    

    $equip->charClassId = isset($data['charClassId']) ? $data['charClassId'] : 0;
    
    return $equip;
}


function saveEquipWish(EquipWishEntity $entity, $memberId) {
    $vexxer77 = $entity->vexxer77;
    $vexxer85 = $entity->vexxer85;
    $vexxer90 = $entity->vexxer90;
    $vexxer95 = $entity->vexxer95;
    $elementar77 = $entity->elementar77;
    $elementar85 = $entity->elementar85;
    $elementar95 = $entity->elementar95;

    $sql = SqlAdapter::getInstance();
    
    $queryString = "INSERT  INTO equip_wish
                            (memberId, charClassId,
                            vexxer77_weapon, vexxer77_hands, vexxer77_boots,
                            vexxer85_weapon, vexxer85_hands,
                            vexxer90_weapon, vexxer90_hands,
                            vexxer95_weapon,
                            ele77_head, ele77_body, ele77_shoulders, ele77_hands, ele77_belt, ele77_leg, ele77_boots,
                            ele85_head, ele85_body, ele85_shoulders, ele85_hands, ele85_belt, ele85_leg, ele85_boots,
                            ele95_head, ele95_body, ele95_shoulders, ele95_hands, ele95_belt, ele95_leg, ele95_boots)
                    VALUES  (
                        {$sql->quote($memberId)}, {$sql->quote($entity->charClassId)},
                        {$sql->quote($vexxer77->weapon)}, {$sql->quote($vexxer77->hands)}, {$sql->quote($vexxer77->boots)},
                        {$sql->quote($vexxer85->weapon)}, {$sql->quote($vexxer85->hands)},
                        {$sql->quote($vexxer90->weapon)}, {$sql->quote($vexxer90->hands)},
                        {$sql->quote($vexxer95->weapon)},
                        {$sql->quote($elementar77->head)}, {$sql->quote($elementar77->body)},
                        {$sql->quote($elementar77->shoulders)}, {$sql->quote($elementar77->hands)},
                        {$sql->quote($elementar77->belt)}, {$sql->quote($elementar77->leg)},
                        {$sql->quote($elementar77->boots)}, 
                        {$sql->quote($elementar85->head)}, {$sql->quote($elementar85->body)},
                        {$sql->quote($elementar85->shoulders)}, {$sql->quote($elementar85->hands)},
                        {$sql->quote($elementar85->belt)}, {$sql->quote($elementar85->leg)},
                        {$sql->quote($elementar85->boots)},
                        {$sql->quote($elementar95->head)}, {$sql->quote($elementar95->body)},
                        {$sql->quote($elementar95->shoulders)}, {$sql->quote($elementar95->hands)},
                        {$sql->quote($elementar95->belt)}, {$sql->quote($elementar95->leg)},
                        {$sql->quote($elementar95->boots)}
                    )
                    ON DUPLICATE KEY UPDATE
                        charClassId = VALUES(charClassId),
                        vexxer77_weapon = VALUES(vexxer77_weapon),                        
                        vexxer77_hands = VALUES(vexxer77_hands),
                        vexxer77_boots = VALUES(vexxer77_boots),
                        vexxer85_weapon = VALUES(vexxer85_weapon),                        
                        vexxer85_hands = VALUES(vexxer85_hands),
                        vexxer90_weapon = VALUES(vexxer90_weapon),
                        vexxer90_hands = VALUES(vexxer90_hands),
                        vexxer95_weapon = VALUES(vexxer95_weapon),
                        ele77_head = VALUES(ele77_head),
                        ele77_body = VALUES(ele77_body),
                        ele77_shoulders = VALUES(ele77_shoulders),
                        ele77_hands = VALUES(ele77_hands),
                        ele77_belt = VALUES(ele77_belt),
                        ele77_leg = VALUES(ele77_leg),
                        ele77_boots = VALUES(ele77_boots),
                        ele85_head = VALUES(ele85_head),
                        ele85_body = VALUES(ele85_body),
                        ele85_shoulders = VALUES(ele85_shoulders),
                        ele85_hands = VALUES(ele85_hands),
                        ele85_belt = VALUES(ele85_belt),
                        ele85_leg = VALUES(ele85_leg),
                        ele85_boots = VALUES(ele85_boots),
                        ele95_head = VALUES(ele95_head),
                        ele95_body = VALUES(ele95_body),
                        ele95_shoulders = VALUES(ele95_shoulders),
                        ele95_hands = VALUES(ele95_hands),
                        ele95_belt = VALUES(ele95_belt),
                        ele95_leg = VALUES(ele95_leg),
                        ele95_boots = VALUES(ele95_boots)
                        ";
                        
    $sql->exec($queryString);    
}


if (isset($_POST['act']) && $_POST['act'] == 'save_wish') {
    $entity = prepareEntityData($_POST);    
    saveEquipWish($entity, $member['id']);
    header('Location: index.php?dir=bereiche&site=equip_wish');
    die();
}

function readEquipWish($memberId) {
    $sql = SqlAdapter::getInstance();
    
    $queryString = "SELECT  charClassId,
                            vexxer77_weapon, vexxer77_hands, vexxer77_boots,
                            vexxer85_weapon, vexxer85_hands,
                            vexxer90_weapon, vexxer90_hands,
                            vexxer95_weapon,
                            ele77_head, ele77_body, ele77_shoulders, ele77_hands, ele77_belt, ele77_leg, ele77_boots,
                            ele85_head, ele85_body, ele85_shoulders, ele85_hands, ele85_belt, ele85_leg, ele85_boots,
                            ele95_head, ele95_body, ele95_shoulders, ele95_hands, ele95_belt, ele95_leg, ele95_boots
                    FROM    equip_wish
                    WHERE   memberId = {$sql->quote($memberId)}";
    $result = $sql->query($queryString);
    
    if (sizeof($result) > 0) {
        return prepareEntityData($result[0]);
    }
    
    $equip = new EquipWishEntity();
    $equip->vexxer77 = new VexxerEquipEntity();
    $equip->vexxer85 = new VexxerEquipEntity();
    $equip->vexxer90 = new VexxerEquipEntity();
    $equip->vexxer95 = new VexxerEquipEntity();
    $equip->elementar77 = new ElementarEquipEntity();
    $equip->elementar85 = new ElementarEquipEntity();    
    $equip->elementar95 = new ElementarEquipEntity();

    return $equip;
}

$entity = readEquipWish($member['id']);

$editorService = new EditorService();
$charClasses = $editorService->fetchCharClasses();

?>
<div style="padding: 25px;">
    <h1>Ausrüstungs-Wunsch</h1>
    <p>Bitte wähle hier aus welche Ausrüstungen du dir von der Gilde wünschen würdest. <br>Pro Member (egal wieviele Accounts) kannst du einen Wunsch pro Körberteil wählen. </p>
	
	<?php if ($member['rolle'] > 3): ?>
	<br /><p><strong>Admin Notiz:</strong> könnten wir das hier so machen das wir für jede klasse wählen könnten was man haben mag man bekommt ja bei ewk nur 1 x 1 item pro körberteil klasse wäre ja egal^^ - dann würden wir das hier gleich für twinks mit einrichten</p>
	<?php endif; ?>

    <p><strong>Legende:</strong><br />        
        <button class="ui-button ui-corner-all"><span class="ui-icon ui-icon-plus"></span></button> Interesse besteht<br />
        <button class="ui-button ui-corner-all"><span class="ui-icon ui-icon-minus"></span></button> kein Interesse<br />
        <button class="ui-button ui-corner-all"><span class="ui-icon ui-icon-help"></span></button> evtl. Interesse<br />
        <button class="ui-button ui-corner-all"><span class="ui-icon ui-icon-check"></span></button> bereits ausgerüstet/erhalten<br />
    </p>
    <br /><br />
    <?php include './partial/equip_wish.phtml'; ?>
</div>