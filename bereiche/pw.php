<?php
use Ewigkeit\Common\EventContribution;
use Ewigkeit\Common\GuildContribution;
use Ewigkeit\Sql\SqlAdapter;

require_once 'functions/MemberAdapter.php';

// ob member im Normalfall wirklich false ist ...?
if (($member === false || fetchGP('userId') != $member['id']) && $member['rolle'] < 3) {
	echo '<div class="message">Du bist nicht angemeldet oder hast hier keinen Zugriff!</div>';
	die();
}

function getTwinkList($memberName) {
    $sql = SqlAdapter::getInstance();
    /* twink infos */
    $twinks = array();
    $queryString = "SELECT `name` FROM `members` WHERE `twink_von` = {$sql->quote($memberName)}";    
    $result = $sql->query($queryString);
    if ($result !== false) {
        return $result;
    }
    return array();
}

function savePassword() {
    $sql = SqlAdapter::getInstance();
    $pwHash = md5(fetchGP('pass'));
    $userId = fetchGP('id');
    $userRole = fetchGP('rolle');
    $queryString = "UPDATE  members
                    SET     pass = {$sql->quote($pwHash)},
                            rolle = {$sql->quote($userRole)}
                    WHERE   id = {$sql->quote($userId)}";
    $sql->exec($queryString);
}

/* saving new password the hard way and not very secure ... */
if (fetchGP('act') == 'savepw') {
    savePassword();
    $userId = fetchGP('userId');
    header('Location: index.php?dir=bereiche&site=pw&userId='.$userId);
    die();
}


/* ===========================================
 * NEW STUFF FOR HOLIDAY HANDLING (START)
 * ----------------------------------------------------------------------------*/

// nicht schön :(
$memberId = $_REQUEST['userId'];

$memberAdapter = new MemberAdapter(SqlAdapter::getInstance());

// handle holidays
if (isset($_POST['holiday']) && isset($_POST['days']) && isset($_POST['userId'])) {	
	$days = (int)$_POST['days'];
	$memberId = (int)$_POST['userId'];
	if (!($days <= 0 || $memberId <= 0)) {
		$memberAdapter->takeHolidays($memberId, $days);		
		header('Location: index.php?dir=bereiche&site=pw&userId='.$memberId);
		die();
		
	}
}

// load the member :)
$memberObj = $memberAdapter->loadMember($memberId);


/* ===========================================
 * NEW STUFF FOR HOLIDAY HANDLING (END)
 * ----------------------------------------------------------------------------*/
$user = (object) array(
	'id'           => '0',
	'name'         => '',
	'kat_id'       => '0',
	'menge'        => '',
	'max'          => '0',
	'preis'        => '',
	'periode'      => '0',
	'beschreibung' => '',
);

$sort_order = fetchGP('sort_order', 'ASC');
$sort       = fetchGP('sort', 'Item');
$kat_id     = fetchGP('kat_id',"'%'");
$keyword    = fetchGP('keyword', '%');
$seite      = fetchGP('seite', 1);
$eps        = 30; // Einträge pro Seite


// aktualisierungszeiten holen
$lastDedicationDate = null;
$lastBonusDate = null;

$queryString = "SELECT	max(woche)as lastdate
				FROM	beitrag
				WHERE	`memberId` = ".$memberId;
$result = $sql->query($queryString);
if ($result !== false) {
	$lastDedicationDate = $result[0]['lastdate'];
}

$queryString = "SELECT	max(woche) as lastdate
				FROM	bonus
				WHERE	`memberId` = ".$memberId;
if ($result !== false) {
	$lastBonusDate = $result[0]['lastdate'];
}
// min urlaubstage
$minDaysToTake = 3;

/* event teilnahmen */

$eventCount = 0;
$eventParticipationCount = 0;
$allEvents = array();
$eventParticipation = array();

$twinks = getTwinkList($member['name']);

// anzahl events der letzten 60 tage

// member eintritt ermitteln
$minMemberTimeDone = false;	// bereits 60 tage in der Gilde?

$queryString = "SELECT	unix_timestamp(date_add(eintritt, interval 60 day)) as minMemberTime
				FROM	members
				WHERE	id = ".$memberId;
$result = $sql->query($queryString);
if ($result !== false) {
	if (time() > $result[0]['minMemberTime']) {
		$minMemberTimeDone = true;
	}
}
unset($result);

// TODO: auslagern in eine funktion oder bereich ... ka wie das gehen soll :D
$eventContribution = new EventContribution();
$eventContribution->determineContributionForMember($memberId);
$eventParticipation = $eventContribution->getParticipations();
$allEvents = $eventContribution->getEvents();

$guildContribution = new GuildContribution();
$weeklyContributions = $guildContribution->getForMember($memberId);

$allowedBankAccess = true;
if (!$minMemberTimeDone
	|| ($eventContribution->getParticipationRatio() < 25
		&& $guildContribution->getAverageForMember($member['id']) < 450) ) {
	$allowedBankAccess = false;
}

// retrieve dkp statistics

$dkpInfo = array(
    'current' => 0,
    'total' => 0,
    'events' => 0,
    'bonus' => 0,
    'spent' => 0,
    'donated' => 0,
    'gifted' => 0,
);
// recieved dkp
$queryString = "SELECT  (events.dkp + bonus.dkp) as total, events.dkp as events, bonus.dkp as bonus
                FROM
                  (
                  SELECT  sum(punkte) as dkp
                  FROM	`dkp` d
                    INNER JOIN `events` `e`
                        ON `e`.`id` = d.`eventId`
                  WHERE	memberId = {$member['id']}
                  AND   e.name <> 'Gildenspende'
                  AND   e.name not like 'Spende%'
                  ) events,
                  (
                  SELECT	sum(punkte) as dkp
                  FROM	bonus
                  WHERE	memberId = {$member['id']}
                  ) bonus";
$result = $sql->query($queryString);
if ($result !== false) {
    $result = $result[0];
    $dkpInfo['total'] = $result['total'];
    $dkpInfo['events'] = $result['events'];
    $dkpInfo['bonus'] = $result['bonus'];
}

// spent dkp
$queryString = "SELECT	sum(ib.preis) as spent
				FROM `items_buy` ib
					INNER JOIN `bank_item` bi
					ON bi.`id` = ib.`itemId`
				WHERE	memberId = {$member['id']}
                AND ib.`status` = 'gebucht'";
$result = $sql->query($queryString);
if ($result !== false) {
    $result = $result[0];
    $dkpInfo['spent'] = $result['spent'];
}

// donated dkp
$queryString = "SELECT  sum(IF(punkte < 0, punkte, 0)) as donated, sum(IF(punkte > 0, punkte, 0)) as gifted
                FROM	`dkp` d
                  INNER JOIN `events` `e`
                    ON `e`.`id` = d.`eventId`
                WHERE	memberId = {$member['id']}
                AND   (e.name like 'Spende%' OR e.name like 'Gildenspende')";
$result = $sql->query($queryString);
if ($result !== false) {
    $result = $result[0];

    $dkpInfo['donated'] = abs($result['donated']);
    $dkpInfo['gifted'] = abs($result['gifted']);
}
$dkpInfo['current'] = $dkpInfo['total'] - $dkpInfo['spent'] - $dkpInfo['donated'] + $dkpInfo['gifted'];

$cssClasses = array('gerade', 'ungerade');

?>

<script>
function checkMinHolidays(obj) {
	var minDays = <?php echo $minDaysToTake ?>;
	if (parseInt(obj.value) >= minDays) {
		return true;
	}
	alert('Du musst mindestens ' + minDays + ' Tag(e) Urlaub nehmen');
	return false;
}

$(function() {
    $( "#slider-holiday").slider({
        range: "min",
        value: 1,
        min: 3,
        max: <?= $memberObj->holidays ?>,
        slide: function( event, ui ) {
            $( "#holidays" ).val(ui.value );
      }
    });
    $( "#holidays" ).val( $("#slider-holiday" ).slider( "value" ) );
  }
);
  
</script>

<div>
    <fieldset id="infobox" style="height: 200px; float: left; width: 340px;">
        <legend><b>Urlaubsmodus</b></legend>
        <?php $holidays = $memberAdapter->getCurrentHoliday($memberObj->getId()); ?>
        <?php if ($memberObj->isHoliday && $holidays !== false): ?>			
			<p>
				<strong>Du befindest dich aktuell im Urlaub für <?php echo $holidays->days ?> Tag(e) ^_^</strong><br />
				Vom <?= date('d.m.Y H:i:s', $holidays->tsStart) ?> bis <?= date('d.m.Y H:i:s', $holidays->tsEnd) ?>.
			</p>
		<?php else: ?>
			<form method="post" action="index.php" onsubmit="return checkMinHolidays(this.days);">
			<input type="hidden" name="dir" value="bereiche" />
			<input type="hidden" name="site" value="pw" />
			<input type="hidden" name="userId" value="<?= $memberObj->getId() ?>" />
			
			<label for="amount">verfügbare Urlaubstage:</label>		
			<strong><?= $memberObj->holidays ?></strong> Tage<br>
			<p>
				<label for="holidays">Ich beantrage</label>
				<input type="text" readonly="readonly" id="holidays" name="days" style="width: 25px" maxlength="2" style="border: 0; color: #f6931f; font-weight: bold;" />
				Tage Urlaub
			</p>			
			<div id="slider-holiday" style="width: 320px;"></div>
			<br />
			<input type="submit" name="holiday" value="Urlaub einreichen" />
		</form>
		<?php endif; ?>
		<p>Information: ein Urlaub geht mindestens 3 Tage | In diesem Zeitraum erhälst du keine Minus DKP (berechnet anhand des Beitragsdurchschnitts^^)</p>
	</fieldset>

	<fieldset id="infobox" style="height: 200px; float: left; width: 240px;">
		<legend><b>Twinks</b></legend>
		Hier findest du deine bei uns geführten Twinks..<br><br>
		<?php foreach ($twinks as $key => $val): ?>
		<input type="button" value="<?php echo $val['name'] ?>">
		<?php endforeach; ?>
    </fieldset>

	<form method="post" action="index.php">
        <input type="hidden" name="act" value="savepw" />
        <input type="hidden" name="dir" value="bereiche" />
        <input type="hidden" name="site" value="pw" />
        <input type="hidden" name="userId" value="<?= $memberObj->getId() ?>" />

        <fieldset id="infobox" style="height: 200px; float: left; width: 340px;">
            <legend><b>Passwort ändern</b></legend>
            <input type="hidden" name="id" value="<?= $memberObj->getId(); ?>" />
            <input type="hidden" name="rolle" value="<?= $memberObj->role; ?>" />
            <ul>
                <li>
                    <label for="pass">Passwort</label>
                    <input type="text" name="pass" />
                </li>
            </ul>
            <button type="submit">speichern</button>
        </fieldset>
	</form>
</div>
<br clear="left" />
<div>
	<fieldset id="infobox">
	<legend><b>Informationen bzw. Statistiken</b></legend>

	<?php if (!$minMemberTimeDone): ?>
	<p><strong style="color: #FF0000">Du bist noch keine 60 Tage Mitglied - Kein Zugriff auf die Bank!</strong></p>	
	<?php endif; ?>
	<?php if ($minMemberTimeDone && !$allowedBankAccess): ?>
	<p><strong style="color: #FF0000">Dein Beitrag und oder deine Event-Beteiligung ist zu niedrig - Kein Zugriff auf die Bank!</strong></p>	
	<?php endif; ?>
	
	
	<div id="kategorien" class="rechtssan">
		<fieldset id="infobox1">
			<legend><b>Letzte Aktualisierungen</b></legend>
			<label>Beitrag:</label>
			<b><?php echo $lastDedicationDate; ?></b><br>

			<label>Wochenbonus:</label>
			<b><font title="List:Beitrag letzter Wochen" color=""><?php echo $lastBonusDate; ?></font></b>
	   </fieldset>
	</div>
	
	<div id="kategorien" class="rechtssan">
		<fieldset id="infobox1">
		<legend><b>Teilnahmen letzte 60 Tagen:</b></legend>
		<strong>Event-Beteiligung:</strong> <?php echo $eventContribution->getParticipationRatio() ?>%
		<table id="overview" cellspacing="0" cellpadding="2">
			<colgroup>
				<col width="220">
				<col width="190">
			</colgroup>
			<tr>
				<th>Event</th>
				<th>Teilnahmen</th>
			</tr>
			<?php foreach ($allEvents as $key => $val): ?>
			<tr>
				<td><?= $key ?></td>
				<?php if (isset($eventParticipation[$key])): ?>
				<td><?= $eventParticipation[$key] . " / " . $val ?></td>
				<?php else: ?>
				<td><?= "0 / " . $val ?></td>
				<?php endif;?>
			</tr>
			<?php endforeach; ?>
		</table>
	   </fieldset>
	</div>	
	
	<div id="kategorien" class="rechtssan">
		<fieldset id="infobox1">
			<legend><b>Wochenbeitrag letzten <?php echo $guildContribution->getIntervalDays() ?> Tage:</b></legend>
			<strong>Wochendurchschnitt:</strong> <?php echo $guildContribution->getAverageForMember($member['id']) ?> Beitrag
			<table id="overview" cellspacing="0" cellpadding="2">
				<colgroup>
					<col width="90">
					<col width="190">
				</colgroup>
				<tr>
					<th>Beitrag</th>
					<th>Datum</th>
				</tr>
				<?php foreach ($weeklyContributions as $key => $val): ?>
				<tr class="<?php echo $cssClasses[$key % 2] ?>">
					<td><?php echo $val['contribution'] ?></td>
					<td><?php echo date('d.m.Y H:i', $val['tsDatetime']) ?></td>
				</tr>
				<?php endforeach; ?>
			</table>
		</fieldset>
	</div>
	<div id="kategorien" class="rechtssan">
		<fieldset id="infobox1">
			<legend><b>DKP-Punkte:</b></legend>
            <strong>aktuell:</strong> <?= $dkpInfo['current'] ?><br>
			<strong>bisher erhalten:</strong> <?= $dkpInfo['total'] ?><br>
            &nbsp;&nbsp;<strong>durch Events:</strong> <?= $dkpInfo['events'] ?><br>
            &nbsp;&nbsp;<strong>durch Wochenbonus:</strong> <?= $dkpInfo['bonus'] ?><br>
			<strong>ausgegeben in Gildenbank:</strong> <?= $dkpInfo['spent'] ?><br>
			<strong>Spenden:</strong><br />
            &nbsp;&nbsp;<strong>Einnahmen:</strong> <?= $dkpInfo['gifted'] ?><br />
            &nbsp;&nbsp;<strong>Ausgaben:</strong> <?= $dkpInfo['donated'] ?><br />

		</fieldset>
	</div>
	</fieldset>
</div>


	
