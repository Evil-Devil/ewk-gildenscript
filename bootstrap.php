<?php
$pathInfo = realpath('./');
define('BASE_PATH', $pathInfo);

require BASE_PATH.'/config/config.php';
require BASE_PATH.'/lib/SplClassLoader.php';
$classLoader = new SplClassLoader('Ewigkeit', BASE_PATH.'/lib/vendor');
$classLoader->register();