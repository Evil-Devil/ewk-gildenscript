<?php
ob_start();
ini_set('display_errors', 'on');
error_reporting(E_ALL);

require 'bootstrap.php';

include('functions/base.php');

use Ewigkeit\Common\Donation;
use Ewigkeit\Sql\SqlAdapter;

// later this will be hidden somewhere ...
$sql = SqlAdapter::getInstance();

// small utility function ...
function fetchGP($name, $default = null) {
	if (isset($_GET[$name])) {
		if (!empty($_GET[$name])) {
			return $_GET[$name];
		}
		return $default;
	}
	if (isset($_POST[$name])) {
		if (!empty($_POST[$name])) {
			return $_POST[$name];
		}
		return $default;
	}
	return $default;
}

// should move these somewhere else...
function selected($source, $target) {
    if ($source == $target) {
        return 'selected="selected"';
    }
    return '';
}

function checked($source, $target) {
    if ($source == $target) {
        return 'checked="checked"';
    }
    return '';
}


// bei Ajax kein layou rendern!
if ($isAjax) {
	exit;
}


/* SPENDER INFO ZEILE - START */
$spendenDkp = 0;
$itemCosts = 0;
$spendenKommentare = array();

// mach die DB abfragen nur, wenn es notwendig ist
if ($member['status'] > 0) {
    $donation = new Donation();
    
	$queryString = "SELECT	SUM(`menge`) as `spende`
					FROM	`messages_spende`";
	$result = $sql->query($queryString);
	if ($result !== false) {
		$spendenDkp = $result[0]['spende'];
	}
	
	$spendenKommentare = $donation->retrieveComments();
	

	$queryString = "SELECT	SUM(`preis` * `menge`) as `preis`
			FROM `items_buy`
			WHERE `status` = 'gebucht'";
	$result = $sql->query($queryString);
	if ($result !== false) {
		$itemCosts = $result[0]['preis'];
	}
	unset($result);
}
/* SPENDER INFO ZEILE - ENd */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'header.phtml'; ?>	
</head>
<body>
<div id="theme-left"></div>
<div id="theme-right"></div>
<div id="page">
    <div id="top">
        <div class="container-inner">
            <img id="logo"  vspace="0" hspace="3"  src="img/cooltext1408891200.gif" title="Willkommen bei Ewigkeit">
            <?php include 'navigation.phtml' ?>

            <?php if ($member['rolle'] > 0 && $member['status'] > 0) : ?>
            <marquee scrollamount="3" scrolldelay="1" onmouseover="this.stop()" onmouseout="this.start()">
                <a href="http://ewigkeit.shivtr.com/forum_threads/1199596" target="_blank" style="color: #CCCCCC; font-size: 10px; font-family: Verdana,Helvetica,Arial,sans-serif;">
                    ausgezahlte DKP's: &gt;&gt; <strong><?php echo $itemCosts; ?></strong> &lt;&lt; +++ Spende an die Gilde: <strong><?php echo $spendenDkp; ?></strong> +++
                    <?php foreach ($spendenKommentare as $val) {
                        echo '<strong>'.$val['spender'].':</strong> '.$val['komentar'].' + + + ';
                    } ?>
                </a>
            </marquee>
            <?php endif; ?>
        </div>
    </div>
    <div id="content">
        <div class="container-mitte"></div>

        <div class="black_overlay" id="fadel">&nbsp;</div>
        <?php if (isset($message)) : ?>
            <div class="message"><?php echo $message; ?></div>
        <?php endif; ?>

        <?php
        $baseDir = realpath('.');

        if (isset($_REQUEST['dir']) && isset($_REQUEST['site'])) {
            if ($member['rolle'] > 3) {
                include 'partial/admin_notice.php';
            }
            if ($member['twink'] != 1) {
                include($baseDir.'/'.$_REQUEST['dir'].'/'.$_REQUEST['site'].'.php');
            }  else {
                unset($member);
                if (session_destroy()) :
                    echo '<div>Du wirst als Twink geführt - bitte logge aus oder melde dich bei deinem Leader!</div>';
                endif;
            }
        }
        ?>
    </div>
</div>
</body>
</html>
<?php ob_end_flush(); ?>