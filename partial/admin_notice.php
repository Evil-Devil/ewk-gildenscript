<?php
use Ewigkeit\Sql\SqlAdapter;

function loadAdminNotice() {    
    $sql = SqlAdapter::getInstance();
    
    $queryString = "SELECT  message
                    FROM    admin_notices
                    WHERE   site = {$sql->quote($_SERVER['QUERY_STRING'])}";
    
    $result = $sql->query($queryString);    
    if (sizeof($result) > 0) {
        return $result[0]['message'];
    }
    return '';
}

?>
<br />
<dl>
    <dd onclick="$('#admin-notice').toggle();"><strong>Admin-Notiz</strong></dd>
    <dd id="admin-notice" style="display: none;">
        <form action="async/admin_notice.php" method="post" onsubmit="return asyncAdminNotice(this);">
            <input type="hidden" name="site" value="<?= $_SERVER['QUERY_STRING'] ?>" />            
            <br />
            <textarea name="message" style="width: 640px; height: 160px;"><?= loadAdminNotice(); ?></textarea>
            <br />
            <input type="submit" value="Speichern" />    
        </form>
    </dd>
</dl>
<script>
function asyncAdminNotice(frm) {
	jQuery.post(frm.action, jQuery(frm).serialize())
	   .done(function(data) {
		   if (data == 'ok') {
			   alert('Gespeichert');
			   document.location.reload();
		   } else {
			   alert('Fehler beim speichern :(');
		   }
	   });
	return false;	
}
</script>