<div class="editor">
    <div class="legend">Item Editor</div>
    <form id="editor-item-form" onsubmit="return false;" method="POST" accept-charset="UTF-8">
        <input type="hidden" name="id" value="0" />
        <label>Name</label>
        <input type="text" name="name" value="" maxlength="50" size="42" />
		<br />
		<label>Level</label>
        <input type="text" name="level" value="" maxlength="2" size="2"/>
		<br />
		<div style="border: 1px solid #CCCCCC; padding: 5px;">
        <label>Set:</label>
            <select name="set" onchange="applySetName(this);">
                <option value="0">-Kein Set / Neues Set-</option>
                <?php foreach ($sets as $set): ?>
                <option value="<?php echo $set['id']?>"><?php echo $set['name'] . '('. $set['description'] .')'; ?></option>            
                <?php endforeach; ?>
    		</select>
    		<input type="checkbox" name="set_edit" value="1" onclick="toggleSetName()" />		
    		editieren
    		<br />
    		Name:<input type="text" name="set_name" value="" disabled="disabled" /><br />
    		Beschreibung:<input type="text" name="set_description" value="" disabled="disabled" />
		</div>
		<br />
		<label>Character Klasse:</label>
		<select name="charclass" onchange="displayItems()">
    		<option value="0">-Bitte auswählen-</option>
            <?php foreach ($charClasses as $class): ?>
            <option value="<?php echo $class['id'] ?>"><?php echo $class['description'] ?></option>
            <?php endforeach; ?>
        </select>
        <br />
    	<label>Qualität:</label>
    	<select name="quality" onchange="displayItems()">
    		<option value="0">-Bitte auswählen-</option>
    		<?php foreach ($qualities as $quality): ?>
            <option value="<?php echo $quality['id']?>"><?php echo $quality['description'] ?></option>
            <?php endforeach; ?>
    	</select>
    	<br />
    	<label>Ausrüstungsart:</label>
    	<select name="part" onchange="displayItems()">
    		<option value="0">-Bitte auswählen-</option>
    		<?php foreach ($parts as $part): ?>
            <option value="<?php echo $part['id']?>"><?php echo $part['description'] ?></option>
            <?php endforeach; ?>
    	</select>
    	<br />
    	<div id="slots">
			<?php for ($i=0; $i<=$slotcount; $i++): ?>			
			<div class="slot" id="slot_<?= $i ?>" style="display: none;">
				<input type="radio" name="icon" value="<?= $i ?>" />
				<span id="item_<?= $i ?>" class=""><span id="quali_<?= $i ?>" class=""></span></span>
			</div>
			<?php endfor; ?>
			<div class="clear"></div>			
		</div>
		<br />
        <input type="submit" value="speichern" onclick="asyncSaveItem(this.form);" />
        <input type="button" value="Reset/Neu" onclick="resetForm();" />
	</form>
</div>