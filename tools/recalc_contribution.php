<?php
include 'bootstrap.php';

use Ewigkeit\Sql\SqlAdapter;

function getMembers($sql)
{
    $members = array();
    $queryString = "SELECT  id, name
                    FROM    members
                    WHERE   twink = 0
                    AND     status = 1";
    $result = $sql->query($queryString, \PDO::FETCH_OBJ);
    if ($result !== false) {
        return $result;
    }
    return array();
}

function getMemberContribution($sql, $memberId)
{
    $queryString = "SELECT  `id`, date(`woche`) as woche, date(date_sub(`woche`, interval 7 day)) as vorwoche,
                            `beitrag` as contribution, 0 as increase
                    FROM    `beitrag`
                    WHERE   `memberId` = {$memberId}
                    ORDER BY    `woche`";
    $result = $sql->query($queryString);
    if ($result !== false) {
        return $result;
    }
    return array();
}

function determineIncreasePerWeek($result)
{
    $lastContribution = 0;
    $lastWeek = null;
    // calculate the increase each weak
    foreach ($result as $key => $val) {
        if ($key > 0 && $lastWeek != $val['vorwoche']) {
            $lastContribution = $val['contribution'];
            $lastWeek = $val['woche'];
            continue;
        }
        $result[$key]['increase'] = $val['contribution'] - $lastContribution;
        $lastContribution = $val['contribution'];
        $lastWeek = $val['woche'];
    }
    return $result;    
}

function updateWeeklyContribution($sql, $result)
{
    foreach ($result as $key => $val) {
        $queryString = "UPDATE  `beitrag`
                        SET     `zuwachs` = {$val['increase']}
                        WHERE   `id` = {$val['id']}";
        $sql->exec($queryString);
    }
}

$sql = SqlAdapter::getInstance();

// get all members
$members = getMembers($sql);

foreach ($members as $member) {
    echo "<p>Calculate weekly contribution for <strong>{$member->name}</strong>"; 
    $contribution = getMemberContribution($sql, $member->id);
    $contribution = determineIncreasePerWeek($contribution);
    echo "...done<br />updating database";
    updateWeeklyContribution($sql, $contribution);
    echo "...done</p>";
}
$memberCount = sizeof($members);
echo "<br /><p><strong>Updated weekly contribution of {$memberCount} members</strong></p>";